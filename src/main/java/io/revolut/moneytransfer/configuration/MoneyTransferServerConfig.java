package io.revolut.moneytransfer.configuration;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import java.net.URI;

public class MoneyTransferServerConfig {

    public static final String APP_BASE_URI = "http://localhost:8080/";

    public static final String ORG_PREFIX = "/revolut";

    public static HttpServer startServer(String uriPrefix) {

        final ResourceConfig serverResourceConfiguration = new ResourceConfig().packages("io.revolut.moneytransfer");

        serverResourceConfiguration.property(ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR, "true");

        // Now you can expect validation errors to be sent to the client.
        serverResourceConfiguration.property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);

        // @ValidateOnExecution annotations on subclasses won't cause errors.
        serverResourceConfiguration.property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);

        return GrizzlyHttpServerFactory.createHttpServer(URI.create(uriPrefix), serverResourceConfiguration);
    }
}
