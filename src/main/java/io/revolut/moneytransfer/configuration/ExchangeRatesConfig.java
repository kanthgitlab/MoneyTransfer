package io.revolut.moneytransfer.configuration;

import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.model.CurrencyPairModel;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ExchangeRatesConfig {

    public static final Map<CurrencyPairModel, BigDecimal>
    rates = new HashMap<CurrencyPairModel, BigDecimal>() {{
        put(new CurrencyPairModel(Currency.GBP, Currency.EUR), BigDecimal.valueOf(1.11));
        put(new CurrencyPairModel(Currency.GBP, Currency.USD), BigDecimal.valueOf(1.25));
        put(new CurrencyPairModel(Currency.GBP, Currency.GBP), BigDecimal.valueOf(1D));

        put(new CurrencyPairModel(Currency.USD, Currency.EUR), BigDecimal.valueOf(0.89));
        put(new CurrencyPairModel(Currency.USD, Currency.USD), BigDecimal.valueOf(1D));
        put(new CurrencyPairModel(Currency.USD, Currency.GBP), BigDecimal.valueOf(0.8));

        put(new CurrencyPairModel(Currency.EUR, Currency.EUR), BigDecimal.valueOf(1D));
        put(new CurrencyPairModel(Currency.EUR, Currency.USD), BigDecimal.valueOf(1.13));
        put(new CurrencyPairModel(Currency.EUR, Currency.GBP), BigDecimal.valueOf(0.9));
    }};
}
