package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.model.CurrencyPairModel;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

/**
 * This class has implementation to query exchangeRates for a CurrencyPair
 * Uses fixed set of exchange rates.
 * Eventually can be linked to external-feed of market-rates / exchange-rates API
 */
public class ExchangeRateQueryService  {

    private Map<CurrencyPairModel, BigDecimal> exchangeRates;

    public ExchangeRateQueryService() { }

    public Map<CurrencyPairModel, BigDecimal> getExchangeRates() {
        return Collections.unmodifiableMap(exchangeRates);
    }

    public Double getExchangeRate(Currency baseCurrency,Currency alternateCurrency){
        BigDecimal rateAsDecimal = getExchangeRates().get(CurrencyPairModel.builder().baseCurrency(baseCurrency).alternateCurrency(alternateCurrency).build());

        return rateAsDecimal!=null ? rateAsDecimal.doubleValue() : null;
    }

    public Map<CurrencyPairModel, BigDecimal> buildStaticRates(Map<CurrencyPairModel, BigDecimal> _exchangeRates){
        this.exchangeRates = _exchangeRates;
        return exchangeRates;
    }
}
