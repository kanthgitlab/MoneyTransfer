package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.entity.Account;
import io.revolut.moneytransfer.exception.AccountNotFoundException;
import io.revolut.moneytransfer.exception.DuplicateAccountCreationExceptionMoneyTransfer;
import io.revolut.moneytransfer.exception.MoneyTransferApplicationException;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.repository.AccountRepository;
import org.multiverse.api.StmUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.revolut.moneytransfer.util.MoneyTransferAPIUtil.getAccount;
import static io.revolut.moneytransfer.util.MoneyTransferAPIUtil.getAccountModel;
import static io.revolut.moneytransfer.util.MoneyTransferAPIUtil.validateAmount;
import static io.revolut.moneytransfer.util.TimeManagementUtil.getCurrentTime;
import static org.multiverse.api.StmUtils.atomic;

/**
 * Service to maintain Account Details
 *
 * Available Functions:
 *  1. Create a new Account (All accountTypes)
 *  2. Update AccountHolder's non-financial details
 *  3. Deactivate Account
 *  4. Update AccountBalance for an Account
 *  5. Transfer funds between accounts
 */
public class AccountOperationsService {

    private static final Logger log = LoggerFactory.getLogger(AccountOperationsService.class);

    private AccountRepository accountRepository;

    private AccountQueryService accountQueryService;

    private AccountOperationsService() {}

    public static AccountOperationsService build(AccountRepository _accountRepository,
                                                 AccountQueryService _accountQueryService){
        AccountOperationsService accountOperationsService = new AccountOperationsService();
        accountOperationsService.accountRepository = _accountRepository;
        accountOperationsService.accountQueryService = _accountQueryService;
        return accountOperationsService;
    }


    /**
     * create new Account
     *
     * @param accountModel
     * @return Account
     * @throws Exception
     */
    public AccountModel createNewAccount(AccountModel accountModel) {

        AccountType accountType = accountModel.getAccountType();
        String accountHolderName = accountModel.getAccountHolderName();
        Currency currency = accountModel.getCurrency();

        if(accountRepository.getAnAccountByAccountHolderNameAndAccountTypeAndCurrency(accountHolderName,accountType,currency) !=null){
            throw new DuplicateAccountCreationExceptionMoneyTransfer(String.format("Cannot create %s Account for : %s - Reason: AccountHolder has Active %s account",accountType,accountHolderName,accountType));
        }

        Account account_For_Creation = getAccount(accountModel);

        account_For_Creation.setIsActive(true);
        account_For_Creation.setCreationDate(getCurrentTime());

        Account account_Created = accountRepository.create(account_For_Creation);

        AccountModel accountModel_Response = atomic (()-> getAccountModel(account_Created));

        return accountModel_Response;
    }

    /**
     *
     * Update AccountBalance
     *
     * transaction is protected by STM (Software Transactional Memory Implementation - Multiverse)
     *
     * @param accountNumber
     * @param currentBalance
     * @param accountBalance
     * @return
     */
    public AccountModel updateAccountBalance(Integer accountNumber,Double currentBalance, Double accountBalance){

        accountQueryService.validateAccountNumber(accountNumber);

        validateAmount(currentBalance);

        validateAmount(accountBalance);

        Account account = accountRepository.getAnActiveAccountByAccountNumber(accountNumber);

        StmUtils.atomic(() -> {
            account.updateBalance(accountBalance.doubleValue());
            account.setUpdateDate(getCurrentTime());
            accountRepository.update(account);
        });

        return accountQueryService.getAnAccountByAccountNumber(accountNumber);
    }

    /**
     * Update AccountHolder's non-financial details
     *
     * @param accountModel
     * @return Account
     */
    public AccountModel updateAccountDetails(AccountModel accountModel){
        accountQueryService.validateAccountNumber(accountModel.getAccountNumber());
        Account account = accountRepository.getAnAccountByAccountNumber(accountModel.getAccountNumber());
        account.setAccountHolderName(accountModel.getAccountHolderName());
        account.setAccountType(accountModel.getAccountType());
        account.setNationalIdentityCode(accountModel.getNationalIdentityCode());
        account.setAddress(accountModel.getAddress());
        account.setUpdateDate(getCurrentTime());
        accountRepository.update(account);

        Account accountDetails = accountRepository.getAnAccountByAccountNumber(account.getAccountNumber());

        return accountDetails!=null ? getAccountModel(accountDetails) : null;
    }

    /**
     *
     * Deactivate Account by AccountNumber
     *
     * @param accountNumber
     * @return Account
     */
    public AccountModel deactivateAccount(Integer accountNumber){
        accountQueryService.validateAccountNumber(accountNumber);
        Account account = accountRepository.getAnAccountByAccountNumber(accountNumber);
        account.setIsActive(false);
        account.setUpdateDate(getCurrentTime());
        accountRepository.update(account);
        return getAccountModel(accountRepository.getAnAccountByAccountNumber(account.getAccountNumber()));
    }

    /**
     *
     * Delete Account by AccountNumber
     *
     * @param accountNumber
     * @return Boolean
     */
    public Boolean deleteAccount(Integer accountNumber){
        accountQueryService.validateAccountNumber(accountNumber);
        return accountRepository.delete(accountNumber);
    }

    /**
     *
     * Delete All Accounts
     *
     */
    public void deleteAllAccounts(){
         accountRepository.cleanup();
    }

    /**
     *
     * Activate Account by AccountNumber
     *
     * @param accountNumber
     * @return Account
     */
    public AccountModel activateAccount(Integer accountNumber){
        Account account = accountRepository.getAnAccountByAccountNumber(accountNumber);

        String errorMessage;

        if(account == null){
            errorMessage = String.format("AccountNumber: %s doesn't exist",accountNumber.toString());
            log.error(errorMessage);
            throw new AccountNotFoundException(errorMessage);
        }else if(account.getIsActive()){
            errorMessage = String.format("Cannot Activate an Active-Account : %s",accountNumber.toString());
            log.error(errorMessage);
            throw new MoneyTransferApplicationException(errorMessage);
        }

        account.setIsActive(true);
        account.setUpdateDate(getCurrentTime());
        accountRepository.update(account);
        return getAccountModel(accountRepository.getAnAccountByAccountNumber(account.getAccountNumber()));
    }



}
