package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.TransactionType;
import io.revolut.moneytransfer.entity.Transaction;
import io.revolut.moneytransfer.model.TransactionModel;
import io.revolut.moneytransfer.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static io.revolut.moneytransfer.util.MoneyTransferAPIUtil.getTransactionModel;

/**
 *Service to Query transactions from transactionStore. TransactionTypes are listed in <link>{@link TransactionType}</link>
 */
public class TransactionQueryService {

    private static final Logger log = LoggerFactory.getLogger(TransactionQueryService.class);

    private TransactionRepository transactionRepository;

    //Comparator - with sort By transaction's creationDate in Descending-Order
    public static Supplier<Comparator<TransactionModel>> compareByCreationDateDescending = () -> (transactionObject1, transactionObject2) -> transactionObject2.getCreationDate().compareTo(transactionObject1.getCreationDate());

    //Comparator - with sort By transaction's amount in Descending-Order
    public static Supplier<Comparator<TransactionModel>> compareByAmountDescending = () -> (transactionObject1, transactionObject2) -> transactionObject2.getAmount().compareTo(transactionObject1.getAmount());

    //Comparator - with sort By transaction's Type in Descending-Order
    public static Supplier<Comparator<TransactionModel>> compareByTransactionTypeDescending = () -> (transactionObject1, transactionObject2) -> transactionObject2.getTransactionType().compareTo(transactionObject1.getTransactionType());

    //Comparator - with sort By transaction's Status in Descending-Order
    public static Supplier<Comparator<TransactionModel>> compareByTransactionStatusDescending = () -> (transactionObject1, transactionObject2) -> transactionObject2.getTransactionStatus().compareTo(transactionObject1.getTransactionStatus());

    /**
     * private constructor
     */
    private TransactionQueryService() { }

    /**
     *
     * @param transactionRepository
     * @return MoneyTransferService
     */
    public static TransactionQueryService build(TransactionRepository transactionRepository) {
        TransactionQueryService transactionService = new TransactionQueryService();
        transactionService.transactionRepository = transactionRepository;
        return transactionService;
    }

    /**
     * get All Transactions
     * @return List<TransactionModel>
     */
    public List<TransactionModel> getAllTransactions(){
        List<Transaction> transactions = transactionRepository.getAllTransactions();
        return transactions.stream().map((transaction -> getTransactionModel(transaction)))
                .sorted(compareByCreationDateDescending.get()
                        .thenComparing(compareByAmountDescending.get()
                        .thenComparing(compareByTransactionStatusDescending.get())
                        .thenComparing(compareByTransactionTypeDescending.get())))
                .collect(Collectors.toList());
    }


    /**
     * get transactionDetails by transactionId
     * @return TransactionModel
     */
    public TransactionModel getTransactionDetails(String transactionId){
        Transaction transaction = transactionRepository.getATransactionByTransactionId(transactionId);
        return getTransactionModel(transaction);
    }

    /**
     * get all transactions by accountNumber
     * @return List<TransactionModel>
     */
    public List<TransactionModel> getAllTransactionsOfAnAccount(Integer accountNumber){
        List<Transaction> transactions = transactionRepository.getAllTransactionsByAccountNumber(accountNumber);
        return transactions.stream().map(transaction -> getTransactionModel(transaction))
                .sorted(compareByCreationDateDescending.get()
                        .thenComparing(compareByAmountDescending.get())
                                .thenComparing(compareByTransactionStatusDescending.get())
                                .thenComparing(compareByTransactionTypeDescending.get()))
                .collect(Collectors.toList());
    }

}
