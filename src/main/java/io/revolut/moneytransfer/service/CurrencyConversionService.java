package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.Currency;

import java.math.BigDecimal;

public class CurrencyConversionService {

    private ExchangeRateQueryService exchangeRateQueryService;

    private CurrencyConversionService() {}

    public static CurrencyConversionService build(ExchangeRateQueryService _exchangeRateQueryService){
        CurrencyConversionService currencyConversionService = new CurrencyConversionService();
        currencyConversionService.exchangeRateQueryService = _exchangeRateQueryService;
        return currencyConversionService;
    }

    public Double convert(Double amount, Currency baseCurrency, Currency alternateCurrency) {

        Double exchangeRate = exchangeRateQueryService.getExchangeRate(baseCurrency,alternateCurrency);

        return convert(amount,exchangeRate);
    }

    public Double convert(Double amount, Double exchangeRate) {
        return BigDecimal.valueOf(amount).multiply(BigDecimal.valueOf(exchangeRate)).doubleValue();
    }
}
