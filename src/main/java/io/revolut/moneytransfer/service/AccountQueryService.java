package io.revolut.moneytransfer.service;

import com.google.common.base.Preconditions;
import io.revolut.moneytransfer.entity.Account;
import io.revolut.moneytransfer.exception.AccountNotFoundException;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static io.revolut.moneytransfer.util.MoneyTransferAPIUtil.getAccountModel;

/**
 * Service to maintain Account Details
 *
 * Available Functions:
 *  1. query Accounts
 *  2. retrieve AccountDetails
 */
public class AccountQueryService {

    private static final Logger log = LoggerFactory.getLogger(AccountQueryService.class);

    private AccountRepository accountRepository;

    //Comparator - with sort By Account's AccountNumber in Ascending-Order
    static Supplier<Comparator<AccountModel>> compareByAccountNumberAscending = () -> (account1, account2) -> account1.getAccountNumber().compareTo(account2.getAccountNumber());

    private AccountQueryService() {}

    public static AccountQueryService build(AccountRepository _accountRepository){
        AccountQueryService accountService = new AccountQueryService();
        accountService.accountRepository = _accountRepository;
        return accountService;
    }

    /**
     * get All Accounts
     * @return List<AccountModel>
     */
    public List<AccountModel> getAllAccounts(){
        List<Account> accounts = accountRepository.getAllAccounts();
        return accounts.stream().map((account -> getAccountModel(account)))
                                .sorted(compareByAccountNumberAscending.get())
                                .collect(Collectors.toList());
    }

    /**
     * get All Accounts
     * @return List<AccountModel>
     */
    public List<AccountModel> getAllActiveAccounts(){
        List<Account> accounts = accountRepository.getAllActiveAccounts();
        return accounts.stream().map((account -> getAccountModel(account)))
                                .sorted(compareByAccountNumberAscending.get())
                                .collect(Collectors.toList());
    }

    /**
     * get AccountDetails by AccountNumber
     * @param accountNumber
     * @return Account
     */
    public AccountModel getAnAccountByAccountNumber(Integer accountNumber){
        Account account = accountRepository.getAnAccountByAccountNumber(accountNumber);
        return account!=null ? getAccountModel(account) : null;
    }

    /**
     * get Active-AccountDetails by AccountNumber
     * @param accountNumber
     * @return AccountModel
     */
    public AccountModel getAnActiveAccountByAccountNumber(Integer accountNumber){
        Account account = accountRepository.getAnActiveAccountByAccountNumber(accountNumber);
        return account!=null ? getAccountModel(account) : null;
    }

    /**
     * validate AccountNumber
     *
     * @param accountNumber
     */
    public void validateAccountNumber(Integer accountNumber) {

        Preconditions.checkArgument(accountNumber != null, "AccountNumber is empty.");
        Preconditions.checkArgument(accountNumber > 0, "AccountNumber should be valid Integer");

        Account account = accountRepository.getAnAccountByAccountNumber(accountNumber);

        if(account == null ){
            throw new AccountNotFoundException(String.format("AccountNumber: %s doesn't exist",accountNumber.toString()));
        }else if(!account.getIsActive()){
            throw new AccountNotFoundException(String.format("AccountNumber: %s is Inactive",accountNumber.toString()));
        }

    }

    public AccountModel getAnAccountByAccountNumberAndAccountHolderName(Integer accountNumber, String accountHolderName) {

        Preconditions.checkArgument(accountNumber != null, "AccountNumber is empty.");
        Preconditions.checkArgument(accountNumber > 0, "AccountNumber should be valid Integer");
        Preconditions.checkArgument(accountHolderName != null && accountHolderName != "", "accountHolderName is empty.");

        Account account =  accountRepository.getAnAccountByAccountNumberAndAccountHolderName(accountNumber,accountHolderName);

        return account!=null ? getAccountModel(account) : null;
    }

}
