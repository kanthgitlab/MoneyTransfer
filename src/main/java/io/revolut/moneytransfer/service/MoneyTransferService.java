package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.TransactionStatus;
import io.revolut.moneytransfer.constants.TransactionType;
import io.revolut.moneytransfer.entity.Account;
import io.revolut.moneytransfer.entity.Transaction;
import io.revolut.moneytransfer.exception.InsufficientMoneyException;
import io.revolut.moneytransfer.model.TransactionModel;
import io.revolut.moneytransfer.model.TransactionRequestModel;
import io.revolut.moneytransfer.repository.AccountRepository;
import io.revolut.moneytransfer.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static io.revolut.moneytransfer.util.MoneyTransferAPIUtil.getTransactionModel;
import static io.revolut.moneytransfer.util.MoneyTransferAPIUtil.validateAmount;
import static io.revolut.moneytransfer.util.TimeManagementUtil.getCurrentTime;
import static org.multiverse.api.StmUtils.atomic;

/**
 *Service to execute Money-Transfer-Transactions. TransactionTypes are listed in <link>{@link TransactionType}</link>
 */
public class MoneyTransferService {

    private static final Logger log = LoggerFactory.getLogger(MoneyTransferService.class);

    //0.05% -> 0.0005
    public static final double TRANSACTION_FEE_PERCENTAGE_AMOUNT = 0.0005;

    private TransactionRepository transactionRepository;

    private AccountQueryService accountQueryService;

    private AccountRepository accountRepository;

    private CurrencyConversionService currencyConversionService;

    private ExchangeRateQueryService exchangeRateQueryService;


    public static String DEPOSIT_TRANSACTION_MESSAGE =
            "%s Deposit Transaction for amount: %s %s to BankAccountNumber: %s" ;

    public static String WITHDRAWAL_TRANSACTION_MESSAGE =
            "%s Withdrawal Transaction for amount: %s %s from BankAccountNumber: %s" ;

    public static String TRANSFER_TRANSACTION_MESSAGE =
            "%s MoneyTransfer Transaction for amount: %s %s from senderBankAccountNumber: %s to recipientBankAccountNumber: %s" ;

    /**
     * private constructor
     */
    private MoneyTransferService() { }

    /**
     *
     * @param transactionRepository
     * @param accountQueryService
     * @param accountRepository
     * @return MoneyTransferService
     */
    public static MoneyTransferService build(TransactionRepository transactionRepository,
                                             AccountQueryService accountQueryService,
                                             AccountRepository accountRepository,
                                             ExchangeRateQueryService exchangeRateQueryService,
                                             CurrencyConversionService currencyConversionService) {
        MoneyTransferService moneyTransferService = new MoneyTransferService();
        moneyTransferService.transactionRepository = transactionRepository;
        moneyTransferService.accountQueryService = accountQueryService;
        moneyTransferService.accountRepository = accountRepository;
        moneyTransferService.exchangeRateQueryService = exchangeRateQueryService;
        moneyTransferService.currencyConversionService = currencyConversionService;
        return moneyTransferService;
    }

    /**
     *
     * executes money-deposit transaction
     *
     * @param transactionRequestModel
     * @return TransactionModel
     */
    public TransactionModel depositMoney(TransactionRequestModel transactionRequestModel) {

        return atomic(() -> {
            Integer accountNumber = transactionRequestModel.getBankAccountNumber();
            Double depositAmount = transactionRequestModel.getAmount();

            accountQueryService.validateAccountNumber(accountNumber);
            validateAmount(depositAmount);

            Account recipientAccount = accountRepository.getAnAccountByAccountNumber(accountNumber);

            Boolean trackErrors = Boolean.FALSE;

            List<String> errorMessages = new ArrayList<>();


            if (!transactionRequestModel.getTransactionType().equals(TransactionType.DEPOSIT)) {
                String errorMessage = String.format("Failed to withdraw Amount: %s from Account: %s due to Invalid-TransactionType", depositAmount.toString(), accountNumber);
                log.error("Validation Error in Deposit Transaction: {}", errorMessage);
                errorMessages.add(errorMessage);
                trackErrors = true;
            }

            if (!transactionRequestModel.getCurrency().equals(recipientAccount.getCurrency())) {

                String errorMessage = String.format("Cannot deposit money in Currency: %s to RecipientAccount: %s (%s)",
                        transactionRequestModel.getCurrency()  , recipientAccount.getAccountNumber(), recipientAccount.getCurrency());

                log.error("Validation Error in Withdrawal Transaction: {}", errorMessage);
                errorMessages.add(errorMessage);
                trackErrors = true;
            }

            if (!trackErrors) {
                recipientAccount.deposit(depositAmount);
            }

            TransactionStatus transactionStatus = !trackErrors ? TransactionStatus.SUCCESSFUL : TransactionStatus.FAILED;

            Transaction depositTransaction = new Transaction();
            depositTransaction.setAmount(depositAmount);
            depositTransaction.setRecipientBankAccountNumber(accountNumber);
            depositTransaction.setRecipientBankAccountCurrency(recipientAccount.getCurrency());
            depositTransaction.setCreationDate(getCurrentTime());
            depositTransaction.setCurrency(transactionRequestModel.getCurrency());
            depositTransaction.setTransactionType(TransactionType.DEPOSIT);

            Double accountBalance = accountRepository.getAnAccountByAccountNumber(accountNumber).getAccountBalanceAmount();
            depositTransaction.setAccountBalance(accountBalance);

            depositTransaction.setTransactionStatus(transactionStatus);
            depositTransaction.setTransactionMaker(transactionRequestModel.getTransactionMaker());
            depositTransaction.setTransactionRecipient(transactionRequestModel.getTransactionRecipient());
            depositTransaction.setTransactionFees(0D);
            depositTransaction.setExceptionMessages(errorMessages);

            String message = String.format(DEPOSIT_TRANSACTION_MESSAGE, transactionStatus, depositAmount,
                    recipientAccount.getCurrency(), recipientAccount.getAccountNumber().toString());

            depositTransaction.setMessage(message);

            Transaction transactionEntity_Persisted = transactionRepository.createTransaction(depositTransaction);

            TransactionModel transactionModel = getTransactionModel(transactionEntity_Persisted);

            return transactionModel;
        });
    }

    /**
     *
     * executes money-withdrawal transaction
     *
     * @param transactionRequestModel
     * @return TransactionModel
     */
    public TransactionModel withdrawMoney(TransactionRequestModel transactionRequestModel) {

        return  atomic(() -> {
                        Integer accountNumber = transactionRequestModel.getBankAccountNumber();
                        Double withdrawalAmount = transactionRequestModel.getAmount();

                        accountQueryService.validateAccountNumber(accountNumber);
                        validateAmount(withdrawalAmount);

                        Account account = accountRepository.getAnAccountByAccountNumber(accountNumber);

                        Double currentBalance = account.getAccountBalanceAmount();

                        Boolean trackErrors = Boolean.FALSE;

                        List<String> errorMessages = new ArrayList<>();

                        if (currentBalance.compareTo(withdrawalAmount) < 0) {
                            String errorMessage = String.format("Failed to withdraw Amount: %s from Account: %s due to inSufficient Money", withdrawalAmount.toString(), accountNumber);
                            log.error("Validation Error in Withdrawal Transaction: {}", errorMessage);
                            errorMessages.add(errorMessage);
                            trackErrors = true;
                        }

                        if (!transactionRequestModel.getCurrency().equals(account.getCurrency())) {

                            String errorMessage = String.format("Cannot withdraw money in Currency: %s from SenderAccount: %s (%s)",
                                    transactionRequestModel.getCurrency()  , account.getAccountNumber(), account.getCurrency());

                            log.error("Validation Error in Withdrawal Transaction: {}", errorMessage);
                            errorMessages.add(errorMessage);
                            trackErrors = true;
                        }

                        if (!trackErrors) {
                            account.withdraw(withdrawalAmount);
                        }

                        TransactionStatus transactionStatus = !trackErrors ? TransactionStatus.SUCCESSFUL : TransactionStatus.FAILED;


                        Transaction withdrawalTransaction = new Transaction();
                        withdrawalTransaction.setAmount(withdrawalAmount);
                        withdrawalTransaction.setRecipientBankAccountNumber(accountNumber);
                        withdrawalTransaction.setRecipientBankAccountCurrency(account.getCurrency());
                        withdrawalTransaction.setCreationDate(getCurrentTime());
                        withdrawalTransaction.setCurrency(transactionRequestModel.getCurrency());
                        withdrawalTransaction.setTransactionType(TransactionType.WITHDRAWAL);
                        withdrawalTransaction.setTransactionStatus(transactionStatus);

                        Double accountBalance = accountRepository.getAnAccountByAccountNumber(accountNumber).getAccountBalanceAmount();
                        withdrawalTransaction.setAccountBalance(accountBalance);

                        withdrawalTransaction.setTransactionMaker(transactionRequestModel.getTransactionMaker());
                        withdrawalTransaction.setTransactionRecipient(transactionRequestModel.getTransactionRecipient());
                        withdrawalTransaction.setTransactionFees(0D);
                        withdrawalTransaction.setExceptionMessages(errorMessages);

                        String message = String.format(WITHDRAWAL_TRANSACTION_MESSAGE, transactionStatus, withdrawalAmount
                                , account.getCurrency(), account.getAccountNumber().toString());

                        withdrawalTransaction.setMessage(message);

                        Transaction withdrawalTransaction_Saved = transactionRepository.createTransaction(withdrawalTransaction);

                        log.info("transactionMessage Stored: {}",transactionRepository.getATransactionByTransactionId(withdrawalTransaction_Saved.getTransactionId()));

                        TransactionModel transactionModel = getTransactionModel(withdrawalTransaction_Saved);

                        return transactionModel;
            });
    }


    /**
     *
     * @param transactionRequestModel
     * @return TransactionModel
     */
    public TransactionModel transferMoney(TransactionRequestModel transactionRequestModel) {

        Double transferAmount = transactionRequestModel.getAmount();
        validateAmount(transferAmount);

        Integer senderAccountNumber = transactionRequestModel.getSenderBankAccountNumber();
        accountQueryService.validateAccountNumber(senderAccountNumber);

        Integer recipientAccountNumber = transactionRequestModel.getRecipientBankAccountNumber();
        accountQueryService.validateAccountNumber(recipientAccountNumber);

        Boolean trackErrors = Boolean.FALSE;

        List<String> errorMessages = new ArrayList<>();

        if(senderAccountNumber == recipientAccountNumber){
            String errorMessage = String.format("Invalid Transfer between accounts: %s and %s",senderAccountNumber,recipientAccountNumber);
            errorMessages.add(errorMessage);
            trackErrors = true;
        }

        Account senderBankAccount = accountRepository.getAnAccountByAccountNumber(senderAccountNumber);
        Account recipientBankAccount = accountRepository.getAnAccountByAccountNumber(recipientAccountNumber);

        if (!senderBankAccount.getCurrency().equals(recipientBankAccount.getCurrency())) {

            String errorMessage = String.format("Cannot transfer funds between Accounts with disparate Currency %s and %s",
                    senderBankAccount.getCurrency(), recipientBankAccount.getCurrency());

            log.error("Validation Error in Money-Transfer Transaction: {}", errorMessage);

            errorMessages.add(errorMessage);
            trackErrors = true;
        }

        if (!senderBankAccount.getCurrency().equals(transactionRequestModel.getCurrency())) {

            String errorMessage = String.format("Cannot transfer money in Currency: %s from SenderAccount: %s (%s)",
                    transactionRequestModel.getCurrency(), senderBankAccount.getAccountNumber(), senderBankAccount.getCurrency());

            log.error("Validation Error in Money-Transfer Transaction: {}", errorMessage);

            errorMessages.add(errorMessage);
            trackErrors = true;
        }else if (senderBankAccount.getAccountBalanceAmount() < transferAmount) {

                String errorMessage = String.format("MoneyTransfer failed -  Reason: Insufficient money in Sender's Account with AccountNumber: %s (%s)",
                                                    senderAccountNumber.toString(),senderBankAccount.getCurrency());

                log.error(errorMessage);

                errorMessages.add(errorMessage);
                trackErrors = true;
        }

        AtomicBoolean isSuccessful = new AtomicBoolean(false);

        Boolean isTransferSuccessful = false;


        if(!trackErrors) {
            isTransferSuccessful = atomic(() -> {

                if (senderBankAccount.getAccountBalance().get() >= transferAmount) {

                    senderBankAccount.transferMoney(recipientBankAccount,transferAmount);

                    isSuccessful.compareAndSet(false, true);

                    if (accountRepository.getAnAccountByAccountNumber(senderAccountNumber).getAccountBalance().get() < 0) {

                        String message = String.format("MoneyTransfer failed -  Reason: Insufficient money in Sender's Account with AccountNumber: %s (%s)", senderAccountNumber.toString(),senderBankAccount.getCurrency());

                        log.error(message);

                        throw new InsufficientMoneyException(message);
                    }

                }

                return isSuccessful.get();

            });
        }

        TransactionStatus transactionStatus = isTransferSuccessful ? TransactionStatus.SUCCESSFUL : TransactionStatus.FAILED;

        Transaction transferTransaction = new Transaction();
        transferTransaction.setAmount(transferAmount);
        transferTransaction.setSenderBankAccountNumber(senderAccountNumber);
        transferTransaction.setSenderBankAccountCurrency(senderBankAccount.getCurrency());
        transferTransaction.setRecipientBankAccountNumber(recipientAccountNumber);
        transferTransaction.setRecipientBankAccountCurrency(recipientBankAccount.getCurrency());
        transferTransaction.setCreationDate(getCurrentTime());
        transferTransaction.setCurrency(transactionRequestModel.getCurrency());
        transferTransaction.setTransactionType(TransactionType.TRANSFER);

        Double accountBalance = accountRepository.getAnAccountByAccountNumber(senderAccountNumber).getAccountBalanceAmount();
        transferTransaction.setAccountBalance(accountBalance);

        transferTransaction.setTransactionStatus(transactionStatus);
        transferTransaction.setTransactionMaker(transactionRequestModel.getTransactionMaker());
        transferTransaction.setTransactionRecipient(transactionRequestModel.getTransactionRecipient());
        transferTransaction.setTransactionFees(0D);
        transferTransaction.setExceptionMessages(errorMessages);

        String message = String.format(TRANSFER_TRANSACTION_MESSAGE,transactionStatus,transferAmount,transactionRequestModel.getCurrency(),
                transactionRequestModel.getSenderBankAccountNumber().toString(),
                transactionRequestModel.getRecipientBankAccountNumber().toString());

        transferTransaction.setMessage(message);

        Transaction withdrawalTransaction_Saved = transactionRepository.createTransaction(transferTransaction);

        TransactionModel transactionModel = getTransactionModel(withdrawalTransaction_Saved);

        return transactionModel;
    }

    /**
     *
     * @param transactionRequestModel
     * @return TransactionModel
     */
    public TransactionModel transferToCrossCurrencyAccount(TransactionRequestModel transactionRequestModel) {

        Double transferAmount = transactionRequestModel.getAmount();
        validateAmount(transferAmount);

        Integer senderAccountNumber = transactionRequestModel.getSenderBankAccountNumber();
        accountQueryService.validateAccountNumber(senderAccountNumber);

        Integer recipientAccountNumber = transactionRequestModel.getRecipientBankAccountNumber();
        accountQueryService.validateAccountNumber(recipientAccountNumber);

        Boolean trackErrors = Boolean.FALSE;

        List<String> errorMessages = new ArrayList<>();

        if(senderAccountNumber == recipientAccountNumber){
            String errorMessage = String.format("Invalid Transfer between accounts: %s and %s",senderAccountNumber,recipientAccountNumber);
            errorMessages.add(errorMessage);
            trackErrors = true;
        }

        Account senderBankAccount = accountRepository.getAnAccountByAccountNumber(senderAccountNumber);
        Account recipientBankAccount = accountRepository.getAnAccountByAccountNumber(recipientAccountNumber);

        Double exchangeRate = exchangeRateQueryService.getExchangeRate(transactionRequestModel.getCurrency(),senderBankAccount.getCurrency());

        Double transferAmountInSendersAccountCurrency = currencyConversionService.convert(transferAmount,exchangeRate);

        Double transactionFees = transferAmountInSendersAccountCurrency * exchangeRate * TRANSACTION_FEE_PERCENTAGE_AMOUNT;

        if (senderBankAccount.getAccountBalanceAmount() < transferAmountInSendersAccountCurrency+transactionFees) {

            String errorMessage = String.format("MoneyTransfer failed -  Reason: Insufficient money: %s (%s) in Sender's Account with AccountNumber: %s (%s)",
                    transferAmountInSendersAccountCurrency,senderBankAccount.getCurrency(),senderAccountNumber.toString(),senderBankAccount.getCurrency());

            log.error(errorMessage);

            errorMessages.add(errorMessage);
            trackErrors = true;
        }

        AtomicBoolean isSuccessful = new AtomicBoolean(false);

        Boolean isTransferSuccessful = false;


        if(!trackErrors) {
            isTransferSuccessful = atomic(() -> {

                if (senderBankAccount.getAccountBalance().get() >= transferAmountInSendersAccountCurrency+transactionFees) {

                    senderBankAccount.transferMoneyToCrossCurrency(recipientBankAccount,transferAmount,transferAmountInSendersAccountCurrency,transactionFees);

                    isSuccessful.compareAndSet(false, true);

                    if (accountRepository.getAnAccountByAccountNumber(senderAccountNumber).getAccountBalance().get() < 0) {

                        String message = String.format("MoneyTransfer failed -  Reason: Insufficient money in Sender's Account with AccountNumber: %s (%s)", senderAccountNumber.toString(),senderBankAccount.getCurrency());

                        log.error(message);

                        throw new InsufficientMoneyException(message);
                    }

                }

                return isSuccessful.get();

            });
        }

        TransactionStatus transactionStatus = isTransferSuccessful ? TransactionStatus.SUCCESSFUL : TransactionStatus.FAILED;

        Transaction transferTransaction = new Transaction();
        transferTransaction.setAmount(transferAmount);
        transferTransaction.setSenderBankAccountNumber(senderAccountNumber);
        transferTransaction.setSenderBankAccountCurrency(senderBankAccount.getCurrency());
        transferTransaction.setRecipientBankAccountNumber(recipientAccountNumber);
        transferTransaction.setRecipientBankAccountCurrency(recipientBankAccount.getCurrency());
        transferTransaction.setCreationDate(getCurrentTime());
        transferTransaction.setCurrency(transactionRequestModel.getCurrency());
        transferTransaction.setTransactionType(TransactionType.TRANSFER);

        Double accountBalance = accountRepository.getAnAccountByAccountNumber(senderAccountNumber).getAccountBalanceAmount();
        transferTransaction.setAccountBalance(accountBalance);

        transferTransaction.setTransactionStatus(transactionStatus);
        transferTransaction.setTransactionMaker(transactionRequestModel.getTransactionMaker());
        transferTransaction.setTransactionRecipient(transactionRequestModel.getTransactionRecipient());
        transferTransaction.setExceptionMessages(errorMessages);
        transferTransaction.setTransactionFees(transactionFees);
        transferTransaction.setExchangeRate(exchangeRate);

        String message = String.format(TRANSFER_TRANSACTION_MESSAGE,transactionStatus,transferAmount,transactionRequestModel.getCurrency(),
                transactionRequestModel.getSenderBankAccountNumber().toString(),
                transactionRequestModel.getRecipientBankAccountNumber().toString());

        transferTransaction.setMessage(message);

        Transaction withdrawalTransaction_Saved = transactionRepository.createTransaction(transferTransaction);

        TransactionModel transactionModel = getTransactionModel(withdrawalTransaction_Saved);

        return transactionModel;
    }

}
