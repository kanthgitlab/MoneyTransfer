package io.revolut.moneytransfer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.pojomatic.annotations.AutoProperty;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AutoProperty
public class Address {

    private String unitNumber;
    private String buildingName;
    private String streetOrLandmarkName;
    private String postalCode;
    private String countryCode;
}
