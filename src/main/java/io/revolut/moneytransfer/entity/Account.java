package io.revolut.moneytransfer.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.exception.InsufficientMoneyException;
import io.revolut.moneytransfer.exception.InvalidAmountException;
import io.revolut.moneytransfer.util.TimeManagementUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.multiverse.api.references.TxnDouble;
import org.multiverse.api.references.TxnRef;
import org.multiverse.stms.gamma.GammaStm;
import org.pojomatic.annotations.AutoProperty;

import java.time.LocalDateTime;

import static org.multiverse.api.StmUtils.atomic;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AutoProperty
public class Account {

    private TxnDouble accountBalance;
    private TxnRef<LocalDateTime> lastModified;
    private GammaStm stm;

    public Account(long amount){
        this.accountBalance = stm.getDefaultRefFactory().newTxnDouble(amount);
        this.lastModified.set(TimeManagementUtil.getCurrentTime());
    }

    private Integer accountNumber;
    private String accountHolderName;
    private Address address;
    private String nationalIdentityCode;
    private AccountType accountType;
    private Currency currency;
    private Boolean isActive = true;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime creationDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime updateDate;


    public void deposit(Double amount) {
        deposit(amount, TimeManagementUtil.getCurrentTime());
    }

    public void deposit(Double amount, LocalDateTime date) {
        atomic(() -> {

            if (amount <= 0) {
                throw new InvalidAmountException(String.format("Invalid Amount number: %d. Amount should be a non-Zero Positive Decimal number",amount));
            }

            accountBalance.incrementAndGet(amount);
            lastModified.set(date);
        });
    }

    public void withdraw(Double amount) {
        withdraw(amount, TimeManagementUtil.getCurrentTime());
    }

    public void withdraw(Double amount, LocalDateTime date) {
        atomic(() -> {

            if (amount <= 0) {
                throw new InvalidAmountException(String.format("Invalid Amount number: %d. Amount should be a non-Zero Positive Decimal number",amount));
            }

            if (accountBalance.get() <= 0) {
                throw new InsufficientMoneyException(String.format("Insufficient Money in Account: %s (%s)",this.accountNumber,this.accountHolderName));
            }

            accountBalance.incrementAndGet(-1*amount);
            lastModified.set(date);

        });
    }

    /**
     * transfer money between 2 accounts
     * Deadlock proof and transactional
     *
     * @param recipientAccount
     * @param transferAmount
     */
    public void transferMoney(Account recipientAccount,Double transferAmount){

        atomic(() -> {

                if(transferAmount <=0){
                    throw new InvalidAmountException(String.format("Invalid Amount number: %d. Amount should be a non-Zero Positive Decimal number",transferAmount));
                }

                if (accountBalance.get() < transferAmount) {
                    String message = String.format("MoneyTransfer failed -  Reason: Insufficient money in Sender's Account %s", accountNumber.toString());
                    throw new InsufficientMoneyException(message);
                }

                LocalDateTime timeStamp = TimeManagementUtil.getCurrentTime();
                withdraw(transferAmount, timeStamp);
                recipientAccount.deposit(transferAmount, timeStamp);
            }
        );
    }

    /**
     * transfer money between 2 accounts (CrossCurrencies)
     * Deadlock proof and transactional
     *
     * @param recipientAccount
     * @param transferAmountInRecipientCurrency
     * @param transferAmountInSendersCurrency
     * @param transactionFees
     */
    public void transferMoneyToCrossCurrency(Account recipientAccount,Double transferAmountInRecipientCurrency,Double transferAmountInSendersCurrency,Double transactionFees){

        atomic(() -> {

                    if(transferAmountInRecipientCurrency <=0){
                        throw new InvalidAmountException(String.format("Invalid transferAmount number: %d. Amount should be a non-Zero Positive Decimal number",transferAmountInRecipientCurrency));
                    }

                    if(transferAmountInSendersCurrency <=0){
                        throw new InvalidAmountException(String.format("Invalid Amount number: %d. Amount should be a non-Zero Positive Decimal number",transferAmountInSendersCurrency));
                    }

                    if (accountBalance.get() < transferAmountInSendersCurrency) {
                        String message = String.format("MoneyTransfer failed -  Reason: Insufficient money in Sender's Account %s", accountNumber.toString());
                        throw new InsufficientMoneyException(message);
                    }

                    LocalDateTime timeStamp = TimeManagementUtil.getCurrentTime();
                    withdraw(transferAmountInSendersCurrency+transactionFees, timeStamp);
                    recipientAccount.deposit(transferAmountInRecipientCurrency, timeStamp);
                }
        );
    }

    public void updateBalance(Double amount) {
        updateBalance(amount, TimeManagementUtil.getCurrentTime());
    }

    public void updateBalance(Double amount, LocalDateTime date) {
        atomic(() -> {

            if (amount <= 0) {
                throw new InvalidAmountException(String.format("UpdateAccountBalance failed -  Reason: Attempt to set negative money in Transacting Account %s",accountNumber.toString()));
            }

            accountBalance.set(amount);
            lastModified.set(date);

        });
    }

    public Double getAccountBalanceAmount(){
       return atomic(() -> getAccountBalance().get());
    }

}
