package io.revolut.moneytransfer.util;

import com.google.common.base.Preconditions;
import io.revolut.moneytransfer.entity.Account;
import io.revolut.moneytransfer.entity.Transaction;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.model.TransactionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.multiverse.api.StmUtils.newTxnDouble;

/**
 * Utility methods to validate data-parameters for transaction and Account Operations
 */
public class MoneyTransferAPIUtil {

    private static final Logger log = LoggerFactory.getLogger(MoneyTransferAPIUtil.class);

    /**
     * validate transactionAmount
     * throws IllegalArgumentException which eventually be handled by application code
     * @param amount
     */
    public static void validateAmount(Double amount) {

        Preconditions.checkArgument(amount != null, "Amount should have valid DecimalNumber");
        Preconditions.checkArgument(amount > 0, "Amount should be non-Zero Positive DecimalNumber");
    }

    /**
     *
     * Build AccountModel via builderPattern
     * Values are initialized using Account Object
     *
     * @param account
     * @return AccountModel
     */
    public static AccountModel getAccountModel(Account account) {

        return  AccountModel.builder().accountType(account.getAccountType())
                .accountHolderName(account.getAccountHolderName())
                .accountBalance(account.getAccountBalanceAmount())
                .accountNumber(account.getAccountNumber())
                .isActive(account.getIsActive())
                .accountType(account.getAccountType())
                .currency(account.getCurrency())
                .creationDate(account.getCreationDate())
                .updateDate(account.getUpdateDate())
                .address(account.getAddress())
                .nationalIdentityCode(account.getNationalIdentityCode()).build();
    }

    /**
     *
     * Build Account via builderPattern
     * Values are initialized using AccountModel Object
     *
     * @param accountModel
     * @return Account (Entity)
     */
    public static Account getAccount(AccountModel accountModel)  {

        Double accountBalance = accountModel.getAccountBalance()!=null ? accountModel.getAccountBalance() : 0;

       return Account.builder().accountType(accountModel.getAccountType())
            .accountHolderName(accountModel.getAccountHolderName())
            .address(accountModel.getAddress())
            .nationalIdentityCode(accountModel.getNationalIdentityCode())
            .accountBalance(newTxnDouble(accountBalance))
            .accountNumber(accountModel.getAccountNumber()).isActive(true)
            .currency(accountModel.getCurrency()).build();
    }


    /**
     *
     * Build TransactionModel via builderPattern
     * Values are initialized using Transaction Object
     *
     * @param transaction
     * @return TransactionModel
     */
    public static TransactionModel getTransactionModel(Transaction transaction)  {
        return TransactionModel.builder()
                .amount(transaction.getAmount())
                .currency(transaction.getCurrency())
                .senderBankAccountNumber(transaction.getSenderBankAccountNumber())
                .senderBankAccountCurrency(transaction.getSenderBankAccountCurrency())
                .recipientBankAccountNumber(transaction.getRecipientBankAccountNumber())
                .recipientBankAccountCurrency(transaction.getRecipientBankAccountCurrency())
                .transactionId(transaction.getTransactionId())
                .transactionStatus(transaction.getTransactionStatus())
                .transactionType(transaction.getTransactionType())
                .transactionFees(transaction.getTransactionFees())
                .exchangeRate(transaction.getExchangeRate())
                .transactionMaker(transaction.getTransactionMaker())
                .transactionRecipient(transaction.getTransactionRecipient())
                .creationDate(transaction.getCreationDate())
                .updateDate(transaction.getUpdateDate())
                .message(transaction.getMessage())
                .accountBalance(transaction.getAccountBalance())
                .errorDetails(transaction.getExceptionMessages()!=null ? String.join(",", transaction.getExceptionMessages()) : "")
                .build();
    }

    public static void sleepForTestSimualtion(long timeInMillis){
        try {
            Thread.sleep(timeInMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}