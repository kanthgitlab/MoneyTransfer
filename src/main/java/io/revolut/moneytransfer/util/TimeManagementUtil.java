package io.revolut.moneytransfer.util;

import java.time.LocalDateTime;

public class TimeManagementUtil {

    public static LocalDateTime getCurrentTime(){
        return LocalDateTime.now();
    }

}
