package io.revolut.moneytransfer.repository;

import com.google.common.annotations.VisibleForTesting;
import io.revolut.moneytransfer.constants.TransactionType;
import io.revolut.moneytransfer.entity.Transaction;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static io.revolut.moneytransfer.util.TimeManagementUtil.getCurrentTime;

public class TransactionRepository {

    private ConcurrentHashMap<String, Transaction> transactionStore = new ConcurrentHashMap<>();

    public List<Transaction> getAllTransactions(){
        return transactionStore.values().stream().collect(Collectors.toList());
    }

    public Transaction getATransactionByTransactionId(String transactionId){
        return transactionStore.getOrDefault(transactionId,null);
    }

    public List<Transaction> getAllTransactionsByAccountNumber(Integer accountNumber){
        return transactionStore.values()
                .stream()
                .filter(transaction -> (transaction.getSenderBankAccountNumber()!=null && transaction.getSenderBankAccountNumber().equals(accountNumber))
                                            || (transaction.getRecipientBankAccountNumber()!=null && transaction.getRecipientBankAccountNumber().equals(accountNumber)))
                .filter(transaction -> transaction!=null)
                .collect(Collectors.toList());
    }

    public Transaction createTransaction(Transaction transaction){

        String transactionId = generateTransactionId(transaction.getTransactionType());
        transaction.setTransactionId(transactionId);
        transaction.setCreationDate(getCurrentTime());
        transactionStore.putIfAbsent(transactionId,transaction);
        return transactionStore.get(transactionId);
    }


    public String generateTransactionId(TransactionType transactionType){
        return "TRN_"+transactionType+"_"+ UUID.randomUUID();
    }


    @VisibleForTesting
    public void cleanup(){
        transactionStore.clear();
    }
}
