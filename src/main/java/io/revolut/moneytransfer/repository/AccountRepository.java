package io.revolut.moneytransfer.repository;

import com.google.common.annotations.VisibleForTesting;
import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.entity.Account;
import io.revolut.moneytransfer.util.TimeManagementUtil;
import org.multiverse.api.StmUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static io.revolut.moneytransfer.util.TimeManagementUtil.getCurrentTime;
import static org.multiverse.api.StmUtils.atomic;

public class AccountRepository {

    private static final Logger log = LoggerFactory.getLogger(AccountRepository.class);

    private ConcurrentHashMap<Integer, Account> accountStore = new ConcurrentHashMap<Integer,Account>();

    private final AtomicInteger accountNumberGenerator = new AtomicInteger(0);

    public List<Account> getAllAccounts(){
        return Collections.unmodifiableList(accountStore.values().stream().collect(Collectors.toList()));
    }

    public List<Account> getAllActiveAccounts() {
        return getAllAccounts().stream().filter(account -> account.getIsActive()).collect(Collectors.toList());
    }

    public Account getAnAccountByAccountNumber(Integer accountNumber){
        return accountStore.getOrDefault(accountNumber,null);
    }


    public Account getAnAccountByAccountNumberAndAccountHolderName(Integer accountNumber,String accountHolderName){
        return accountStore.values().parallelStream().filter(account -> account.getAccountNumber().equals(accountNumber)
                && account.getAccountHolderName().equals(accountHolderName))
                .findFirst().orElse(null);
    }


    public Account getAnActiveAccountByAccountNumber(Integer accountNumber){
        Account account = getAnAccountByAccountNumber(accountNumber);
        return account!=null && account.getIsActive() ? account :null;
    }

    public Account getAnAccountByAccountNumberAndAccountType(Integer accountNumber, AccountType accountType){
        return accountStore.values().parallelStream().filter(account -> account.getAccountNumber().equals(accountNumber)
                && account.getAccountType().equals(accountType))
                .findFirst().orElse(null);
    }

    public Account getAnAccountByAccountHolderNameAndAccountTypeAndCurrency(String accountHolderName, AccountType accountType, Currency currency){
        return accountStore.values().parallelStream().filter(account -> account.getAccountHolderName().equalsIgnoreCase(accountHolderName)
                && account.getAccountType().equals(accountType) && account.getCurrency().equals(currency))
                .findFirst().orElse(null);
    }

    public Account create(Account account){

        atomic(() -> {
            account.setAccountNumber(accountNumberGenerator.incrementAndGet());
            account.setCreationDate(getCurrentTime());
            account.setIsActive(true);
            account.setLastModified(StmUtils.newTxnRef(TimeManagementUtil.getCurrentTime()));
            accountStore.putIfAbsent(account.getAccountNumber(), account);
        });

        return accountStore.get(account.getAccountNumber());
    }

    public Account update(Account account){
        atomic(() -> {
            account.setUpdateDate(getCurrentTime());
            account.setLastModified(StmUtils.newTxnRef(TimeManagementUtil.getCurrentTime()));
            accountStore.put(account.getAccountNumber(), account);
        });
        return accountStore.get(account.getAccountNumber());
    }

    public Boolean delete(Integer accountNumber){
        accountStore.remove(accountNumber);
        return true;
    }

    public Account deactivate(Integer accountNumber){
        Account account = accountStore.get(accountNumber);
        atomic(() -> {
            account.setIsActive(false);
            account.setUpdateDate(getCurrentTime());
            account.setLastModified(StmUtils.newTxnRef(TimeManagementUtil.getCurrentTime()));
            accountStore.put(account.getAccountNumber(), account);
        });

        return accountStore.get(account.getAccountNumber());
    }

    public Account activate(Integer accountNumber){
        Account account = accountStore.get(accountNumber);
        atomic(() -> {
            account.setIsActive(true);
            account.setUpdateDate(getCurrentTime());
            account.setLastModified(StmUtils.newTxnRef(TimeManagementUtil.getCurrentTime()));
            accountStore.put(account.getAccountNumber(), account);
        });

        return accountStore.get(account.getAccountNumber());
    }

    @VisibleForTesting
     public void cleanup(){
        accountStore.clear();
        accountNumberGenerator.getAndSet(0);
    }

}
