package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.service.AccountQueryService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


/**
 * RESTFul Controller class to query Account(s)
 */
@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountQueryController {

    private static final AccountQueryService accountQueryService = MoneyTransferAPIFactory.getAccountQueryService();

    @GET
    public Response getAllAccounts()  {

        List<AccountModel> accounts = accountQueryService.getAllAccounts();

        return accounts!=null ? Response.ok(accounts).build() : Response.noContent().build();
    }

    @GET
    @Path("/active")
    public Response getAllActiveAccounts()  {

        List<AccountModel> accounts = accountQueryService.getAllActiveAccounts();

        return accounts!=null ? Response.ok(accounts).build() : Response.noContent().build();
    }

    @GET
    @Path("/details")
    public Response getAccountDetails(@QueryParam("accountNumber") Integer accountNumber)  {

        AccountModel accountDetails = accountQueryService.getAnAccountByAccountNumber(accountNumber);

        return accountDetails!=null ? Response.ok(accountDetails).build() : Response.noContent().build();
    }
}
