package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.exception.ExceptionMapperBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Set;

@Provider
public class GenericExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    private static final Logger log = LoggerFactory.getLogger(GenericExceptionMapper.class);

    @Override
    public Response toResponse(ConstraintViolationException exception) {

        log.error("error while processing requests to MoneyTransferAPI - ConstraintViolationException Caught",exception);

        Set<?> violationsSet = exception.getConstraintViolations();

        int violationsSize = violationsSet.size();

        Object[] violations = new Object[violationsSize];

        violationsSet.toArray(violations);

        StringBuilder errorMessageString = new StringBuilder();

        if(violationsSize>0){

            for(int index = 0;index<violationsSize;index++){

                ConstraintViolation constraintViolation = (ConstraintViolation)violations[index];

                errorMessageString.append(constraintViolation.getMessage()).append("\n");
            }
       }

        Response.ResponseBuilder errorMessage = Response.serverError().type(MediaType.APPLICATION_JSON_TYPE);

        ExceptionMapperBean exceptionMapperBean = new ExceptionMapperBean("Apologies for failed transaction due to fatal-Error while processing your request. Please contact technical Support Team. \n Reason: \n"+errorMessageString.toString());

        return errorMessage.entity(exceptionMapperBean).build();
    }
}