package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.constants.TransactionStatus;
import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import io.revolut.moneytransfer.model.TransactionModel;
import io.revolut.moneytransfer.model.TransactionRequestModel;
import io.revolut.moneytransfer.service.MoneyTransferService;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * RESTFul Controller class to execute
 *  1. Deposit,Withdrawal and moneyTransfer transactions
 */
@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class MoneyTransferController {

    private static final MoneyTransferService MONEY_TRANSFER_SERVICE = MoneyTransferAPIFactory.getMoneyTransferService();

    @POST
    @Path("/deposit")
    public Response depositMoney(@Valid TransactionRequestModel transactionRequestModel)  {

        TransactionModel transactionModel = MONEY_TRANSFER_SERVICE.depositMoney(transactionRequestModel);

        return transactionModel!=null ? transactionModel.getTransactionStatus().equals(TransactionStatus.SUCCESSFUL) ?
                                            Response.ok(transactionModel).build() : Response.serverError().entity(transactionModel).build()
                                      : Response.noContent().build();
    }

    @POST
    @Path("/withdraw")
    public Response withdrawMoney(@Valid TransactionRequestModel transactionRequestModel)  {

        TransactionModel transactionModel = MONEY_TRANSFER_SERVICE.withdrawMoney(transactionRequestModel);

        return transactionModel!=null ? transactionModel.getTransactionStatus().equals(TransactionStatus.SUCCESSFUL) ?
                Response.ok(transactionModel).build() : Response.serverError().entity(transactionModel).build()
                : Response.noContent().build();
    }

    @POST
    @Path("/transfer")
    public Response transferMoney(@Valid TransactionRequestModel transactionRequestModel)  {

        TransactionModel transactionModel = MONEY_TRANSFER_SERVICE.transferMoney(transactionRequestModel);

        return transactionModel!=null ? transactionModel.getTransactionStatus().equals(TransactionStatus.SUCCESSFUL) ?
                Response.ok(transactionModel).build() : Response.serverError().entity(transactionModel).build()
                : Response.noContent().build();
    }

    @POST
    @Path("/crossTransfer")
    public Response crossTransferMoney(@Valid TransactionRequestModel transactionRequestModel)  {

        TransactionModel transactionModel = MONEY_TRANSFER_SERVICE.transferToCrossCurrencyAccount(transactionRequestModel);

        return transactionModel!=null ? transactionModel.getTransactionStatus().equals(TransactionStatus.SUCCESSFUL) ?
                Response.ok(transactionModel).build() : Response.serverError().entity(transactionModel).build()
                : Response.noContent().build();
    }


}
