package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.service.AccountOperationsService;
import io.revolut.moneytransfer.service.AccountQueryService;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * RESTFul Controller class for Account Creation, Update and Deactivation
 */
@Path(AccountCommandController.ACCOUNTS)
@Produces(MediaType.APPLICATION_JSON)
public class AccountCommandController {

    private static final AccountOperationsService ACCOUNT_OPERATIONS_SERVICE = MoneyTransferAPIFactory.getAccountOperationsService();

    private static final AccountQueryService accountQueryService = MoneyTransferAPIFactory.getAccountQueryService();

    public static final String ACCOUNTS = "/accounts";

    @POST
    public Response createAccount(AccountModel accountModel)  {

        AccountModel createdBankAccount = ACCOUNT_OPERATIONS_SERVICE.createNewAccount(accountModel);

        return createdBankAccount!=null ? Response.ok(createdBankAccount).build() : Response.noContent().build();
    }

    @PUT
    @Path("/update")
    public Response updateAccount(AccountModel accountModel)  {

        AccountModel createdBankAccount = ACCOUNT_OPERATIONS_SERVICE.updateAccountDetails(accountModel);

        return createdBankAccount!=null ? Response.ok(createdBankAccount).build() : Response.noContent().build();
    }


    @PUT
    @Path("/balanceUpdate")
    public Response updateAccountBalance(AccountModel accountModel)  {

        AccountModel account = accountQueryService.getAnAccountByAccountNumber(accountModel.getAccountNumber());

        AccountModel updatedBankAccount = ACCOUNT_OPERATIONS_SERVICE.updateAccountBalance(account.getAccountNumber(),
                                                                                account.getAccountBalance(),
                                                                                accountModel.getAccountBalance());

        return updatedBankAccount!=null ? Response.ok(updatedBankAccount).build() : Response.noContent().build();
    }

    @POST
    @Path("/deactivate")
    public Response deactivateAccount(@QueryParam("accountNumber") Integer accountNumber)  {

        AccountModel deactivatedBankAccount = ACCOUNT_OPERATIONS_SERVICE.deactivateAccount(accountNumber);

        return deactivatedBankAccount!=null ? Response.ok(deactivatedBankAccount).build() : Response.noContent().build();
    }


    @POST
    @Path("/activate")
    public Response activateAccount(@QueryParam("accountNumber") Integer accountNumber)  {

        AccountModel deactivatedBankAccount = ACCOUNT_OPERATIONS_SERVICE.activateAccount(accountNumber);

        return deactivatedBankAccount!=null ? Response.ok(deactivatedBankAccount).build() : Response.noContent().build();
    }

    @DELETE
    @Path("/delete")
    public Response deleteAccount(@QueryParam("accountNumber") Integer accountNumber)  {

        Boolean isDeletionSuccessful = ACCOUNT_OPERATIONS_SERVICE.deleteAccount(accountNumber);

        return isDeletionSuccessful!=null ? Response.ok(isDeletionSuccessful).build() : Response.noContent().build();
    }

    @DELETE
    @Path("/clear")
    public Response clearAccounts()  {

        ACCOUNT_OPERATIONS_SERVICE.deleteAllAccounts();

        return Response.ok(true).build() ;
    }
}
