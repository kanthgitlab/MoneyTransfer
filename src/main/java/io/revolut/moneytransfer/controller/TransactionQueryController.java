package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import io.revolut.moneytransfer.model.TransactionModel;
import io.revolut.moneytransfer.service.TransactionQueryService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * RESTFul Controller class to execute
 *  1. List All transactions
 *  2. filter transactions by transactionId, AccountNumber
 */
@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionQueryController {

    private static final TransactionQueryService transactionQueryService = MoneyTransferAPIFactory.getTransactionQueryService();

    @GET
    public Response getAllTransactions()  {

        List<TransactionModel> transactionModels = transactionQueryService.getAllTransactions();

        return transactionModels!=null ? Response.ok(transactionModels).build() : Response.noContent().build();
    }

    @GET
    @Path("/details")
    public Response getTransactionDetailsByTransactionId(@QueryParam("transactionId") String transactionId)  {

        TransactionModel transactionModel = transactionQueryService.getTransactionDetails(transactionId);

        return transactionModel!=null ? Response.ok(transactionModel).build() : Response.noContent().build();
    }

    @GET
    @Path("/listByAccount")
    public Response getAllTransactionsByAccountNumber(@QueryParam("accountNumber") Integer accountNumber)  {

        List<TransactionModel> transactionModels = transactionQueryService.getAllTransactionsOfAnAccount(accountNumber);

        return transactionModels!=null ? Response.ok(transactionModels).build() : Response.noContent().build();
    }

}
