package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.exception.ExceptionMapperBean;
import io.revolut.moneytransfer.exception.MoneyTransferApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class MoneyTransferExceptionMapper implements ExceptionMapper<Throwable> {

    private static final Logger log = LoggerFactory.getLogger(MoneyTransferExceptionMapper.class);

    @Override
    public Response toResponse(Throwable exception) {

        log.error("error while processing requests to MoneyTransferAPI",exception);

        ExceptionMapperBean exceptionMapperBean;

        Response.ResponseBuilder errorMessage = Response.serverError().type(MediaType.APPLICATION_JSON_TYPE);

        if (exception instanceof MoneyTransferApplicationException) {
            exceptionMapperBean =  new ExceptionMapperBean(exception.getMessage());
        } else {
            log.error("Unhandled-Exception", exception);
            exceptionMapperBean = new ExceptionMapperBean("Apologies for failed transaction due to fatal-Error while processing your request. Please contact technical Support Team \n"+exception.getMessage());
        }

        return errorMessage.entity(exceptionMapperBean).build();
    }
}
