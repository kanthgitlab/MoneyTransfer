package io.revolut.moneytransfer.constants;

public enum AccountType {

    SAVINGS, CURRENT, FIXED_DEPOSIT, TERM_DEPOSIT;
}
