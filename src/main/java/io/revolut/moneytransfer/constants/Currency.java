package io.revolut.moneytransfer.constants;

/**
 * Valid Currencies
 */
public enum Currency {
    USD("USD"), EUR("EUR") , GBP("GBP");

    private String code;

    Currency(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
