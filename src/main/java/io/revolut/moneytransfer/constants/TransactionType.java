package io.revolut.moneytransfer.constants;

public enum TransactionType {

    DEPOSIT, WITHDRAWAL, TRANSFER, BILL_PAYMENT, CHEQUE_DEPOSIT, CHEQUE_ISSUANCE;

}
