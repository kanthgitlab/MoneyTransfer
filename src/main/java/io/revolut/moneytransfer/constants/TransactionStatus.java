package io.revolut.moneytransfer.constants;

public enum  TransactionStatus {

     FAILED, SUCCESSFUL

}
