package io.revolut.moneytransfer.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.revolut.moneytransfer.configuration.ExchangeRatesConfig;
import io.revolut.moneytransfer.repository.AccountRepository;
import io.revolut.moneytransfer.repository.TransactionRepository;
import io.revolut.moneytransfer.service.AccountOperationsService;
import io.revolut.moneytransfer.service.AccountQueryService;
import io.revolut.moneytransfer.service.CurrencyConversionService;
import io.revolut.moneytransfer.service.ExchangeRateQueryService;
import io.revolut.moneytransfer.service.MoneyTransferService;
import io.revolut.moneytransfer.service.TransactionQueryService;

public final class MoneyTransferAPIFactory {

    private static AccountRepository accountRepository = null;
    private static TransactionRepository transactionRepository = null;
    private static AccountOperationsService accountOperationsService = null;
    private static AccountQueryService accountQueryService = null;
    private static MoneyTransferService moneyTransferService = null;
    private static TransactionQueryService transactionQueryService = null;
    private static ExchangeRateQueryService exchangeRateQueryService = null;
    private static CurrencyConversionService currencyConversionService = null;
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static void build(){

        objectMapper.registerModule(new JavaTimeModule())
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        objectMapper.findAndRegisterModules();

        exchangeRateQueryService = buildExchangeRateQueryService();
        exchangeRateQueryService.buildStaticRates(ExchangeRatesConfig.rates);
        currencyConversionService = buildCurrencyConversionService();
        accountRepository = buildAccountRepository();
        transactionRepository = buildTransactionRepository();
        accountQueryService = buildAccountQueryService();
        accountOperationsService = buildAccountService();
        moneyTransferService = buildTransactionService();
        transactionQueryService = buildTransactionQueryService();

    }

    private static ExchangeRateQueryService buildExchangeRateQueryService() {
        return new ExchangeRateQueryService();
    }

    private static CurrencyConversionService buildCurrencyConversionService() {
        return CurrencyConversionService.build(exchangeRateQueryService);
    }

    private static AccountRepository buildAccountRepository(){
        return new AccountRepository();
    }

    private  static TransactionRepository buildTransactionRepository(){
        return new TransactionRepository();
    }


    private static AccountQueryService buildAccountQueryService(){
        return AccountQueryService.build(accountRepository);

    }

    private static AccountOperationsService buildAccountService(){
        return AccountOperationsService.build(accountRepository,accountQueryService);

    }


    private static MoneyTransferService buildTransactionService(){
        return MoneyTransferService.build(transactionRepository,accountQueryService,accountRepository,exchangeRateQueryService,currencyConversionService);
    }


    private static TransactionQueryService buildTransactionQueryService(){
        return TransactionQueryService.build(transactionRepository);
    }

    public static AccountRepository getAccountRepository() {
        return accountRepository;
    }

    public static AccountOperationsService getAccountOperationsService() {
        return accountOperationsService;
    }

    public static AccountQueryService getAccountQueryService() {
        return accountQueryService;
    }

    public static TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public static MoneyTransferService getMoneyTransferService() {
        return moneyTransferService;
    }

    public static TransactionQueryService getTransactionQueryService() {
        return transactionQueryService;
    }

    public static CurrencyConversionService getCurrencyConversionService() {
        return currencyConversionService;
    }

    public static ExchangeRateQueryService getExchangeRateQueryService() {
        return exchangeRateQueryService;
    }
}
