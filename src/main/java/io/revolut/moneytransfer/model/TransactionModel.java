package io.revolut.moneytransfer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.constants.TransactionStatus;
import io.revolut.moneytransfer.constants.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.pojomatic.annotations.AutoProperty;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@AutoProperty
@Builder
public class TransactionModel {

    @NotNull(message = "transactionId is required for transaction")
    private String transactionId;

    @NotNull(message = "transactionMaker is required for transaction")
    private String transactionMaker;

    @NotNull(message = "transactionRecipient is required for transaction")
    private String transactionRecipient;

    private Integer senderBankAccountNumber;

    private Currency senderBankAccountCurrency;

    private Integer recipientBankAccountNumber;

    private Currency recipientBankAccountCurrency;

    @NotNull(message = "amount is required for transaction")
    private Double amount;

    @NotNull(message = "currency is required for transaction")
    private Currency currency;

    @NotNull(message = "transactionType is required for transaction")
    private TransactionType transactionType;

    private Double accountBalance;

    private Double transactionFees;

    private Double exchangeRate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime creationDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime updateDate;

    @NotNull(message = "transactionStatus is required for transaction")
    private TransactionStatus transactionStatus;

    private String message;

    private String errorDetails;
}
