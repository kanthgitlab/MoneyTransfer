package io.revolut.moneytransfer.model;

import io.revolut.moneytransfer.constants.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.pojomatic.annotations.AutoProperty;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@AutoProperty
@Builder
public class CurrencyPairModel {

    private Currency baseCurrency;
    private Currency alternateCurrency;


}
