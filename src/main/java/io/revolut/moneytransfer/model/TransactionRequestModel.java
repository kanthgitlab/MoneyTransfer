package io.revolut.moneytransfer.model;

import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.constants.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.pojomatic.annotations.AutoProperty;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@AutoProperty
public class TransactionRequestModel {

    @NotNull(message = "transactionMaker is required for transaction")
    private String transactionMaker;

    @NotNull(message = "transactionRecipient is required for transaction")
    private String transactionRecipient;

    private Integer senderBankAccountNumber;

    private Integer recipientBankAccountNumber;

    private Integer bankAccountNumber;

    @NotNull(message = "amount is required for transaction")
    private Double amount;

    @NotNull(message = "currency is required for transaction")
    private Currency currency;

    @NotNull(message = "transactionType is required for transaction")
    private TransactionType transactionType;
}
