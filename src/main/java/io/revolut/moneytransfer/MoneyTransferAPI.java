package io.revolut.moneytransfer;

import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import org.glassfish.grizzly.http.server.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.revolut.moneytransfer.configuration.MoneyTransferServerConfig.APP_BASE_URI;
import static io.revolut.moneytransfer.configuration.MoneyTransferServerConfig.ORG_PREFIX;
import static io.revolut.moneytransfer.configuration.MoneyTransferServerConfig.startServer;

public class MoneyTransferAPI {

    private static final Logger log = LoggerFactory.getLogger(MoneyTransferAPI.class);

    public static void main(String[] args) throws Exception {

        MoneyTransferAPIFactory.build();

        final HttpServer server = startServer(APP_BASE_URI + ORG_PREFIX);

        log.info("started MoneyTransferAPI Server - Press ENTER-Key in this command console to stop the server");

        System.in.read();

        log.info("About to shutdown MoneyTransferAPI Server");

        server.shutdownNow();
    }

}
