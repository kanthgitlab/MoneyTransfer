package io.revolut.moneytransfer.exception;

public class InvalidAmountException extends MoneyTransferApplicationException {

    public InvalidAmountException(String message) {
        super(message);
    }
}
