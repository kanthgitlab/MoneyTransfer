package io.revolut.moneytransfer.exception;

public class DuplicateAccountCreationExceptionMoneyTransfer extends MoneyTransferApplicationException {

    public DuplicateAccountCreationExceptionMoneyTransfer(String message) {
        super(message);
    }
}
