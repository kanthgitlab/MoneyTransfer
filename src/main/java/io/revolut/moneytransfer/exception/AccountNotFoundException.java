package io.revolut.moneytransfer.exception;

public class AccountNotFoundException extends MoneyTransferApplicationException {

    public AccountNotFoundException(String message) {
        super(message);
    }
}
