package io.revolut.moneytransfer.exception;

public class InsufficientMoneyException extends MoneyTransferApplicationException {

    public InsufficientMoneyException(String message) {
        super(message);
    }
}
