package io.revolut.moneytransfer.exception;

public class MoneyTransferApplicationException extends RuntimeException {

    public MoneyTransferApplicationException(String message) {
        super(message);
    }
}
