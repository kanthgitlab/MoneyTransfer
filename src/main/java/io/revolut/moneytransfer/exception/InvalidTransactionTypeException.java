package io.revolut.moneytransfer.exception;

public class InvalidTransactionTypeException extends MoneyTransferApplicationException {

    public InvalidTransactionTypeException(String message) {
        super(message);
    }
}
