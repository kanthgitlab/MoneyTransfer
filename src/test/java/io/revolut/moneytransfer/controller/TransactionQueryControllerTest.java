package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.configuration.MoneyTransferServerConfig;
import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.constants.TransactionType;
import io.revolut.moneytransfer.entity.Address;
import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.model.TransactionModel;
import io.revolut.moneytransfer.model.TransactionRequestModel;
import io.revolut.moneytransfer.service.AccountOperationsService;
import io.revolut.moneytransfer.service.MoneyTransferService;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static io.revolut.moneytransfer.constants.Currency.EUR;
import static io.revolut.moneytransfer.constants.Currency.USD;
import static io.revolut.moneytransfer.service.TransactionQueryService.compareByAmountDescending;
import static io.revolut.moneytransfer.service.TransactionQueryService.compareByCreationDateDescending;
import static io.revolut.moneytransfer.service.TransactionQueryService.compareByTransactionStatusDescending;
import static io.revolut.moneytransfer.service.TransactionQueryService.compareByTransactionTypeDescending;
import static io.revolut.moneytransfer.util.MoneyTransferAPIUtil.sleepForTestSimualtion;
import static io.revolut.moneytransfer.util.MoneyTransferTestComparator.compareTransactionModelForTestMethods;
import static io.revolut.moneytransfer.util.MoneyTransferTestComparator.compareTransactionModelsForTestMethods;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TransactionQueryControllerTest {

    private static final int TEST_DATA_SIMULATION_TIME_IN_MILLIS = 3;

    public static final String APP_BASE_URI = "http://localhost:8099/";
    public static final String ACCOUNT_NUMBER_QUERYPARAM = "accountNumber";
    public static final String DEACTIVATE_ACCOUNT_URI = "/deactivate";

    private static HttpServer server;
    private static WebTarget target;

    static AccountModel testAccount_Savings_EUR_1 = null;
    static AccountModel testAccount_Savings_USD_1 = null;

    static AccountModel testAccount_Current_EUR_1 = null;
    static AccountModel testAccount_Current_USD_1 = null;

    static AccountModel testAccount_Savings_EUR_2 = null;
    static AccountModel testAccount_Savings_USD_2 = null;

    static AccountModel testAccount_Current_EUR_2 = null;
    static AccountModel testAccount_Current_USD_2 = null;

    private List<TransactionModel> transactionModelList_TestData = null;

    @BeforeClass
    public static void beforeTest() {

        if(server!=null && server.isStarted()){

            do {
                server.shutdownNow();
            }while (server.isStarted());
        }

        MoneyTransferAPIFactory.build();

        // start the server
        server = MoneyTransferServerConfig.startServer(APP_BASE_URI);

        MoneyTransferAPIFactory.getAccountRepository().cleanup();
        MoneyTransferAPIFactory.getTransactionRepository().cleanup();

        // create the client
        Client clientObject = ClientBuilder.newClient();

        //set URI for client
        target = clientObject.target(APP_BASE_URI);
    }

    @Before
    public void initForEachTest(){
        prepareTestAccounts();
        prepareTransactionTestData();
    }

    @After
    public void cleanupAfterEachTest(){
        MoneyTransferAPIFactory.getAccountRepository().cleanup();
        MoneyTransferAPIFactory.getTransactionRepository().cleanup();
    }

    @AfterClass
    public static void afterTest() {
        MoneyTransferAPIFactory.getAccountRepository().cleanup();
        MoneyTransferAPIFactory.getTransactionRepository().cleanup();

        do {
            server.shutdownNow();
        }while (server.isStarted());
    }

    @Test
    public void test_Get_All_Transactions(){

        //given

        //Test Data Prepared with:

        /**
         *
         * 2 Successful Deposits
         * 1 Successful Withdrawal
         * 2 Failed Withdrawal Attempt
         * 1 Successful Money-Transfer
         *
         */

        //when
        Response httpResponse_Get_All_Transactions = target.path("/transaction").request().get();

        //then
        assertEquals(Response.Status.OK, httpResponse_Get_All_Transactions.getStatusInfo().toEnum());
        List<TransactionModel> transactionModels_Extracted = getObjectsFromEntity(httpResponse_Get_All_Transactions);

        //then
        assertNotNull(transactionModels_Extracted);
        assertEquals(5,transactionModels_Extracted.size());
        compareTransactionModelsForTestMethods(transactionModelList_TestData,transactionModels_Extracted);
    }

    @Test
    public void test_Get_All_Transactions_For_A_Specific_Account(){

        //given

        //Test Data Prepared with:

        /**
         *
         * 2 Successful Deposits
         * 1 Successful Withdrawal
         * 2 Failed Withdrawal Attempt
         * 1 Successful Money-Transfer
         *
         */

        Integer accountNumber = testAccount_Current_EUR_1.getAccountNumber();

        //when
        Response httpResponse_Get_All_Transactions = target.path("/transaction" + "/listByAccount").queryParam("accountNumber",accountNumber).request().get();

        //then
        assertEquals(Response.Status.OK, httpResponse_Get_All_Transactions.getStatusInfo().toEnum());
        List<TransactionModel> transactionModels_Extracted = getObjectsFromEntity(httpResponse_Get_All_Transactions);

        assertNotNull(transactionModels_Extracted);
        assertEquals(5,transactionModels_Extracted.size());
        compareTransactionModelsForTestMethods(transactionModelList_TestData,transactionModels_Extracted);
    }


    @Test
    public void test_Get_All_Transactions_For_A_Specific_Account_Subset(){

        //given

        //Test Data Prepared with:

        /**
         *
         * 2 Successful Deposits
         * 1 Successful Withdrawal
         * 2 Failed Withdrawal Attempt
         * 1 Successful Money-Transfer
         *
         */

        //when
        Integer accountNumber = testAccount_Savings_EUR_2.getAccountNumber();

        Response httpResponse_Get_Transactions = target.path("/transaction" + "/listByAccount").queryParam("accountNumber",accountNumber).request().get();

        //then
        assertEquals(Response.Status.OK, httpResponse_Get_Transactions.getStatusInfo().toEnum());
        List<TransactionModel> transactionModels_Extracted = getObjectsFromEntity(httpResponse_Get_Transactions);

        assertNotNull(transactionModels_Extracted);
        assertEquals(1,transactionModels_Extracted.size());

        List<TransactionModel> transactionModels_Expected = transactionModelList_TestData.stream()
                .filter(transactionModel ->
                        (transactionModel.getRecipientBankAccountNumber().equals(accountNumber)
                                &&
                                transactionModel.getRecipientBankAccountNumber().equals(accountNumber))
                ).sorted(compareByCreationDateDescending.get()
                        .thenComparing(compareByAmountDescending.get())
                        .thenComparing(compareByTransactionStatusDescending.get())
                        .thenComparing(compareByTransactionTypeDescending.get())).collect(Collectors.toList());


        compareTransactionModelsForTestMethods(transactionModels_Expected, transactionModels_Extracted);
    }

    @Test
    public void test_Get_Transaction_For_A_TransactionId(){

        //given

        //Test Data Prepared with:

        /**
         *
         * 2 Successful Deposits
         * 1 Successful Withdrawal
         * 2 Failed Withdrawal Attempt
         * 1 Successful Money-Transfer
         *
         */


        //when
        String  transactionId = transactionModelList_TestData.get(0).getTransactionId();

        Response httpResponse_Get_Transaction_By_TransactionId = target.path("/transaction" + "/details").queryParam("transactionId",transactionId).request().get();

        //then
        assertEquals(Response.Status.OK, httpResponse_Get_Transaction_By_TransactionId.getStatusInfo().toEnum());
        TransactionModel transactionModel_Extracted = getObjectFromEntity(httpResponse_Get_Transaction_By_TransactionId);

        //then
        assertNotNull(transactionModel_Extracted);
        compareTransactionModelForTestMethods(transactionModelList_TestData.get(0),transactionModel_Extracted);

    }


    /**
     * Test Data:
     *
     * 2 Successful Deposits
     * 1 Successful Withdrawal
     * 1 Failed Withdrawal Attempt
     * 1 Successful Money-Transfer
     *
     */
    public void prepareTransactionTestData(){

        //given

        MoneyTransferService moneyTransferService = MoneyTransferAPIFactory.getMoneyTransferService();

        transactionModelList_TestData = new ArrayList<>();

        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.DEPOSIT;

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_1 = moneyTransferService.depositMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_1);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        //given

        amount = 100.00;
        bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        currency = testAccount_Current_EUR_1.getCurrency();
        recipientBankAccountNumber = null;
        senderBankAccountNumber = null;
        transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        transactionType = TransactionType.DEPOSIT;

        transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_2 = moneyTransferService.depositMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_2);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        //given

        amount = 100.00;
        bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        currency = testAccount_Current_EUR_1.getCurrency();
        recipientBankAccountNumber = null;
        senderBankAccountNumber = null;
        transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        transactionType = TransactionType.WITHDRAWAL;

        transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_3 = moneyTransferService.withdrawMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_3);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        //given

        amount = 100.00;
        bankAccountNumber = null;
        currency = testAccount_Current_EUR_1.getCurrency();
        recipientBankAccountNumber = testAccount_Savings_EUR_2.getAccountNumber();
        senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        transactionRecipient = testAccount_Savings_EUR_2.getAccountHolderName();
        transactionType = TransactionType.TRANSFER;

        transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_4 = moneyTransferService.transferMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_4);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        //given

        amount = 10000.00;
        bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        currency = testAccount_Current_EUR_1.getCurrency();
        recipientBankAccountNumber = null;
        senderBankAccountNumber = null;
        transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        transactionType = TransactionType.WITHDRAWAL;

        transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_5 = moneyTransferService.withdrawMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_5);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        transactionModelList_TestData =transactionModelList_TestData.stream()
                .sorted(compareByCreationDateDescending.get().thenComparing(compareByAmountDescending.get()))
                .collect(Collectors.toList());
    }



    public static void prepareTestAccounts(){

        //given

        AccountOperationsService accountOperationsService = MoneyTransferAPIFactory.getAccountOperationsService();

        //given
        String accountHolderName_1 = "Steve";
        String unitNumber_1 = "#01-22";
        String buildingName_1 = "New Castle";
        String streetOrLandmarkName_1 = "Flora Drive";
        String postalCode_1= "5001213";
        String countryCode_1 = "SG";
        String nationalIdentityCode_1 = "SG55415G";
        Double accountBalance_1 = 1000.10;

        Address address_1 = getAddressFromParameters(unitNumber_1,buildingName_1,streetOrLandmarkName_1,postalCode_1,countryCode_1);

        AccountModel accountForCreation_Current_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_1);

        assertNotNull(testAccount_Current_EUR_1);
        assertTrue(testAccount_Current_EUR_1.getIsActive());

        AccountModel accountForCreation_Savings_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_1);

        assertNotNull(testAccount_Savings_EUR_1);
        assertTrue(testAccount_Savings_EUR_1.getIsActive());

        AccountModel accountForCreation_Current_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_1);

        assertNotNull(accountForCreation_Current_USD_1);
        assertTrue(accountForCreation_Current_USD_1.getIsActive());

        AccountModel accountForCreation_Savings_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_1);

        assertNotNull(testAccount_Savings_USD_1);
        assertTrue(testAccount_Savings_USD_1.getIsActive());

        //AccountHolder-2

        //given
        String accountHolderName_2 = "Jobs";
        String unitNumber_2 = "#32-12";
        String buildingName_2 = "Fort Canning";
        String streetOrLandmarkName_2 = "Castle Road";
        String postalCode_2= "765331";
        String countryCode_2 = "SG";
        String nationalIdentityCode_2 = "SG99076G";
        Double accountBalance_2 = 200.00;

        Address address_2 = getAddressFromParameters(unitNumber_2,buildingName_2,streetOrLandmarkName_2,postalCode_2,countryCode_2);

        AccountModel accountForCreation_Current_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_2);

        assertNotNull(testAccount_Current_EUR_2);
        assertTrue(testAccount_Current_EUR_2.getIsActive());

        AccountModel accountForCreation_Savings_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_2);

        assertNotNull(testAccount_Savings_EUR_2);
        assertTrue(testAccount_Savings_EUR_2.getIsActive());

        AccountModel accountForCreation_Current_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_2);

        assertNotNull(accountForCreation_Current_USD_2);
        assertTrue(accountForCreation_Current_USD_2.getIsActive());

        AccountModel accountForCreation_Savings_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_2);

        assertNotNull(testAccount_Savings_USD_2);
        assertTrue(testAccount_Savings_USD_2.getIsActive());

        accountOperationsService.deactivateAccount(testAccount_Savings_USD_2.getAccountNumber());
    }

    /**
     *
     * @param accountNumber
     * @param accountHolderName
     * @param address
     * @param nationalIdentityCode
     * @param accountType
     * @param accountBalance
     * @param currency
     * @param isActive
     * @param creationDate
     * @param updateDate
     * @return AccountModel
     */
    public static AccountModel getAccountModelBeanFromParameters(Integer accountNumber, String accountHolderName,
                                                                 Address address, String nationalIdentityCode,
                                                                 AccountType accountType, Double accountBalance,
                                                                 Currency currency, Boolean isActive,
                                                                 LocalDateTime creationDate, LocalDateTime updateDate){

        return AccountModel.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(accountBalance)
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();
    }

    /**
     *
     * @param unitNumber
     * @param buildingName
     * @param streetOrLandmarkName
     * @param postalCode
     * @param countryCode
     * @return Address
     */
    public static Address getAddressFromParameters( String unitNumber,
                                                    String buildingName,
                                                    String streetOrLandmarkName,
                                                    String postalCode,
                                                    String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                .postalCode(postalCode).countryCode(countryCode).build();
    }


    /**
     *
     * @param amount
     * @param bankAccountNumber
     * @param currency
     * @param recipientBankAccountNumber
     * @param senderBankAccountNumber
     * @param transactionMaker
     * @param transactionRecipient
     * @param transactionType
     * @return TransactionRequestModel
     */
    public TransactionRequestModel getATransactionRequestModelFromParameters(Double amount, Integer bankAccountNumber,
                                                                             Currency currency, Integer recipientBankAccountNumber,
                                                                             Integer senderBankAccountNumber,
                                                                             String transactionMaker,
                                                                             String transactionRecipient,
                                                                             TransactionType transactionType){

        return TransactionRequestModel.builder().amount(amount).bankAccountNumber(bankAccountNumber).currency(currency)
                .recipientBankAccountNumber(recipientBankAccountNumber).senderBankAccountNumber(senderBankAccountNumber)
                .transactionMaker(transactionMaker).transactionRecipient(transactionRecipient).transactionType(transactionType).build();
    }


    private static TransactionModel getObjectFromEntity(Response responseEntity) {
        return responseEntity.readEntity(TransactionModel.class);
    }

    private static List<TransactionModel> getObjectsFromEntity(Response responseEntity) {
        Collection<TransactionModel> transactionModels = responseEntity.readEntity(new GenericType<Collection<TransactionModel>>(){});
        return transactionModels.stream().collect(Collectors.toList());
    }

    private static Entity getEntityFromAccountObject(AccountModel accountModel) {
        return Entity.entity(accountModel, MediaType.valueOf(MediaType.APPLICATION_JSON));
    }

}
