package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.configuration.MoneyTransferServerConfig;
import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.entity.Address;
import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.service.AccountOperationsService;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.List;

import static io.revolut.moneytransfer.constants.Currency.EUR;
import static io.revolut.moneytransfer.constants.Currency.USD;
import static io.revolut.moneytransfer.controller.AccountCommandController.ACCOUNTS;
import static io.revolut.moneytransfer.util.MoneyTransferTestComparator.compareForTestMethods;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

//@Ignore
public class AccountQueryControllerTest {

    public static final String APP_BASE_URI = "http://localhost:8081/";
    public static final String ACCOUNTDETAILS_BY_ACCOUNTNUMBER = "/details";
    public static final String ACCOUNT_NUMBER = "accountNumber";
    public static final String DEACTIVATE = "/deactivate";

    private static HttpServer server = null;
    private static WebTarget target = null;

    static AccountModel testAccount_Savings_EUR_1 = null;
    static AccountModel testAccount_Savings_USD_1 = null;

    static AccountModel testAccount_Current_EUR_1 = null;
    static AccountModel testAccount_Current_USD_1 = null;

    static AccountModel testAccount_Savings_EUR_2 = null;
    static AccountModel testAccount_Savings_USD_2 = null;

    static AccountModel testAccount_Current_EUR_2 = null;
    static AccountModel testAccount_Current_USD_2 = null;

    @BeforeClass
    public static void beforeTest() {

        if(server!=null && server.isStarted()){

            do {
                server.shutdownNow();
            }while (server.isStarted());
        }

        MoneyTransferAPIFactory.build();

        // start the server
        server = MoneyTransferServerConfig.startServer(MoneyTransferServerConfig.APP_BASE_URI);

        // create the client
        Client clientObject = ClientBuilder.newClient();

        //set URI for client
        target = clientObject.target(MoneyTransferServerConfig.APP_BASE_URI);

    }

    @After
    public void cleanup_After_Each_Test(){
        MoneyTransferAPIFactory.getAccountRepository().cleanup();
    }

    @AfterClass
    public static void afterTest() {

        do {
            server.shutdownNow();
        }while (server.isStarted());

    }

    @Test
    public void test_Get_All_Accounts(){

        //given
        prepareTestAccounts();


        //when
        Response httpResponse_Get_All_Accounts = target.path(ACCOUNTS).request().get();

        //then
        assertEquals(Response.Status.OK, httpResponse_Get_All_Accounts.getStatusInfo().toEnum());
        List<AccountModel> accountModels_Extracted = getObjectsFromEntity(httpResponse_Get_All_Accounts);
        assertNotNull(accountModels_Extracted);
        assertEquals(8,accountModels_Extracted.size());
    }


    @Test
    public void test_Get_All_Active_Accounts(){

        //given
        prepareTestAccounts();

        //when
        Response httpResponse_Get_All_Accounts = target.path(ACCOUNTS + "/active").request().get();

        //then
        assertEquals(Response.Status.OK, httpResponse_Get_All_Accounts.getStatusInfo().toEnum());
        List<AccountModel> accountModels_Extracted = getObjectsFromEntity(httpResponse_Get_All_Accounts);
        assertNotNull(accountModels_Extracted);
        assertEquals(7,accountModels_Extracted.size());
    }

    @Test
    public void test_Get_AccountDetails_By_AccountNumber(){

        //given
        prepareTestAccounts();

        Integer accountNumber = testAccount_Current_EUR_1.getAccountNumber();

        //when
        Response httpResponse_Get_Account_Details = target.path(ACCOUNTS + "/details").queryParam(ACCOUNT_NUMBER,accountNumber).request().get();

        //then
        assertEquals(Response.Status.OK, httpResponse_Get_Account_Details.getStatusInfo().toEnum());
        AccountModel accountModel_Extracted = getObjectFromEntity(httpResponse_Get_Account_Details);
        assertNotNull(accountModel_Extracted);
        assertEquals(accountNumber,accountModel_Extracted.getAccountNumber());
        compareForTestMethods(testAccount_Current_EUR_1,accountModel_Extracted);
    }

    @Test
    public void test_Get_Null_AccountDetails_For_A_NonExistent_AccountNumber(){

        //given
        prepareTestAccounts();

        Integer accountNumber = 1000;

        //when
        Response httpResponse_Get_Account_Details = target.path(ACCOUNTS + "/details").queryParam(ACCOUNT_NUMBER,accountNumber).request().get();

        //then
        assertEquals(Response.Status.NO_CONTENT, httpResponse_Get_Account_Details.getStatusInfo().toEnum());
        AccountModel accountModel_Extracted = getObjectFromEntity(httpResponse_Get_Account_Details);
        assertNull(accountModel_Extracted);
    }


    public void prepareTestAccounts(){

        AccountOperationsService accountOperationsService = MoneyTransferAPIFactory.getAccountOperationsService();

        //given
        String accountHolderName_1 = "Steve";
        String unitNumber_1 = "#01-22";
        String buildingName_1 = "New Castle";
        String streetOrLandmarkName_1 = "Flora Drive";
        String postalCode_1= "5001213";
        String countryCode_1 = "SG";
        String nationalIdentityCode_1 = "SG55415G";
        Double accountBalance_1 = 1000.10;

        Address address_1 = getAddressFromParameters(unitNumber_1,buildingName_1,streetOrLandmarkName_1,postalCode_1,countryCode_1);

        AccountModel accountForCreation_Current_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_1);

        assertNotNull(testAccount_Current_EUR_1);
        assertTrue(testAccount_Current_EUR_1.getIsActive());

        AccountModel accountForCreation_Savings_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_1);

        assertNotNull(testAccount_Savings_EUR_1);
        assertTrue(testAccount_Savings_EUR_1.getIsActive());

        AccountModel accountForCreation_Current_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_1);

        assertNotNull(accountForCreation_Current_USD_1);
        assertTrue(accountForCreation_Current_USD_1.getIsActive());

        AccountModel accountForCreation_Savings_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_1);

        assertNotNull(testAccount_Savings_USD_1);
        assertTrue(testAccount_Savings_USD_1.getIsActive());

        //AccountHolder-2

        //given
        String accountHolderName_2 = "Jobs";
        String unitNumber_2 = "#32-12";
        String buildingName_2 = "Fort Canning";
        String streetOrLandmarkName_2 = "Castle Road";
        String postalCode_2= "765331";
        String countryCode_2 = "SG";
        String nationalIdentityCode_2 = "SG99076G";
        Double accountBalance_2 = 200.00;

        Address address_2 = getAddressFromParameters(unitNumber_2,buildingName_2,streetOrLandmarkName_2,postalCode_2,countryCode_2);

        AccountModel accountForCreation_Current_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_2);

        assertNotNull(testAccount_Current_EUR_2);
        assertTrue(testAccount_Current_EUR_2.getIsActive());

        AccountModel accountForCreation_Savings_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_2);

        assertNotNull(testAccount_Savings_EUR_2);
        assertTrue(testAccount_Savings_EUR_2.getIsActive());

        AccountModel accountForCreation_Current_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_2);

        assertNotNull(accountForCreation_Current_USD_2);
        assertTrue(accountForCreation_Current_USD_2.getIsActive());

        AccountModel accountForCreation_Savings_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_2);

        assertNotNull(testAccount_Savings_USD_2);
        assertTrue(testAccount_Savings_USD_2.getIsActive());

        accountOperationsService.deactivateAccount(testAccount_Savings_USD_2.getAccountNumber());
    }

    /**
     *
     * @param accountNumber
     * @param accountHolderName
     * @param address
     * @param nationalIdentityCode
     * @param accountType
     * @param accountBalance
     * @param currency
     * @param isActive
     * @param creationDate
     * @param updateDate
     * @return AccountModel
     */
    public static AccountModel getAccountModelBeanFromParameters(Integer accountNumber, String accountHolderName,
                                                          Address address, String nationalIdentityCode,
                                                          AccountType accountType, Double accountBalance,
                                                          Currency currency, Boolean isActive,
                                                          LocalDateTime creationDate, LocalDateTime updateDate){

        return AccountModel.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(accountBalance)
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();

    }

    /**
     *
     * @param unitNumber
     * @param buildingName
     * @param streetOrLandmarkName
     * @param postalCode
     * @param countryCode
     * @return Address
     */
    public static Address getAddressFromParameters( String unitNumber,
                                             String buildingName,
                                             String streetOrLandmarkName,
                                             String postalCode,
                                             String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                .postalCode(postalCode).countryCode(countryCode).build();
    }


    private Response getByAccountNumber(Integer accountNumber) {
        return target.path(ACCOUNTS + ACCOUNTDETAILS_BY_ACCOUNTNUMBER).queryParam(ACCOUNT_NUMBER,accountNumber).request().get();
    }

    private static Entity getEntityFromAccountObject(AccountModel accountModel) {
        return Entity.entity(accountModel, MediaType.valueOf(MediaType.APPLICATION_JSON));
    }

    private static AccountModel getObjectFromEntity(Response responseEntity) {
        return responseEntity.readEntity(AccountModel.class);
    }

    private static List<AccountModel> getObjectsFromEntity(Response responseEntity) {
        return responseEntity.readEntity(List.class);
    }

    private static Boolean getBooleanFromResponseEntity(Response responseEntity) {
        return responseEntity.readEntity(Boolean.class);
    }
}
