package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.configuration.MoneyTransferServerConfig;
import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.constants.TransactionStatus;
import io.revolut.moneytransfer.constants.TransactionType;
import io.revolut.moneytransfer.entity.Account;
import io.revolut.moneytransfer.entity.Address;
import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.model.TransactionModel;
import io.revolut.moneytransfer.model.TransactionRequestModel;
import io.revolut.moneytransfer.util.TimeManagementUtil;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.List;

import static io.revolut.moneytransfer.constants.Currency.EUR;
import static io.revolut.moneytransfer.constants.Currency.USD;
import static io.revolut.moneytransfer.constants.TransactionType.WITHDRAWAL;
import static io.revolut.moneytransfer.controller.AccountCommandController.ACCOUNTS;
import static io.revolut.moneytransfer.service.MoneyTransferService.TRANSACTION_FEE_PERCENTAGE_AMOUNT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MoneyTransferControllerTest {

    public static final String APP_BASE_URI = "http://localhost:9092/";
    public static final String ACCOUNT_NUMBER_QUERYPARAM = "accountNumber";
    public static final String DEACTIVATE_ACCOUNT_URI = "/deactivate";
    public static final String TRANSACTION_URI = "/transaction";
    public static final String DEPOSIT_URI = "/deposit";
    private static final String WITHDRAWAL_URI = "/withdraw";
    private static final String TRANSFER_URI = "/transfer";
    private static final String CROSS_CURRENCY_TRANSFER_URI = "/crossTransfer";

    private static HttpServer server;
    private static WebTarget target;

    static AccountModel testAccount_Savings_EUR_1 = null;
    static AccountModel testAccount_Savings_USD_1 = null;

    static AccountModel testAccount_Current_EUR_1 = null;
    static AccountModel testAccount_Current_USD_1 = null;

    static AccountModel testAccount_Savings_EUR_2 = null;
    static AccountModel testAccount_Savings_USD_2 = null;

    static AccountModel testAccount_Current_EUR_2 = null;
    static AccountModel testAccount_Current_USD_2 = null;

    @BeforeClass
    public static void beforeTest() {

        if(server!=null && server.isStarted()){

            do {
                server.shutdownNow();
            }while (server.isStarted());
        }

        MoneyTransferAPIFactory.build();

        // start the server
        server = MoneyTransferServerConfig.startServer(APP_BASE_URI);

        MoneyTransferAPIFactory.getAccountRepository().cleanup();
        MoneyTransferAPIFactory.getTransactionRepository().cleanup();

        // create the client
        Client clientObject = ClientBuilder.newClient();

        //set URI for client
        target = clientObject.target(APP_BASE_URI);

    }

    @Before
    public void initForEachTest(){
        prepareTestAccounts();
    }

    @After
    public void cleanupAfterEachTest(){
        MoneyTransferAPIFactory.getAccountRepository().cleanup();
        MoneyTransferAPIFactory.getTransactionRepository().cleanup();
    }

    @AfterClass
    public static void afterTest() {
        MoneyTransferAPIFactory.getAccountRepository().cleanup();
        MoneyTransferAPIFactory.getTransactionRepository().cleanup();

        do {
            server.shutdownNow();
        }while (server.isStarted());

    }

    @Test
    public void test_Successful_Deposit_Money_Transaction(){

        //given
        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.DEPOSIT;

        Double accountBalance_Before_Transaction = testAccount_Current_EUR_1.getAccountBalance();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        Double accountBalance_After_Transaction_Expected = accountBalance_Before_Transaction + amount;

        //when
        Response depositTransaction_HttpResponse = target.path(TRANSACTION_URI + DEPOSIT_URI).request().post(getEntityFromTransactionObject(transactionRequestModel));

        TransactionModel transactionModel_Response_Actual = getObjectFromEntity(depositTransaction_HttpResponse);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(bankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.SUCCESSFUL,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());

        assertEquals(accountBalance_After_Transaction_Expected,
                MoneyTransferAPIFactory.getAccountRepository().getAnAccountByAccountNumber(transactionModel_Response_Actual.getRecipientBankAccountNumber()).getAccountBalanceAmount());
    }

    @Test
    public void test_Failed_Deposit_Money_Transaction_From_Mismatching_DepositCurrency(){

        //given
        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = USD;
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.DEPOSIT;

        Double accountBalance_Before_Transaction = testAccount_Current_EUR_1.getAccountBalance();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);


        //when
        Response depositTransaction_HttpResponse = target.path(TRANSACTION_URI + DEPOSIT_URI).request().post(getEntityFromTransactionObject(transactionRequestModel));

        TransactionModel transactionModel_Response_Actual = getObjectFromEntity(depositTransaction_HttpResponse);


        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(bankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.FAILED,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(accountBalance_Before_Transaction,transactionModel_Response_Actual.getAccountBalance());
        assertNotNull(transactionModel_Response_Actual.getMessage());
        assertEquals("FAILED Deposit Transaction for amount: 100.0 EUR to BankAccountNumber: 1",transactionModel_Response_Actual.getMessage());
    }

    @Test
    public void test_Successful_Withdraw_Money_Transaction(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = WITHDRAWAL;

        Double accountBalance_Before_Transaction = testAccount_Current_EUR_1.getAccountBalance();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        Double accountBalance_After_Transaction_Expected = accountBalance_Before_Transaction - amount;

        //when
        Response withdrawalTransaction_HttpResponse = target.path(TRANSACTION_URI + WITHDRAWAL_URI).request().post(getEntityFromTransactionObject(transactionRequestModel));

        TransactionModel transactionModel_Response_Actual = getObjectFromEntity(withdrawalTransaction_HttpResponse);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(bankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.SUCCESSFUL,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());
    }

    @Test
    public void test_Failed_Withdraw_Money_Transaction_From_Mismatching_WithdrawalCurrency(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = USD;
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.WITHDRAWAL;

        Double accountBalance_Before_Transaction = testAccount_Current_EUR_1.getAccountBalance();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        Response withdrawalTransaction_HttpResponse = target.path(TRANSACTION_URI + WITHDRAWAL_URI).request().post(getEntityFromTransactionObject(transactionRequestModel));

        TransactionModel transactionModel_Response_Actual = getObjectFromEntity(withdrawalTransaction_HttpResponse);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(bankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.FAILED,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(accountBalance_Before_Transaction,transactionModel_Response_Actual.getAccountBalance());
        assertNotNull(transactionModel_Response_Actual.getMessage());
        assertEquals("FAILED Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1",transactionModel_Response_Actual.getMessage());
    }


    @Test
    public void test_Successful_Money_Transfer_Transaction_Distinct_AccountHolders(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = null;
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = testAccount_Savings_EUR_2.getAccountNumber();
        Integer senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Savings_EUR_2.getAccountHolderName();
        TransactionType transactionType = TransactionType.TRANSFER;

        Double recipient_AccountBalance_Before_Transaction = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber).getAccountBalanceAmount();
        Double recipient_AccountBalance_After_Transaction = recipient_AccountBalance_Before_Transaction + amount;

        Double sender_accountBalance_Before_Transaction = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber).getAccountBalanceAmount();
        Double sender_accountBalance_After_Transaction_Expected = sender_accountBalance_Before_Transaction - amount;

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        Response transferTransaction_HttpResponse = target.path(TRANSACTION_URI + TRANSFER_URI).request().post(getEntityFromTransactionObject(transactionRequestModel));

        TransactionModel transactionModel_Response_Actual = getObjectFromEntity(transferTransaction_HttpResponse);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertEquals(senderBankAccountNumber,transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(recipientBankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.SUCCESSFUL,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());

        //Assert SenderAccountBalance and reconcile with TransactionResponseObject
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(sender_accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());
        Account senderAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber);
        assertEquals(sender_accountBalance_After_Transaction_Expected,senderAccountDetails_For_Assertion.getAccountBalanceAmount());
        assertEquals(transactionModel_Response_Actual.getAccountBalance(),senderAccountDetails_For_Assertion.getAccountBalanceAmount());

        //Assert RecipientAccountBalance
        Account recipientAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber);
        assertNotNull(recipientAccountDetails_For_Assertion);
        assertEquals(recipient_AccountBalance_After_Transaction,recipientAccountDetails_For_Assertion.getAccountBalanceAmount());
    }



    @Test
    public void test_Successful_Money_Transfer_Transaction_SelfTransfer_Current_To_Savings(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = null;
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = testAccount_Savings_EUR_1.getAccountNumber();
        Integer senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Savings_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.TRANSFER;

        Double recipient_AccountBalance_Before_Transaction = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber).getAccountBalanceAmount();
        Double recipient_AccountBalance_After_Transaction = recipient_AccountBalance_Before_Transaction + amount;

        Double sender_accountBalance_Before_Transaction = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber).getAccountBalanceAmount();
        Double sender_accountBalance_After_Transaction_Expected = sender_accountBalance_Before_Transaction - amount;

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        Response transferTransaction_HttpResponse = target.path(TRANSACTION_URI + TRANSFER_URI).request().post(getEntityFromTransactionObject(transactionRequestModel));

        TransactionModel transactionModel_Response_Actual = getObjectFromEntity(transferTransaction_HttpResponse);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertEquals(senderBankAccountNumber,transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(recipientBankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.SUCCESSFUL,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());

        //Assert SenderAccountBalance and reconcile with TransactionResponseObject
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(sender_accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());
        Account senderAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber);
        assertEquals(sender_accountBalance_After_Transaction_Expected,senderAccountDetails_For_Assertion.getAccountBalanceAmount());
        assertEquals(transactionModel_Response_Actual.getAccountBalance(),senderAccountDetails_For_Assertion.getAccountBalanceAmount());

        //Assert RecipientAccountBalance
        Account recipientAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber);
        assertNotNull(recipientAccountDetails_For_Assertion);
        assertEquals(recipient_AccountBalance_After_Transaction,recipientAccountDetails_For_Assertion.getAccountBalanceAmount());
    }

    @Test
    public void test_Successful_CrossCurrency_Money_Transfer_Transaction(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = null;
        Currency currency = testAccount_Savings_USD_1.getCurrency();
        Integer recipientBankAccountNumber = testAccount_Savings_USD_1.getAccountNumber();
        Integer senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Savings_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.TRANSFER;

        Double recipient_AccountBalance_Before_Transaction = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber).getAccountBalanceAmount();
        Double recipient_AccountBalance_After_Transaction = recipient_AccountBalance_Before_Transaction + amount;

        Double sender_accountBalance_Before_Transaction = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber).getAccountBalanceAmount();

        Double exchangeRate = MoneyTransferAPIFactory.getExchangeRateQueryService().getExchangeRate(currency,testAccount_Current_EUR_1.getCurrency());
        Double transferAmountInSendersAccountCurrency = MoneyTransferAPIFactory.getCurrencyConversionService().convert(amount,exchangeRate);
        Double transactionFees = transferAmountInSendersAccountCurrency * exchangeRate * TRANSACTION_FEE_PERCENTAGE_AMOUNT;
        Double sender_accountBalance_After_Transaction_Expected = sender_accountBalance_Before_Transaction - transferAmountInSendersAccountCurrency - transactionFees;

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        Response transferTransaction_HttpResponse = target.path(TRANSACTION_URI + CROSS_CURRENCY_TRANSFER_URI).request().post(getEntityFromTransactionObject(transactionRequestModel));

        TransactionModel transactionModel_Response_Actual = getObjectFromEntity(transferTransaction_HttpResponse);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertEquals(senderBankAccountNumber,transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(recipientBankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.SUCCESSFUL,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());

        //Assert SenderAccountBalance and reconcile with TransactionResponseObject
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(sender_accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());
        Account senderAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber);
        assertEquals(sender_accountBalance_After_Transaction_Expected,senderAccountDetails_For_Assertion.getAccountBalanceAmount());
        assertEquals(transactionModel_Response_Actual.getAccountBalance(),senderAccountDetails_For_Assertion.getAccountBalanceAmount());

        //Assert RecipientAccountBalance
        Account recipientAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber);
        assertNotNull(recipientAccountDetails_For_Assertion);
        assertEquals(recipient_AccountBalance_After_Transaction,recipientAccountDetails_For_Assertion.getAccountBalanceAmount());
    }


    @Test
    public void test_Failed_Money_Transfer_Transaction_InSufficient_Balance_Scenario(){

        //given

        Double amount = 3000.00;
        Integer bankAccountNumber = null;
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = testAccount_Savings_EUR_1.getAccountNumber();
        Integer senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Savings_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.TRANSFER;

        Double recipient_AccountBalance_After_Transaction_Expected = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber).getAccountBalanceAmount();

        Double sender_accountBalance_After_Transaction_Expected = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber).getAccountBalanceAmount();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        Response transferTransaction_HttpResponse = target.path(TRANSACTION_URI + TRANSFER_URI).request().post(getEntityFromTransactionObject(transactionRequestModel));

        TransactionModel transactionModel_Response_Actual = getObjectFromEntity(transferTransaction_HttpResponse);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertEquals(senderBankAccountNumber,transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(recipientBankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.FAILED,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());

        //Assert SenderAccountBalance and reconcile with TransactionResponseObject
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(sender_accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());
        Account senderAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber);
        assertEquals(sender_accountBalance_After_Transaction_Expected,senderAccountDetails_For_Assertion.getAccountBalanceAmount());
        assertEquals(transactionModel_Response_Actual.getAccountBalance(),senderAccountDetails_For_Assertion.getAccountBalanceAmount());

        //Assert RecipientAccountBalance
        Account recipientAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber);
        assertNotNull(recipientAccountDetails_For_Assertion);
        assertEquals(recipient_AccountBalance_After_Transaction_Expected,recipientAccountDetails_For_Assertion.getAccountBalanceAmount());
    }


    @Test
    public void test_Invalid_Money_Transfer_Transaction_Same_Sender_And_Recipient(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = null;
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Integer senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Savings_EUR_2.getAccountHolderName();
        TransactionType transactionType = TransactionType.TRANSFER;

        Double recipient_AccountBalance_Before_Transaction = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber).getAccountBalanceAmount();

        Double sender_accountBalance_Before_Transaction = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber).getAccountBalanceAmount();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        Response transferTransaction_HttpResponse = target.path(TRANSACTION_URI + TRANSFER_URI).request().post(getEntityFromTransactionObject(transactionRequestModel));

        TransactionModel transactionModel_Response_Actual = getObjectFromEntity(transferTransaction_HttpResponse);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertEquals(senderBankAccountNumber,transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(recipientBankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.FAILED,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());

        //Assert SenderAccountBalance and reconcile with TransactionResponseObject
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(sender_accountBalance_Before_Transaction,transactionModel_Response_Actual.getAccountBalance());
        Account senderAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(senderBankAccountNumber);
        assertEquals(transactionModel_Response_Actual.getAccountBalance(),senderAccountDetails_For_Assertion.getAccountBalanceAmount());

        //Assert RecipientAccountBalance
        Account recipientAccountDetails_For_Assertion = MoneyTransferAPIFactory.getAccountRepository().getAnActiveAccountByAccountNumber(recipientBankAccountNumber);
        assertNotNull(recipientAccountDetails_For_Assertion);
        assertEquals(recipient_AccountBalance_Before_Transaction,recipientAccountDetails_For_Assertion.getAccountBalanceAmount());

        assertNotNull(transactionModel_Response_Actual.getMessage());
        assertEquals("FAILED MoneyTransfer Transaction for amount: 100.0 EUR from senderBankAccountNumber: 1 to recipientBankAccountNumber: 1",transactionModel_Response_Actual.getMessage());
    }

    public static void prepareTestAccounts(){

        //given

        //AccountHolder-1

        String accountHolderName_1 = "Rod Hammer";
        String unitNumber_1 = "#01-22";
        String buildingName_1 = "New Castle";
        String streetOrLandmarkName_1 = "Flora Drive";
        String postalCode_1= "5001213";
        String countryCode_1 = "SG";
        String nationalIdentityCode_1 = "SG55415G";
        Double accountBalance_1 = 1000.10;

        //Current - EUR Account for AccountHolder-1

        Address address_1 = getAddressFromParameters(unitNumber_1,buildingName_1,streetOrLandmarkName_1,postalCode_1,countryCode_1);

        AccountModel accountForCreation_Current_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        Response accountCreationHttpResponse_Current_EUR_1 = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation_Current_EUR_1));

        assertEquals(Response.Status.OK, accountCreationHttpResponse_Current_EUR_1.getStatusInfo().toEnum());

        testAccount_Current_EUR_1 = accountCreationHttpResponse_Current_EUR_1.readEntity(AccountModel.class);
        assertNotNull(testAccount_Current_EUR_1);
        assertTrue(testAccount_Current_EUR_1.getIsActive());
        assertNotNull(testAccount_Current_EUR_1.getCreationDate());
        assertEquals(EUR,testAccount_Current_EUR_1.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_EUR_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Current_EUR_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Current_EUR_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Current_EUR_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Current_EUR_1.getAccountHolderName());
        assertEquals(Integer.valueOf(1),testAccount_Current_EUR_1.getAccountNumber());

        //Savings - EUR Account for AccountHolder-1

        AccountModel accountForCreation_Savings_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        Response accountCreationHttpResponse_Savings_EUR_1 = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation_Savings_EUR_1));

        assertEquals(Response.Status.OK, accountCreationHttpResponse_Savings_EUR_1.getStatusInfo().toEnum());

        testAccount_Savings_EUR_1 = accountCreationHttpResponse_Savings_EUR_1.readEntity(AccountModel.class);
        assertNotNull(testAccount_Savings_EUR_1);
        assertTrue(testAccount_Savings_EUR_1.getIsActive());
        assertNotNull(testAccount_Savings_EUR_1.getCreationDate());
        assertEquals(EUR,testAccount_Savings_EUR_1.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_EUR_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Savings_EUR_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Savings_EUR_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Savings_EUR_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Savings_EUR_1.getAccountHolderName());
        assertEquals(Integer.valueOf(2),testAccount_Savings_EUR_1.getAccountNumber());

        //Current - USD Account for AccountHolder-1

        AccountModel accountForCreation_Current_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        Response accountCreationHttpResponse_Current_USD_1 = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation_Current_USD_1));

        assertEquals(Response.Status.OK, accountCreationHttpResponse_Current_USD_1.getStatusInfo().toEnum());

        testAccount_Current_USD_1 = accountCreationHttpResponse_Current_USD_1.readEntity(AccountModel.class);

        assertNotNull(accountForCreation_Current_USD_1);
        assertTrue(accountForCreation_Current_USD_1.getIsActive());
        assertNotNull(testAccount_Current_USD_1.getCreationDate());
        assertEquals(USD,testAccount_Current_USD_1.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_USD_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Current_USD_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Current_USD_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Current_USD_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Current_USD_1.getAccountHolderName());
        assertEquals(Integer.valueOf(3),testAccount_Current_USD_1.getAccountNumber());

        //Savings - USD Account for AccountHolder-1

        AccountModel accountForCreation_Savings_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        Response accountCreationHttpResponse_Savings_USD_1 = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation_Savings_USD_1));

        assertEquals(Response.Status.OK, accountCreationHttpResponse_Savings_USD_1.getStatusInfo().toEnum());

        testAccount_Savings_USD_1 = accountCreationHttpResponse_Savings_USD_1.readEntity(AccountModel.class);

        assertNotNull(testAccount_Savings_USD_1);
        assertTrue(testAccount_Savings_USD_1.getIsActive());
        assertNotNull(testAccount_Savings_USD_1.getCreationDate());
        assertEquals(USD,testAccount_Savings_USD_1.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_USD_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Savings_USD_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Savings_USD_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Savings_USD_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Savings_USD_1.getAccountHolderName());
        assertEquals(Integer.valueOf(4),testAccount_Savings_USD_1.getAccountNumber());


        //given

        //AccountHolder-2
        String accountHolderName_2 = "MadRush";
        String unitNumber_2 = "#32-12";
        String buildingName_2 = "Fort Canning";
        String streetOrLandmarkName_2 = "Castle Road";
        String postalCode_2= "765331";
        String countryCode_2 = "SG";
        String nationalIdentityCode_2 = "SG99076G";
        Double accountBalance_2 = 200.00;

        Address address_2 = getAddressFromParameters(unitNumber_2,buildingName_2,streetOrLandmarkName_2,postalCode_2,countryCode_2);

        //Current - EUR Account for AccountHolder-2

        AccountModel accountForCreation_Current_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        Response accountCreationHttpResponse_Current_EUR_2 = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation_Current_EUR_2));

        assertEquals(Response.Status.OK, accountCreationHttpResponse_Current_EUR_2.getStatusInfo().toEnum());

        testAccount_Current_EUR_2 = accountCreationHttpResponse_Current_EUR_2.readEntity(AccountModel.class);

        assertNotNull(testAccount_Current_EUR_2);
        assertTrue(testAccount_Current_EUR_2.getIsActive());
        assertNotNull(testAccount_Current_EUR_2.getCreationDate());
        assertEquals(EUR,testAccount_Current_EUR_2.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_EUR_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Current_EUR_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Current_EUR_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Current_EUR_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Current_EUR_2.getAccountHolderName());
        assertEquals(Integer.valueOf(5),testAccount_Current_EUR_2.getAccountNumber());

        //Savings - EUR Account for AccountHolder-2

        AccountModel accountForCreation_Savings_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                EUR,Boolean.TRUE,null,null);


        Response accountCreationHttpResponse_Savings_EUR_2 = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation_Savings_EUR_2));

        assertEquals(Response.Status.OK, accountCreationHttpResponse_Savings_EUR_2.getStatusInfo().toEnum());

        testAccount_Savings_EUR_2 = accountCreationHttpResponse_Savings_EUR_2.readEntity(AccountModel.class);

        assertNotNull(testAccount_Savings_EUR_2);
        assertTrue(testAccount_Savings_EUR_2.getIsActive());
        assertNotNull(testAccount_Savings_EUR_2.getCreationDate());
        assertEquals(EUR,testAccount_Savings_EUR_2.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_EUR_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Savings_EUR_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Savings_EUR_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Savings_EUR_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Savings_EUR_2.getAccountHolderName());
        assertEquals(Integer.valueOf(6),testAccount_Savings_EUR_2.getAccountNumber());

        //Current - USD Account for AccountHolder-2

        AccountModel accountForCreation_Current_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        Response accountCreationHttpResponse_Current_USD_2 = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation_Current_USD_2));

        assertEquals(Response.Status.OK, accountCreationHttpResponse_Current_USD_2.getStatusInfo().toEnum());

        testAccount_Current_USD_2 = accountCreationHttpResponse_Current_USD_2.readEntity(AccountModel.class);

        assertNotNull(accountForCreation_Current_USD_2);
        assertTrue(accountForCreation_Current_USD_2.getIsActive());
        assertNotNull(testAccount_Current_USD_2.getCreationDate());
        assertEquals(USD,testAccount_Current_USD_2.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_USD_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Current_USD_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Current_USD_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Current_USD_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Current_USD_2.getAccountHolderName());
        assertEquals(Integer.valueOf(7),testAccount_Current_USD_2.getAccountNumber());

        //Savings - USD Account for AccountHolder-2

        AccountModel accountForCreation_Savings_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        Response accountCreationHttpResponse_Savings_USD_2 = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation_Savings_USD_2));

        assertEquals(Response.Status.OK, accountCreationHttpResponse_Savings_USD_2.getStatusInfo().toEnum());

        testAccount_Savings_USD_2 = accountCreationHttpResponse_Savings_USD_2.readEntity(AccountModel.class);

        assertNotNull(testAccount_Savings_USD_2);
        assertTrue(testAccount_Savings_USD_2.getIsActive());
        assertNotNull(testAccount_Savings_USD_2.getCreationDate());
        assertEquals(USD,testAccount_Savings_USD_2.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_USD_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Savings_USD_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Savings_USD_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Savings_USD_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Savings_USD_2.getAccountHolderName());
        assertEquals(Integer.valueOf(8),testAccount_Savings_USD_2.getAccountNumber());

        Response accountDeactivationHttpResponse = target.path(ACCOUNTS + DEACTIVATE_ACCOUNT_URI).queryParam(ACCOUNT_NUMBER_QUERYPARAM,testAccount_Savings_USD_2.getAccountNumber()).request().post(null);

    }


    /**
     *
     * @param accountNumber
     * @param accountHolderName
     * @param address
     * @param nationalIdentityCode
     * @param accountType
     * @param accountBalance
     * @param currency
     * @param isActive
     * @param creationDate
     * @param updateDate
     * @return AccountModel
     */
    public static AccountModel getAccountModelBeanFromParameters(Integer accountNumber, String accountHolderName,
                                                                 Address address, String nationalIdentityCode,
                                                                 AccountType accountType, Double accountBalance,
                                                                 Currency currency, Boolean isActive,
                                                                 LocalDateTime creationDate, LocalDateTime updateDate){

        return AccountModel.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(accountBalance)
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();

    }

    /**
     *
     * @param unitNumber
     * @param buildingName
     * @param streetOrLandmarkName
     * @param postalCode
     * @param countryCode
     * @return Address
     */
    public static Address getAddressFromParameters( String unitNumber,
                                                    String buildingName,
                                                    String streetOrLandmarkName,
                                                    String postalCode,
                                                    String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                .postalCode(postalCode).countryCode(countryCode).build();
    }


    /**
     *
     * @param amount
     * @param bankAccountNumber
     * @param currency
     * @param recipientBankAccountNumber
     * @param senderBankAccountNumber
     * @param transactionMaker
     * @param transactionRecipient
     * @param transactionType
     * @return TransactionRequestModel
     */
    public TransactionRequestModel getATransactionRequestModelFromParameters(Double amount, Integer bankAccountNumber,
                                                                             Currency currency, Integer recipientBankAccountNumber,
                                                                             Integer senderBankAccountNumber,
                                                                             String transactionMaker,
                                                                             String transactionRecipient,
                                                                             TransactionType transactionType){

        return TransactionRequestModel.builder().amount(amount).bankAccountNumber(bankAccountNumber).currency(currency)
                .recipientBankAccountNumber(recipientBankAccountNumber).senderBankAccountNumber(senderBankAccountNumber)
                .transactionMaker(transactionMaker).transactionRecipient(transactionRecipient).transactionType(transactionType).build();
    }


    private static Entity getEntityFromTransactionObject(TransactionRequestModel transactionRequestModel) {
        return Entity.entity(transactionRequestModel, MediaType.valueOf(MediaType.APPLICATION_JSON));
    }

    private static TransactionModel getObjectFromEntity(Response responseEntity) {
        return responseEntity.readEntity(TransactionModel.class);
    }

    private static List<TransactionModel> getObjectsFromEntity(Response responseEntity) {
        return responseEntity.readEntity(List.class);
    }

    private static Boolean getBooleanFromResponseEntity(Response responseEntity) {
        return responseEntity.readEntity(Boolean.class);
    }


    private static Entity getEntityFromAccountObject(AccountModel accountModel) {
        return Entity.entity(accountModel, MediaType.valueOf(MediaType.APPLICATION_JSON));
    }

}
