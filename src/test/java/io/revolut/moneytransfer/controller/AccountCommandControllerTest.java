package io.revolut.moneytransfer.controller;

import io.revolut.moneytransfer.configuration.MoneyTransferServerConfig;
import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.entity.Address;
import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import io.revolut.moneytransfer.model.AccountModel;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;

import static io.revolut.moneytransfer.controller.AccountCommandController.ACCOUNTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class AccountCommandControllerTest {

    Logger log = LoggerFactory.getLogger(AccountCommandControllerTest.class);

    public static final String APP_BASE_URI = "http://localhost:9090/";
    public static final String ACCOUNTDETAILS_BY_ACCOUNTNUMBER = "/details";
    public static final String ACCOUNT_NUMBER = "accountNumber";
    public static final String DEACTIVATE = "/deactivate";
    private static final String ACTIVATE = "/activate";
    private static final String DELETE_ACCOUNT = "/delete";
    public static final String BALANCE_UPDATE = "/balanceUpdate";
    public static final String UPDATE = "/update";

    private static HttpServer server;
    private static WebTarget target;

    @Before
    public void beforeTest() throws Exception{

        if(server!=null && server.isStarted()){

            do {
                server.shutdownNow();
            }while (server.isStarted());
        }

        MoneyTransferAPIFactory.build();

        MoneyTransferAPIFactory.getAccountRepository().cleanup();
        MoneyTransferAPIFactory.getTransactionRepository().cleanup();

        // start the server
        server = MoneyTransferServerConfig.startServer(APP_BASE_URI);

        // create the client
        Client clientObject = ClientBuilder.newClient();

        //set URI for client
        target = clientObject.target(APP_BASE_URI);

        Response accountCreationHttpResponse = target.path(ACCOUNTS + "/clear").request().delete();
    }

    @After
    public void afterTest() throws Exception{
        MoneyTransferAPIFactory.getAccountRepository().cleanup();
        target.path(ACCOUNTS + "/clear").request().delete();

        do {
            server.shutdownNow();
        }while (server.isStarted());

    }


    @Test
    public void test_Create_New_Account() {

        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Response accountCreationHttpResponse = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation));

        assertEquals(Response.Status.OK, accountCreationHttpResponse.getStatusInfo().toEnum());

        AccountModel accountModel_Created = accountCreationHttpResponse.readEntity(AccountModel.class);
        assertNotNull(accountModel_Created);

    }

    @Test
    public void test_Update_Account_Details() {

        String accountHolderName = "Jhonson";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Response accountCreationHttpResponse = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation));

        assertEquals(Response.Status.OK, accountCreationHttpResponse.getStatusInfo().toEnum());

        AccountModel accountModel_Created = accountCreationHttpResponse.readEntity(AccountModel.class);
        assertNotNull(accountModel_Created);
        log.info("Account created: {}",accountModel_Created);
        log.info("is Started: {}",server.isStarted());


        //when
         unitNumber = "#32-43";
         buildingName = "Old Castle";
         streetOrLandmarkName = "Jasmine Drive";
         postalCode= "500987";

        address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForUpdate = getAccountModelBeanFromParameters(accountModel_Created.getAccountNumber(),accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,accountModel_Created.getCreationDate(),null);

        Response accountUpdateHttpResponse = target.path(ACCOUNTS + UPDATE).request().put(getEntityFromAccountObject(accountForUpdate));


        assertEquals(Response.Status.OK, accountUpdateHttpResponse.getStatusInfo().toEnum());

        AccountModel accountModel_Updated = accountUpdateHttpResponse.readEntity(AccountModel.class);
        assertNotNull(accountModel_Updated);
    }

    @Test
    public void test_Update_Account_Balance() {

        String accountHolderName = "Cameron";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Response accountCreationHttpResponse = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation));

        assertEquals(Response.Status.OK, accountCreationHttpResponse.getStatusInfo().toEnum());

        AccountModel accountModel_Created = accountCreationHttpResponse.readEntity(AccountModel.class);
        assertNotNull(accountModel_Created);



        //when

        accountBalance = 3000.908;

        AccountModel accountForUpdate = getAccountModelBeanFromParameters(accountModel_Created.getAccountNumber(),accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,accountModel_Created.getCreationDate(),null);

        Response accountUpdateHttpResponse = target.path(ACCOUNTS + BALANCE_UPDATE).request().put(getEntityFromAccountObject(accountForUpdate));


        assertEquals(Response.Status.OK, accountUpdateHttpResponse.getStatusInfo().toEnum());

        AccountModel accountModel_Updated = accountUpdateHttpResponse.readEntity(AccountModel.class);
        assertNotNull(accountModel_Updated);
        assertFalse(accountModel_Created.getAccountBalance().equals(accountModel_Updated.getAccountBalance()));
        assertEquals(accountBalance,accountModel_Updated.getAccountBalance());

    }

    @Test
    public void test_Delete_New_Account() {

        String accountHolderName = "Ramesh";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        //prepare testData for Account Creation
        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        //prepare Account TestData via API Call
        Response accountCreationHttpResponse = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation));

        assertEquals(Response.Status.OK, accountCreationHttpResponse.getStatusInfo().toEnum());

        AccountModel accountModel_Created = accountCreationHttpResponse.readEntity(AccountModel.class);
        assertNotNull(accountModel_Created);


        //when
        Response accountDeactivationHttpResponse = target.path(ACCOUNTS + DELETE_ACCOUNT).queryParam(ACCOUNT_NUMBER,accountModel_Created.getAccountNumber()).request().delete();

        //then
        Boolean deleteSucessIndicator = getBooleanFromResponseEntity(accountDeactivationHttpResponse);
        assertTrue(deleteSucessIndicator);
    }

    @Test
    public void test_Deactivate_New_Account() {

        String accountHolderName = "Martin";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        //prepare testData for Account Creation
        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        //prepare Account TestData via API Call
        Response accountCreationHttpResponse = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation));

        assertEquals(Response.Status.OK, accountCreationHttpResponse.getStatusInfo().toEnum());

        AccountModel accountModel_Created = accountCreationHttpResponse.readEntity(AccountModel.class);
        assertNotNull(accountModel_Created);

        //when
        Response accountDeactivationHttpResponse = target.path(ACCOUNTS + DEACTIVATE).queryParam(ACCOUNT_NUMBER,accountModel_Created.getAccountNumber()).request().post(null);

        //then
        AccountModel accountModel_Queried = getObjectFromEntity(accountDeactivationHttpResponse);
        assertNotNull(accountModel_Queried);
        assertFalse(accountModel_Queried.getIsActive());
    }

    @Test
    public void test_Activate_An_InActive_Account() {

        String accountHolderName = "Jobs";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        //prepare testData for Account Creation
        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        //prepare Account TestData via API Call
        Response accountCreationHttpResponse = target.path(ACCOUNTS).request().post(getEntityFromAccountObject(accountForCreation));

        assertEquals(Response.Status.OK, accountCreationHttpResponse.getStatusInfo().toEnum());

        AccountModel accountModel_Created = accountCreationHttpResponse.readEntity(AccountModel.class);
        assertNotNull(accountModel_Created);

        //deactivate the Account
        Response accountDeactivationHttpResponse = target.path(ACCOUNTS + DEACTIVATE).queryParam(ACCOUNT_NUMBER,accountModel_Created.getAccountNumber()).request().post(null);

        //when
        Response accountActivationHttpResponse = target.path(ACCOUNTS + ACTIVATE).queryParam(ACCOUNT_NUMBER,accountModel_Created.getAccountNumber()).request().post(null);

        //then
        AccountModel accountModel_Queried = getObjectFromEntity(accountActivationHttpResponse);
        assertNotNull(accountModel_Queried);
        assertTrue(accountModel_Queried.getIsActive());
    }


    private static Response getByAccountNumber(Integer accountNumber) {
        return target.path(ACCOUNTS + ACCOUNTDETAILS_BY_ACCOUNTNUMBER).queryParam(ACCOUNT_NUMBER,accountNumber).request().get();
    }

    private static Entity getEntityFromAccountObject(AccountModel accountModel) {
        return Entity.entity(accountModel, MediaType.valueOf(MediaType.APPLICATION_JSON));
    }

    private static AccountModel getObjectFromEntity(Response responseEntity) {
        return responseEntity.readEntity(AccountModel.class);
    }

    private static Boolean getBooleanFromResponseEntity(Response responseEntity) {
        return responseEntity.readEntity(Boolean.class);
    }

    public AccountModel getAccountModelBeanFromParameters(Integer accountNumber, String accountHolderName,
                                                          Address address, String nationalIdentityCode,
                                                          AccountType accountType, Double accountBalance,
                                                          Currency currency, Boolean isActive,
                                                          LocalDateTime creationDate, LocalDateTime updateDate){

        return AccountModel.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(accountBalance)
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();

    }

    public Address getAddressFromParameters( String unitNumber,
                                             String buildingName,
                                             String streetOrLandmarkName,
                                             String postalCode,
                                             String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                .postalCode(postalCode).countryCode(countryCode).build();
    }

}
