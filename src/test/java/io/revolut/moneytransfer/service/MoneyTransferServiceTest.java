package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.constants.TransactionStatus;
import io.revolut.moneytransfer.constants.TransactionType;
import io.revolut.moneytransfer.entity.Account;
import io.revolut.moneytransfer.entity.Address;
import io.revolut.moneytransfer.entity.Transaction;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.model.TransactionModel;
import io.revolut.moneytransfer.model.TransactionRequestModel;
import io.revolut.moneytransfer.repository.AccountRepository;
import io.revolut.moneytransfer.repository.TransactionRepository;
import io.revolut.moneytransfer.util.TimeManagementUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.List;

import static io.revolut.moneytransfer.constants.Currency.EUR;
import static io.revolut.moneytransfer.constants.Currency.USD;
import static io.revolut.moneytransfer.util.TimeManagementUtil.getCurrentTime;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Test Class to verify MoneyTransferService
 * <ul>
 * <li>Deposit-Money</li>
 * <li>Withdraw-Money</li>
 * <li>Transfer-Money</li>
 * </ul>
 */
public class MoneyTransferServiceTest {

    Logger log = LoggerFactory.getLogger(MoneyTransferServiceTest.class);

    private AccountRepository accountRepository = null;

    private TransactionRepository transactionRepository = null;

    private AccountOperationsService accountOperationsService = null;

    private AccountQueryService accountQueryService = null;

    private MoneyTransferService moneyTransferService = null;

    private ExchangeRateQueryService exchangeRateQueryService = null;

    private CurrencyConversionService currencyConversionService = null;

    private AccountModel testAccount_Savings_EUR_1 = null;
    private AccountModel testAccount_Savings_USD_1 = null;

    private AccountModel testAccount_Current_EUR_1 = null;
    private AccountModel testAccount_Current_USD_1 = null;

    private AccountModel testAccount_Savings_EUR_2 = null;
    private AccountModel testAccount_Savings_USD_2 = null;

    private AccountModel testAccount_Current_EUR_2 = null;
    private AccountModel testAccount_Current_USD_2 = null;


    @Before
    public void init(){
        accountRepository = new AccountRepository();
        AccountQueryService accountQueryService = AccountQueryService.build(accountRepository);
        accountOperationsService = AccountOperationsService.build(accountRepository,accountQueryService);
        accountQueryService = AccountQueryService.build(accountRepository);
        transactionRepository = new TransactionRepository();

        ExchangeRateQueryService exchangeRateQueryService = new ExchangeRateQueryService();
        currencyConversionService = CurrencyConversionService.build(exchangeRateQueryService);
        moneyTransferService = MoneyTransferService.build(transactionRepository,accountQueryService,accountRepository,exchangeRateQueryService,currencyConversionService);

        prepareTestAccounts();
    }

    @After
    public void teardown(){
        accountRepository.cleanup();
        accountRepository = null;
        transactionRepository.cleanup();
        transactionRepository = null;
        accountQueryService = null;
        accountQueryService = null;
        moneyTransferService = null;

        testAccount_Savings_EUR_1 = null;
        testAccount_Savings_USD_1 = null;
        testAccount_Current_EUR_1 = null;
        testAccount_Current_USD_1 = null;
        testAccount_Savings_EUR_2 = null;
        testAccount_Savings_USD_2 = null;
        testAccount_Current_EUR_2 = null;
        testAccount_Current_USD_2 = null;
    }

    @Test
    public void test_Successful_Deposit_Money_Transaction(){
        
        //given

        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.DEPOSIT;

        Double accountBalance_Before_Transaction = testAccount_Current_EUR_1.getAccountBalance();


        TransactionRequestModel transactionRequestModel = 
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        Double accountBalance_After_Transaction_Expected = accountBalance_Before_Transaction + amount;


        //when
        TransactionModel transactionModel_Response_Actual = moneyTransferService.depositMoney(transactionRequestModel);
        
        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(bankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.SUCCESSFUL,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());

        assertEquals(accountBalance_After_Transaction_Expected,accountRepository.getAnAccountByAccountNumber(transactionModel_Response_Actual.getRecipientBankAccountNumber()).getAccountBalanceAmount());
    }



    @Test
    public void test_Failed_Deposit_Money_Transaction_From_Mismatching_DepositCurrency(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = USD;
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.DEPOSIT;

        Double accountBalance_Before_Transaction = testAccount_Current_EUR_1.getAccountBalance();


        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);


        //when
        TransactionModel transactionModel_Response_Actual = moneyTransferService.depositMoney(transactionRequestModel);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(bankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.FAILED,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(accountBalance_Before_Transaction,transactionModel_Response_Actual.getAccountBalance());
        assertNotNull(transactionModel_Response_Actual.getMessage());
        assertEquals("FAILED Deposit Transaction for amount: 100.0 EUR to BankAccountNumber: 1",transactionModel_Response_Actual.getMessage());
    }


    @Test
    public void test_Successful_Withdraw_Money_Transaction(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.WITHDRAWAL;

        Double accountBalance_Before_Transaction = testAccount_Current_EUR_1.getAccountBalance();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        Double accountBalance_After_Transaction_Expected = accountBalance_Before_Transaction - amount;

        //when
        TransactionModel transactionModel_Response_Actual = moneyTransferService.withdrawMoney(transactionRequestModel);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(bankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.SUCCESSFUL,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());
    }

    @Test
    public void test_Failed_Withdraw_Money_Transaction_From_Mismatching_WithdrawalCurrency(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = USD;
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.WITHDRAWAL;

        Double accountBalance_Before_Transaction = testAccount_Current_EUR_1.getAccountBalance();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_Actual = moneyTransferService.withdrawMoney(transactionRequestModel);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(bankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.FAILED,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(accountBalance_Before_Transaction,transactionModel_Response_Actual.getAccountBalance());
        assertNotNull(transactionModel_Response_Actual.getMessage());
        assertEquals("FAILED Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1",transactionModel_Response_Actual.getMessage());
    }


    @Test
    public void test_Successful_Money_Transfer_Transaction_Distinct_AccountHolders(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = null;
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = testAccount_Savings_EUR_2.getAccountNumber();
        Integer senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Savings_EUR_2.getAccountHolderName();
        TransactionType transactionType = TransactionType.TRANSFER;

        Double recipient_AccountBalance_Before_Transaction = accountRepository.getAnActiveAccountByAccountNumber(recipientBankAccountNumber).getAccountBalanceAmount();
        Double recipient_AccountBalance_After_Transaction = recipient_AccountBalance_Before_Transaction + amount;

        Double sender_accountBalance_Before_Transaction = accountRepository.getAnActiveAccountByAccountNumber(senderBankAccountNumber).getAccountBalanceAmount();
        Double sender_accountBalance_After_Transaction_Expected = sender_accountBalance_Before_Transaction - amount;

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_Actual = moneyTransferService.transferMoney(transactionRequestModel);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertEquals(senderBankAccountNumber,transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(recipientBankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.SUCCESSFUL,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());

        //Assert SenderAccountBalance and reconcile with TransactionResponseObject
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(sender_accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());
        Account senderAccountDetails_For_Assertion = accountRepository.getAnActiveAccountByAccountNumber(senderBankAccountNumber);
        assertEquals(sender_accountBalance_After_Transaction_Expected,senderAccountDetails_For_Assertion.getAccountBalanceAmount());
        assertEquals(transactionModel_Response_Actual.getAccountBalance(),senderAccountDetails_For_Assertion.getAccountBalanceAmount());

        //Assert RecipientAccountBalance
        Account recipientAccountDetails_For_Assertion = accountRepository.getAnActiveAccountByAccountNumber(recipientBankAccountNumber);
        assertNotNull(recipientAccountDetails_For_Assertion);
        assertEquals(recipient_AccountBalance_After_Transaction,recipientAccountDetails_For_Assertion.getAccountBalanceAmount());
    }

    @Test
    public void test_Successful_Money_Transfer_Transaction_SelfTransfer_Current_To_Savings(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = null;
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = testAccount_Savings_EUR_1.getAccountNumber();
        Integer senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Savings_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.TRANSFER;

        Double recipient_AccountBalance_Before_Transaction = accountRepository.getAnActiveAccountByAccountNumber(recipientBankAccountNumber).getAccountBalanceAmount();
        Double recipient_AccountBalance_After_Transaction = recipient_AccountBalance_Before_Transaction + amount;

        Double sender_accountBalance_Before_Transaction = accountRepository.getAnActiveAccountByAccountNumber(senderBankAccountNumber).getAccountBalanceAmount();
        Double sender_accountBalance_After_Transaction_Expected = sender_accountBalance_Before_Transaction - amount;

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_Actual = moneyTransferService.transferMoney(transactionRequestModel);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertEquals(senderBankAccountNumber,transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(recipientBankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.SUCCESSFUL,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());

        //Assert SenderAccountBalance and reconcile with TransactionResponseObject
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(sender_accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());
        Account senderAccountDetails_For_Assertion = accountRepository.getAnActiveAccountByAccountNumber(senderBankAccountNumber);
        assertEquals(sender_accountBalance_After_Transaction_Expected,senderAccountDetails_For_Assertion.getAccountBalanceAmount());
        assertEquals(transactionModel_Response_Actual.getAccountBalance(),senderAccountDetails_For_Assertion.getAccountBalanceAmount());

        //Assert RecipientAccountBalance
        Account recipientAccountDetails_For_Assertion = accountRepository.getAnActiveAccountByAccountNumber(recipientBankAccountNumber);
        assertNotNull(recipientAccountDetails_For_Assertion);
        assertEquals(recipient_AccountBalance_After_Transaction,recipientAccountDetails_For_Assertion.getAccountBalanceAmount());
    }


    @Test
    public void test_Failed_Money_Transfer_Transaction_InSufficient_Balance_Scenario(){

        //given

        Double amount = 3000.00;
        Integer bankAccountNumber = null;
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = testAccount_Savings_EUR_1.getAccountNumber();
        Integer senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Savings_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.TRANSFER;

        Double recipient_AccountBalance_After_Transaction_Expected = accountRepository.getAnActiveAccountByAccountNumber(recipientBankAccountNumber).getAccountBalanceAmount();

        Double sender_accountBalance_After_Transaction_Expected = accountRepository.getAnActiveAccountByAccountNumber(senderBankAccountNumber).getAccountBalanceAmount();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_Actual = moneyTransferService.transferMoney(transactionRequestModel);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertEquals(senderBankAccountNumber,transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(recipientBankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.FAILED,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());

        //Assert SenderAccountBalance and reconcile with TransactionResponseObject
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(sender_accountBalance_After_Transaction_Expected,transactionModel_Response_Actual.getAccountBalance());
        Account senderAccountDetails_For_Assertion = accountRepository.getAnActiveAccountByAccountNumber(senderBankAccountNumber);
        assertEquals(sender_accountBalance_After_Transaction_Expected,senderAccountDetails_For_Assertion.getAccountBalanceAmount());
        assertEquals(transactionModel_Response_Actual.getAccountBalance(),senderAccountDetails_For_Assertion.getAccountBalanceAmount());

        //Assert RecipientAccountBalance
        Account recipientAccountDetails_For_Assertion = accountRepository.getAnActiveAccountByAccountNumber(recipientBankAccountNumber);
        assertNotNull(recipientAccountDetails_For_Assertion);
        assertEquals(recipient_AccountBalance_After_Transaction_Expected,recipientAccountDetails_For_Assertion.getAccountBalanceAmount());
    }

    @Test
    public void test_Invalid_Money_Transfer_Transaction_Same_Sender_And_Recipient(){

        //given

        Double amount = 100.00;
        Integer bankAccountNumber = null;
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Integer senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Savings_EUR_2.getAccountHolderName();
        TransactionType transactionType = TransactionType.TRANSFER;

        Double recipient_AccountBalance_Before_Transaction = accountRepository.getAnActiveAccountByAccountNumber(recipientBankAccountNumber).getAccountBalanceAmount();

        Double sender_accountBalance_Before_Transaction = accountRepository.getAnActiveAccountByAccountNumber(senderBankAccountNumber).getAccountBalanceAmount();

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_Actual = moneyTransferService.transferMoney(transactionRequestModel);

        //then
        assertNotNull(transactionModel_Response_Actual);
        assertNotNull(transactionModel_Response_Actual.getTransactionId());
        assertNotNull(transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertEquals(senderBankAccountNumber,transactionModel_Response_Actual.getSenderBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertEquals(recipientBankAccountNumber,transactionModel_Response_Actual.getRecipientBankAccountNumber());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertNotNull(transactionModel_Response_Actual.getTransactionMaker());
        assertEquals(transactionMaker,transactionModel_Response_Actual.getTransactionMaker());
        assertNotNull(transactionModel_Response_Actual.getTransactionRecipient());
        assertEquals(transactionRecipient,transactionModel_Response_Actual.getTransactionRecipient());
        assertNotNull(transactionModel_Response_Actual.getCurrency());
        assertEquals(currency,transactionModel_Response_Actual.getCurrency());
        assertNotNull(transactionModel_Response_Actual.getAmount());
        assertEquals(amount,transactionModel_Response_Actual.getAmount());
        assertNotNull(transactionModel_Response_Actual.getTransactionType());
        assertEquals(transactionType,transactionModel_Response_Actual.getTransactionType());
        assertNotNull(transactionModel_Response_Actual.getTransactionStatus());
        assertEquals(TransactionStatus.FAILED,transactionModel_Response_Actual.getTransactionStatus());
        assertNotNull(transactionModel_Response_Actual.getCreationDate());
        assertEquals(TimeManagementUtil.getCurrentTime().toLocalDate(),transactionModel_Response_Actual.getCreationDate().toLocalDate());

        //Assert SenderAccountBalance and reconcile with TransactionResponseObject
        assertNotNull(transactionModel_Response_Actual.getAccountBalance());
        assertEquals(sender_accountBalance_Before_Transaction,transactionModel_Response_Actual.getAccountBalance());
        Account senderAccountDetails_For_Assertion = accountRepository.getAnActiveAccountByAccountNumber(senderBankAccountNumber);
        assertEquals(transactionModel_Response_Actual.getAccountBalance(),senderAccountDetails_For_Assertion.getAccountBalanceAmount());

        //Assert RecipientAccountBalance
        Account recipientAccountDetails_For_Assertion = accountRepository.getAnActiveAccountByAccountNumber(recipientBankAccountNumber);
        assertNotNull(recipientAccountDetails_For_Assertion);
        assertEquals(recipient_AccountBalance_Before_Transaction,recipientAccountDetails_For_Assertion.getAccountBalanceAmount());

        assertNotNull(transactionModel_Response_Actual.getMessage());
        assertEquals("FAILED MoneyTransfer Transaction for amount: 100.0 EUR from senderBankAccountNumber: 1 to recipientBankAccountNumber: 1",transactionModel_Response_Actual.getMessage());
    }



    public void prepareTestAccounts(){

        //given
        String accountHolderName_1 = "Steve";
        String unitNumber_1 = "#01-22";
        String buildingName_1 = "New Castle";
        String streetOrLandmarkName_1 = "Flora Drive";
        String postalCode_1= "5001213";
        String countryCode_1 = "SG";
        String nationalIdentityCode_1 = "SG55415G";
        Double accountBalance_1 = 1000.10;

        Address address_1 = getAddressFromParameters(unitNumber_1,buildingName_1,streetOrLandmarkName_1,postalCode_1,countryCode_1);

        AccountModel accountForCreation_Current_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_1);

        assertNotNull(testAccount_Current_EUR_1);
        assertTrue(testAccount_Current_EUR_1.getIsActive());
        assertNotNull(testAccount_Current_EUR_1.getCreationDate());
        assertEquals(EUR,testAccount_Current_EUR_1.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_EUR_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Current_EUR_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Current_EUR_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Current_EUR_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Current_EUR_1.getAccountHolderName());
        assertEquals(Integer.valueOf(1),testAccount_Current_EUR_1.getAccountNumber());

        AccountModel accountForCreation_Savings_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_1);

        assertNotNull(testAccount_Savings_EUR_1);
        assertTrue(testAccount_Savings_EUR_1.getIsActive());
        assertNotNull(testAccount_Savings_EUR_1.getCreationDate());
        assertEquals(EUR,testAccount_Savings_EUR_1.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_EUR_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Savings_EUR_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Savings_EUR_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Savings_EUR_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Savings_EUR_1.getAccountHolderName());
        assertEquals(Integer.valueOf(2),testAccount_Savings_EUR_1.getAccountNumber());


        AccountModel accountForCreation_Current_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_1);

        assertNotNull(accountForCreation_Current_USD_1);
        assertTrue(accountForCreation_Current_USD_1.getIsActive());
        assertNotNull(testAccount_Current_USD_1.getCreationDate());
        assertEquals(USD,testAccount_Current_USD_1.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_USD_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Current_USD_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Current_USD_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Current_USD_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Current_USD_1.getAccountHolderName());
        assertEquals(Integer.valueOf(3),testAccount_Current_USD_1.getAccountNumber());

        AccountModel accountForCreation_Savings_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_1);

        assertNotNull(testAccount_Savings_USD_1);
        assertTrue(testAccount_Savings_USD_1.getIsActive());
        assertNotNull(testAccount_Savings_USD_1.getCreationDate());
        assertEquals(USD,testAccount_Savings_USD_1.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_USD_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Savings_USD_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Savings_USD_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Savings_USD_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Savings_USD_1.getAccountHolderName());
        assertEquals(Integer.valueOf(4),testAccount_Savings_USD_1.getAccountNumber());


        //AccountHolder-2

        //given
        String accountHolderName_2 = "Jobs";
        String unitNumber_2 = "#32-12";
        String buildingName_2 = "Fort Canning";
        String streetOrLandmarkName_2 = "Castle Road";
        String postalCode_2= "765331";
        String countryCode_2 = "SG";
        String nationalIdentityCode_2 = "SG99076G";
        Double accountBalance_2 = 200.00;

        Address address_2 = getAddressFromParameters(unitNumber_2,buildingName_2,streetOrLandmarkName_2,postalCode_2,countryCode_2);

        AccountModel accountForCreation_Current_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_2);

        assertNotNull(testAccount_Current_EUR_2);
        assertTrue(testAccount_Current_EUR_2.getIsActive());
        assertNotNull(testAccount_Current_EUR_2.getCreationDate());
        assertEquals(EUR,testAccount_Current_EUR_2.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_EUR_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Current_EUR_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Current_EUR_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Current_EUR_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Current_EUR_2.getAccountHolderName());
        assertEquals(Integer.valueOf(5),testAccount_Current_EUR_2.getAccountNumber());

        AccountModel accountForCreation_Savings_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_2);

        assertNotNull(testAccount_Savings_EUR_2);
        assertTrue(testAccount_Savings_EUR_2.getIsActive());
        assertNotNull(testAccount_Savings_EUR_2.getCreationDate());
        assertEquals(EUR,testAccount_Savings_EUR_2.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_EUR_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Savings_EUR_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Savings_EUR_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Savings_EUR_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Savings_EUR_2.getAccountHolderName());
        assertEquals(Integer.valueOf(6),testAccount_Savings_EUR_2.getAccountNumber());


        AccountModel accountForCreation_Current_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_2);

        assertNotNull(accountForCreation_Current_USD_2);
        assertTrue(accountForCreation_Current_USD_2.getIsActive());
        assertNotNull(testAccount_Current_USD_2.getCreationDate());
        assertEquals(USD,testAccount_Current_USD_2.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_USD_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Current_USD_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Current_USD_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Current_USD_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Current_USD_2.getAccountHolderName());
        assertEquals(Integer.valueOf(7),testAccount_Current_USD_2.getAccountNumber());

        AccountModel accountForCreation_Savings_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_2);

        assertNotNull(testAccount_Savings_USD_2);
        assertTrue(testAccount_Savings_USD_2.getIsActive());
        assertNotNull(testAccount_Savings_USD_2.getCreationDate());
        assertEquals(USD,testAccount_Savings_USD_2.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_USD_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Savings_USD_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Savings_USD_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Savings_USD_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Savings_USD_2.getAccountHolderName());
        assertEquals(Integer.valueOf(8),testAccount_Savings_USD_2.getAccountNumber());

    }

    /**
     *
     * @param amount
     * @param bankAccountNumber
     * @param currency
     * @param recipientBankAccountNumber
     * @param senderBankAccountNumber
     * @param transactionMaker
     * @param transactionRecipient
     * @param transactionType
     * @return TransactionRequestModel
     */
    public TransactionRequestModel getATransactionRequestModelFromParameters(Double amount,Integer bankAccountNumber,
                                                                             Currency currency,Integer recipientBankAccountNumber,
                                                                             Integer senderBankAccountNumber,
                                                                             String transactionMaker,
                                                                             String transactionRecipient,
                                                                                TransactionType transactionType){

        return TransactionRequestModel.builder().amount(amount).bankAccountNumber(bankAccountNumber).currency(currency)
                .recipientBankAccountNumber(recipientBankAccountNumber).senderBankAccountNumber(senderBankAccountNumber)
                .transactionMaker(transactionMaker).transactionRecipient(transactionRecipient).transactionType(transactionType).build();
    }

    /**
     *
     * @param transactionId
     * @param transactionMaker
     * @param transactionRecipient
     * @param transactionStatus
     * @param transactionType
     * @return TransactionModel
     */
    public TransactionModel getATransactionModel(String transactionId, String transactionMaker, String transactionRecipient, TransactionStatus transactionStatus, TransactionType transactionType){

        return TransactionModel.builder().transactionId(transactionId).transactionMaker(transactionMaker).transactionRecipient(transactionRecipient)
                .transactionStatus(transactionStatus).transactionType(transactionType).build();


    }

    /**
     *
     * @param transactionMaker
     * @param transactionRecipient
     * @param senderBankAccountNumber
     * @param recipientBankAccountNumber
     * @param amount
     * @param currency
     * @param transactionType
     * @param creationDate
     * @param updateDate
     * @param transactionStatus
     * @param exceptionMessages
     * @param message
     * @return Transaction
     */
    public Transaction getATransactionEntityFromParameters(String transactionMaker,
                                                           String transactionRecipient, Integer senderBankAccountNumber,
                                                           Integer recipientBankAccountNumber, Double amount,
                                                           Currency currency, TransactionType transactionType,
                                                           LocalDateTime creationDate, LocalDateTime updateDate,
                                                           TransactionStatus transactionStatus, List<String> exceptionMessages, String message){

        Transaction transactionObject = new Transaction();
        transactionObject.setAmount(amount);
        transactionObject.setSenderBankAccountNumber(senderBankAccountNumber);
        transactionObject.setRecipientBankAccountNumber(recipientBankAccountNumber);
        transactionObject.setCreationDate(getCurrentTime());
        transactionObject.setCurrency(currency);
        transactionObject.setTransactionType(transactionType);
        transactionObject.setTransactionStatus(transactionStatus);
        transactionObject.setTransactionMaker(transactionMaker);
        transactionObject.setTransactionRecipient(transactionRecipient);
        transactionObject.setCreationDate(creationDate);
        transactionObject.setUpdateDate(updateDate);
        transactionObject.setExceptionMessages(exceptionMessages);
        transactionObject.setMessage(message);

        return transactionObject;
    }

    /**
     *
     * @param accountNumber
     * @param accountHolderName
     * @param address
     * @param nationalIdentityCode
     * @param accountType
     * @param accountBalance
     * @param currency
     * @param isActive
     * @param creationDate
     * @param updateDate
     * @return AccountModel
     */
    public AccountModel getAccountModelBeanFromParameters(Integer accountNumber, String accountHolderName,
                                                          Address address, String nationalIdentityCode,
                                                          AccountType accountType, Double accountBalance,
                                                          Currency currency, Boolean isActive,
                                                          LocalDateTime creationDate, LocalDateTime updateDate){

        return AccountModel.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(accountBalance)
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();

    }

    /**
     *
     * @param unitNumber
     * @param buildingName
     * @param streetOrLandmarkName
     * @param postalCode
     * @param countryCode
     * @return Address
     */
    public Address getAddressFromParameters( String unitNumber,
                                             String buildingName,
                                             String streetOrLandmarkName,
                                             String postalCode,
                                             String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                .postalCode(postalCode).countryCode(countryCode).build();
    }

}
