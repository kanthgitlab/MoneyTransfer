package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.entity.Address;
import io.revolut.moneytransfer.factory.MoneyTransferAPIFactory;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.repository.AccountRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

import static io.revolut.moneytransfer.constants.Currency.EUR;
import static io.revolut.moneytransfer.constants.Currency.USD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class AccountQueryServiceTest {

    private AccountQueryService accountQueryService = null;

    private AccountRepository accountRepository = null;

    private AccountOperationsService accountOperationsService = null;

    AccountModel testAccount_Savings_EUR_1 = null;
    AccountModel testAccount_Savings_USD_1 = null;

    AccountModel testAccount_Current_EUR_1 = null;
    AccountModel testAccount_Current_USD_1 = null;

    AccountModel testAccount_Savings_EUR_2 = null;
    AccountModel testAccount_Savings_USD_2 = null;

    AccountModel testAccount_Current_EUR_2 = null;
    AccountModel testAccount_Current_USD_2 = null;

    @Before
    public void init(){
        accountRepository = new AccountRepository();

        accountRepository = new AccountRepository();
        accountRepository.cleanup();

        accountQueryService = AccountQueryService.build(accountRepository);
        accountOperationsService = AccountOperationsService.build(accountRepository,accountQueryService);
    }

    @After
    public void teardown(){
        accountRepository.cleanup();
        accountRepository = null;

        testAccount_Savings_EUR_1 = null;
        testAccount_Savings_USD_1 = null;

        testAccount_Current_EUR_1 = null;
        testAccount_Current_USD_1 = null;

        testAccount_Savings_EUR_2 = null;
        testAccount_Savings_USD_2 = null;

        testAccount_Current_EUR_2 = null;
        testAccount_Current_USD_2 = null;
    }


    @Before
    public void initForEachTest(){
        prepareTestAccounts();
    }

    @After
    public void cleanupAfterEachTest(){
        MoneyTransferAPIFactory.getAccountRepository().cleanup();
        MoneyTransferAPIFactory.getTransactionRepository().cleanup();
    }


    @Test
    public void test_Get_All_Accounts(){

        //given
        // test accounts prepared in init method

        //when
        List<AccountModel> accountModelList = accountQueryService.getAllAccounts();

        //then
        assertNotNull(accountModelList);
        assertEquals(8,accountModelList.size());
        assertTrue(accountModelList.contains(testAccount_Savings_EUR_1));
        assertTrue(accountModelList.contains(testAccount_Savings_USD_1));
        assertTrue(accountModelList.contains(testAccount_Current_EUR_1));
        assertTrue(accountModelList.contains(testAccount_Current_USD_1));
        assertTrue(accountModelList.contains(testAccount_Savings_EUR_2));
        assertTrue(accountModelList.contains(testAccount_Savings_USD_2));
        assertTrue(accountModelList.contains(testAccount_Current_EUR_2));
        assertTrue(accountModelList.contains(testAccount_Current_USD_2));
    }


    @Test
    public void test_Get_All_Active_Accounts(){

        //given
        // 8 test accounts prepared in init method

        // When I Deactivate 2 Accounts, then Query for all activeAccounts should return 6 Accounts
        accountOperationsService.deactivateAccount(testAccount_Current_EUR_1.getAccountNumber());
        accountOperationsService.deactivateAccount(testAccount_Savings_USD_1.getAccountNumber());

        //when
        List<AccountModel> accountModelList = accountQueryService.getAllActiveAccounts();

        //then
        assertNotNull(accountModelList);
        assertEquals(6,accountModelList.size());

        assertFalse(accountModelList.contains(testAccount_Current_EUR_1));
        assertFalse(accountModelList.contains(testAccount_Savings_USD_1));
        assertTrue(accountModelList.contains(testAccount_Savings_EUR_1));
        assertTrue(accountModelList.contains(testAccount_Current_USD_1));
        assertTrue(accountModelList.contains(testAccount_Savings_EUR_2));
        assertTrue(accountModelList.contains(testAccount_Savings_USD_2));
        assertTrue(accountModelList.contains(testAccount_Current_EUR_2));
        assertTrue(accountModelList.contains(testAccount_Current_USD_2));
    }

    @Test
    public void test_Query_For_A_NonExistent_Account(){

        //given
        // 8 test accounts prepared in init method

        //when I Query for an accountNumber which is not-Existent
        AccountModel accountModel_Response = accountQueryService.getAnActiveAccountByAccountNumber(12);

        //then - it should be NULL
        assertNull(accountModel_Response);
    }

    @Test
    public void test_Query_For_An_Active_Account(){

        //given
        // 8 test accounts prepared in init method

        //when I Query for an accountNumber which is Existent
        AccountModel accountModel_Response = accountQueryService.getAnActiveAccountByAccountNumber(testAccount_Current_EUR_1.getAccountNumber());

        //then - it should be NULL
        assertNotNull(accountModel_Response);
        assertEquals(testAccount_Current_EUR_1,accountModel_Response);
    }

    @Test
    public void test_Query_For_An_Account(){

        //given
        // 8 test accounts prepared in init method
        // And I De-Activate 1 Account testAccount_Current_EUR_1
        accountOperationsService.deactivateAccount(testAccount_Current_EUR_1.getAccountNumber());

        //when I Query for an accountNumber which is InActive
        AccountModel accountModel_Response = accountQueryService.getAnAccountByAccountNumber(testAccount_Current_EUR_1.getAccountNumber());

        //then - it should be Non-NULL
        assertNotNull(accountModel_Response);
        assertEquals(testAccount_Current_EUR_1.getAccountNumber(),accountModel_Response.getAccountNumber());
        assertEquals(testAccount_Current_EUR_1.getAccountHolderName(),accountModel_Response.getAccountHolderName());
        assertEquals(testAccount_Current_EUR_1.getAccountBalance(),accountModel_Response.getAccountBalance());
        assertEquals(testAccount_Current_EUR_1.getAccountType(),accountModel_Response.getAccountType());
        assertEquals(testAccount_Current_EUR_1.getAddress(),accountModel_Response.getAddress());
        assertEquals(testAccount_Current_EUR_1.getCurrency(),accountModel_Response.getCurrency());
        assertEquals(testAccount_Current_EUR_1.getCreationDate(),accountModel_Response.getCreationDate());
        assertEquals(testAccount_Current_EUR_1.getNationalIdentityCode(),accountModel_Response.getNationalIdentityCode());
    }


    public void prepareTestAccounts(){

        //given
        String accountHolderName_1 = "Steve";
        String unitNumber_1 = "#01-22";
        String buildingName_1 = "New Castle";
        String streetOrLandmarkName_1 = "Flora Drive";
        String postalCode_1= "5001213";
        String countryCode_1 = "SG";
        String nationalIdentityCode_1 = "SG55415G";
        Double accountBalance_1 = 1000.10;

        Address address_1 = getAddressFromParameters(unitNumber_1,buildingName_1,streetOrLandmarkName_1,postalCode_1,countryCode_1);

        AccountModel accountForCreation_Current_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_1);

        assertNotNull(testAccount_Current_EUR_1);
        assertTrue(testAccount_Current_EUR_1.getIsActive());
        assertNotNull(testAccount_Current_EUR_1.getCreationDate());
        assertEquals(EUR,testAccount_Current_EUR_1.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_EUR_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Current_EUR_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Current_EUR_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Current_EUR_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Current_EUR_1.getAccountHolderName());
        assertEquals(Integer.valueOf(1),testAccount_Current_EUR_1.getAccountNumber());

        AccountModel accountForCreation_Savings_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_1);

        assertNotNull(testAccount_Savings_EUR_1);
        assertTrue(testAccount_Savings_EUR_1.getIsActive());
        assertNotNull(testAccount_Savings_EUR_1.getCreationDate());
        assertEquals(EUR,testAccount_Savings_EUR_1.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_EUR_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Savings_EUR_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Savings_EUR_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Savings_EUR_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Savings_EUR_1.getAccountHolderName());
        assertEquals(Integer.valueOf(2),testAccount_Savings_EUR_1.getAccountNumber());


        AccountModel accountForCreation_Current_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_1);

        assertNotNull(accountForCreation_Current_USD_1);
        assertTrue(accountForCreation_Current_USD_1.getIsActive());
        assertNotNull(testAccount_Current_USD_1.getCreationDate());
        assertEquals(USD,testAccount_Current_USD_1.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_USD_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Current_USD_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Current_USD_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Current_USD_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Current_USD_1.getAccountHolderName());
        assertEquals(Integer.valueOf(3),testAccount_Current_USD_1.getAccountNumber());

        AccountModel accountForCreation_Savings_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_1);

        assertNotNull(testAccount_Savings_USD_1);
        assertTrue(testAccount_Savings_USD_1.getIsActive());
        assertNotNull(testAccount_Savings_USD_1.getCreationDate());
        assertEquals(USD,testAccount_Savings_USD_1.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_USD_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Savings_USD_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Savings_USD_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Savings_USD_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Savings_USD_1.getAccountHolderName());
        assertEquals(Integer.valueOf(4),testAccount_Savings_USD_1.getAccountNumber());


        //AccountHolder-2

        //given
        String accountHolderName_2 = "Jobs";
        String unitNumber_2 = "#32-12";
        String buildingName_2 = "Fort Canning";
        String streetOrLandmarkName_2 = "Castle Road";
        String postalCode_2= "765331";
        String countryCode_2 = "SG";
        String nationalIdentityCode_2 = "SG99076G";
        Double accountBalance_2 = 200.00;

        Address address_2 = getAddressFromParameters(unitNumber_2,buildingName_2,streetOrLandmarkName_2,postalCode_2,countryCode_2);

        AccountModel accountForCreation_Current_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_2);

        assertNotNull(testAccount_Current_EUR_2);
        assertTrue(testAccount_Current_EUR_2.getIsActive());
        assertNotNull(testAccount_Current_EUR_2.getCreationDate());
        assertEquals(EUR,testAccount_Current_EUR_2.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_EUR_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Current_EUR_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Current_EUR_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Current_EUR_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Current_EUR_2.getAccountHolderName());
        assertEquals(Integer.valueOf(5),testAccount_Current_EUR_2.getAccountNumber());

        AccountModel accountForCreation_Savings_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_2);

        assertNotNull(testAccount_Savings_EUR_2);
        assertTrue(testAccount_Savings_EUR_2.getIsActive());
        assertNotNull(testAccount_Savings_EUR_2.getCreationDate());
        assertEquals(EUR,testAccount_Savings_EUR_2.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_EUR_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Savings_EUR_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Savings_EUR_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Savings_EUR_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Savings_EUR_2.getAccountHolderName());
        assertEquals(Integer.valueOf(6),testAccount_Savings_EUR_2.getAccountNumber());


        AccountModel accountForCreation_Current_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_2);

        assertNotNull(accountForCreation_Current_USD_2);
        assertTrue(accountForCreation_Current_USD_2.getIsActive());
        assertNotNull(testAccount_Current_USD_2.getCreationDate());
        assertEquals(USD,testAccount_Current_USD_2.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_USD_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Current_USD_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Current_USD_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Current_USD_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Current_USD_2.getAccountHolderName());
        assertEquals(Integer.valueOf(7),testAccount_Current_USD_2.getAccountNumber());

        AccountModel accountForCreation_Savings_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_2);

        assertNotNull(testAccount_Savings_USD_2);
        assertTrue(testAccount_Savings_USD_2.getIsActive());
        assertNotNull(testAccount_Savings_USD_2.getCreationDate());
        assertEquals(USD,testAccount_Savings_USD_2.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_USD_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Savings_USD_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Savings_USD_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Savings_USD_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Savings_USD_2.getAccountHolderName());
        assertEquals(Integer.valueOf(8),testAccount_Savings_USD_2.getAccountNumber());

    }


    /**
     *
     * @param accountNumber
     * @param accountHolderName
     * @param address
     * @param nationalIdentityCode
     * @param accountType
     * @param accountBalance
     * @param currency
     * @param isActive
     * @param creationDate
     * @param updateDate
     * @return AccountModel
     */
    public AccountModel getAccountModelBeanFromParameters(Integer accountNumber, String accountHolderName,
                                                          Address address, String nationalIdentityCode,
                                                          AccountType accountType, Double accountBalance,
                                                          Currency currency, Boolean isActive,
                                                          LocalDateTime creationDate, LocalDateTime updateDate){

        return AccountModel.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(accountBalance)
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();

    }

    /**
     *
     * @param unitNumber
     * @param buildingName
     * @param streetOrLandmarkName
     * @param postalCode
     * @param countryCode
     * @return Address
     */
    public Address getAddressFromParameters( String unitNumber,
                                             String buildingName,
                                             String streetOrLandmarkName,
                                             String postalCode,
                                             String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                .postalCode(postalCode).countryCode(countryCode).build();
    }

}
