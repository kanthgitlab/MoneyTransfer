package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.model.CurrencyPairModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ExchangeRateQueryServiceTest {

    private ExchangeRateQueryService exchangeRateQueryService;

    public static final Map<CurrencyPairModel, BigDecimal>
            testRates = new HashMap<CurrencyPairModel, BigDecimal>() {{
        put(new CurrencyPairModel(Currency.GBP, Currency.EUR), BigDecimal.valueOf(1.11));
        put(new CurrencyPairModel(Currency.GBP, Currency.USD), BigDecimal.valueOf(1.25));
        put(new CurrencyPairModel(Currency.GBP, Currency.GBP), BigDecimal.valueOf(1D));

        put(new CurrencyPairModel(Currency.USD, Currency.EUR), BigDecimal.valueOf(0.89));
        put(new CurrencyPairModel(Currency.USD, Currency.USD), BigDecimal.valueOf(1D));
        put(new CurrencyPairModel(Currency.USD, Currency.GBP), BigDecimal.valueOf(0.8));

        put(new CurrencyPairModel(Currency.EUR, Currency.EUR), BigDecimal.valueOf(1D));
        put(new CurrencyPairModel(Currency.EUR, Currency.USD), BigDecimal.valueOf(1.13));
        put(new CurrencyPairModel(Currency.EUR, Currency.GBP), BigDecimal.valueOf(0.9));
    }};

    @Before
    public void beforeTest(){
        exchangeRateQueryService = new ExchangeRateQueryService();
        exchangeRateQueryService.buildStaticRates(testRates);
    }

    @After
    public void tearDown(){
        exchangeRateQueryService = null;
    }

    @Test
    public void test_Assert_Initialize_ExchangeRates(){

        //given - ExchangeRateQueryService is initialized

        //when
        Map<CurrencyPairModel, BigDecimal> exchangeRateMap =  exchangeRateQueryService.getExchangeRates();

        //then
        assertNotNull(exchangeRateMap);
        assertEquals(9,exchangeRateMap.size());
    }

    @Test
    public void test_Assert_Get_ExchangeRate_For_A_CurrencyPair(){

        //given - ExchangeRateQueryService is initialized
        Currency baseCurrency = Currency.EUR;
        Currency alternateCurrency = Currency.GBP;

        //when
        Double exchangeRate =  exchangeRateQueryService.getExchangeRate(baseCurrency,alternateCurrency);

        //then
        assertNotNull(exchangeRate);
        assertEquals(Double.valueOf(0.9),exchangeRate);
    }

    @Test
    public void test_Assert_Get_ExchangeRate_For_A_NonExisting_CurrencyPair(){

        //given - ExchangeRateQueryService is initialized
        Currency baseCurrency = Currency.EUR;
        Currency alternateCurrency = null;

        //when
        Double exchangeRate =  exchangeRateQueryService.getExchangeRate(baseCurrency,alternateCurrency);

        //then
        assertNull(exchangeRate);
    }




}
