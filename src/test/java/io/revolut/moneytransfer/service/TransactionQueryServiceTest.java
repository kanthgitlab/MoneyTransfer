package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.constants.TransactionStatus;
import io.revolut.moneytransfer.constants.TransactionType;
import io.revolut.moneytransfer.entity.Address;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.model.TransactionModel;
import io.revolut.moneytransfer.model.TransactionRequestModel;
import io.revolut.moneytransfer.repository.AccountRepository;
import io.revolut.moneytransfer.repository.TransactionRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static io.revolut.moneytransfer.constants.Currency.EUR;
import static io.revolut.moneytransfer.constants.Currency.USD;
import static io.revolut.moneytransfer.service.TransactionQueryService.compareByAmountDescending;
import static io.revolut.moneytransfer.service.TransactionQueryService.compareByCreationDateDescending;
import static io.revolut.moneytransfer.service.TransactionQueryService.compareByTransactionStatusDescending;
import static io.revolut.moneytransfer.service.TransactionQueryService.compareByTransactionTypeDescending;
import static io.revolut.moneytransfer.util.MoneyTransferAPIUtil.sleepForTestSimualtion;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TransactionQueryServiceTest {

    private static final int TEST_DATA_SIMULATION_TIME_IN_MILLIS = 3;

    private TransactionRepository transactionRepository;

    private TransactionQueryService transactionQueryService;

    private AccountOperationsService accountOperationsService = null;

    private MoneyTransferService moneyTransferService = null;

    private AccountRepository accountRepository = null;

    private CurrencyConversionService currencyConversionService = null;

    private List<TransactionModel> transactionModelList_TestData = null;

    AccountModel testAccount_Savings_EUR_1 = null;
    AccountModel testAccount_Savings_USD_1 = null;

    AccountModel testAccount_Current_EUR_1 = null;
    AccountModel testAccount_Current_USD_1 = null;

    AccountModel testAccount_Savings_EUR_2 = null;
    AccountModel testAccount_Savings_USD_2 = null;

    AccountModel testAccount_Current_EUR_2 = null;
    AccountModel testAccount_Current_USD_2 = null;

    @Before
    public void init(){
        transactionRepository = new TransactionRepository();
        transactionRepository.cleanup();

        transactionQueryService = TransactionQueryService.build(transactionRepository);

        accountRepository = new AccountRepository();
        accountRepository.cleanup();

        AccountQueryService accountQueryService = AccountQueryService.build(accountRepository);
        accountOperationsService = AccountOperationsService.build(accountRepository,accountQueryService);

        ExchangeRateQueryService exchangeRateQueryService = new ExchangeRateQueryService();
        currencyConversionService = CurrencyConversionService.build(exchangeRateQueryService);
        moneyTransferService = MoneyTransferService.build(transactionRepository,accountQueryService,accountRepository,exchangeRateQueryService,currencyConversionService);

        prepareTestAccounts();
        prepareTransactionTestData();
    }

    @After
    public void teardown(){
        transactionRepository.cleanup();
        transactionRepository = null;
        transactionQueryService = null;
        accountRepository.cleanup();
        accountRepository = null;

        if(transactionModelList_TestData!=null) {
            transactionModelList_TestData.clear();
        }
        transactionModelList_TestData = null;

        testAccount_Savings_EUR_1 = null;
        testAccount_Savings_USD_1 = null;

        testAccount_Current_EUR_1 = null;
        testAccount_Current_USD_1 = null;

        testAccount_Savings_EUR_2 = null;
        testAccount_Savings_USD_2 = null;

        testAccount_Current_EUR_2 = null;
        testAccount_Current_USD_2 = null;
    }


    @Test
    public void test_Get_All_Transactions(){

        //given

        //Test Data Prepared with:

        /**
         *
         * 2 Successful Deposits
         * 1 Successful Withdrawal
         * 2 Failed Withdrawal Attempt
         * 1 Successful Money-Transfer
         *
         */

        //when

        List<TransactionModel> transactionModels = transactionQueryService.getAllTransactions();

        //then
        assertNotNull(transactionModels);
        assertEquals(5,transactionModels.size());
        assertEquals(transactionModelList_TestData,transactionModels);
    }

    @Test
    public void test_Get_All_Transactions_For_A_Specific_Account(){

        //given

        //Test Data Prepared with:

        /**
         *
         * 2 Successful Deposits
         * 1 Successful Withdrawal
         * 2 Failed Withdrawal Attempt
         * 1 Successful Money-Transfer
         *
         */

        //when

        List<TransactionModel> transactionModels = transactionQueryService.getAllTransactionsOfAnAccount(testAccount_Current_EUR_1.getAccountNumber());

        //then
        assertNotNull(transactionModels);
        assertEquals(5,transactionModels.size());
        assertEquals(transactionModelList_TestData,transactionModels);
    }


    @Test
    public void test_Get_All_Transactions_For_A_Specific_Account_Subset(){

        //given

        //Test Data Prepared with:

        /**
         *
         * 2 Successful Deposits
         * 1 Successful Withdrawal
         * 2 Failed Withdrawal Attempt
         * 1 Successful Money-Transfer
         *
         */

        //when

        List<TransactionModel> transactionModels = transactionQueryService.getAllTransactionsOfAnAccount(testAccount_Savings_EUR_2.getAccountNumber());

        //then
        assertNotNull(transactionModels);
        assertEquals(1,transactionModels.size());

        List<TransactionModel> transactionModels_Expected = transactionModelList_TestData.stream()
                .filter(transactionModel ->
                        (transactionModel.getRecipientBankAccountNumber().equals(testAccount_Savings_EUR_2.getAccountNumber())
                                &&
                                transactionModel.getRecipientBankAccountNumber().equals(testAccount_Savings_EUR_2.getAccountNumber()))
                ).sorted(compareByCreationDateDescending.get()
                        .thenComparing(compareByAmountDescending.get())
                        .thenComparing(compareByTransactionStatusDescending.get())
                        .thenComparing(compareByTransactionTypeDescending.get())).collect(Collectors.toList());

        assertEquals(transactionModels_Expected, transactionModels);
    }

    @Test
    public void test_Get_Transaction_For_A_TransactionId(){

        //given

        //Test Data Prepared with:

        /**
         *
         * 2 Successful Deposits
         * 1 Successful Withdrawal
         * 2 Failed Withdrawal Attempt
         * 1 Successful Money-Transfer
         *
         */

        //when

        TransactionModel transactionModel = transactionQueryService.getTransactionDetails(transactionModelList_TestData.get(0).getTransactionId());

        //then
        assertNotNull(transactionModel);
        assertEquals(transactionModelList_TestData.get(0),transactionModel);

    }


    /**
     * Test Data:
     *
     * 2 Successful Deposits
     * 1 Successful Withdrawal
     * 1 Failed Withdrawal Attempt
     * 1 Successful Money-Transfer
     *
     */
    public void prepareTransactionTestData(){

        //given

        transactionModelList_TestData = new ArrayList<>();

        Double amount = 100.00;
        Integer bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        Currency currency = testAccount_Current_EUR_1.getCurrency();
        Integer recipientBankAccountNumber = null;
        Integer senderBankAccountNumber = null;
        String transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        String transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        TransactionType transactionType = TransactionType.DEPOSIT;

        TransactionRequestModel transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_1 = moneyTransferService.depositMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_1);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        //given

         amount = 100.00;
         bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
         currency = testAccount_Current_EUR_1.getCurrency();
         recipientBankAccountNumber = null;
         senderBankAccountNumber = null;
         transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
         transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
         transactionType = TransactionType.DEPOSIT;

         transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_2 = moneyTransferService.depositMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_2);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        //given

        amount = 100.00;
        bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        currency = testAccount_Current_EUR_1.getCurrency();
        recipientBankAccountNumber = null;
        senderBankAccountNumber = null;
        transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        transactionType = TransactionType.WITHDRAWAL;

        transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_3 = moneyTransferService.withdrawMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_3);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        //given

         amount = 100.00;
         bankAccountNumber = null;
         currency = testAccount_Current_EUR_1.getCurrency();
         recipientBankAccountNumber = testAccount_Savings_EUR_2.getAccountNumber();
         senderBankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
         transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
         transactionRecipient = testAccount_Savings_EUR_2.getAccountHolderName();
         transactionType = TransactionType.TRANSFER;

         transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_4 = moneyTransferService.transferMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_4);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        //given

        amount = 10000.00;
        bankAccountNumber = testAccount_Current_EUR_1.getAccountNumber();
        currency = testAccount_Current_EUR_1.getCurrency();
        recipientBankAccountNumber = null;
        senderBankAccountNumber = null;
        transactionMaker = testAccount_Current_EUR_1.getAccountHolderName();
        transactionRecipient = testAccount_Current_EUR_1.getAccountHolderName();
        transactionType = TransactionType.WITHDRAWAL;

        transactionRequestModel =
                getATransactionRequestModelFromParameters(amount,bankAccountNumber,currency,recipientBankAccountNumber,
                        senderBankAccountNumber,transactionMaker,transactionRecipient,transactionType);

        //when
        TransactionModel transactionModel_Response_5 = moneyTransferService.withdrawMoney(transactionRequestModel);

        transactionModelList_TestData.add(transactionModel_Response_5);

        sleepForTestSimualtion(TEST_DATA_SIMULATION_TIME_IN_MILLIS);

        transactionModelList_TestData =transactionModelList_TestData.stream()
                                     .sorted(compareByCreationDateDescending.get().thenComparing(compareByAmountDescending.get()))
                                     .collect(Collectors.toList());
    }

    public void prepareTestAccounts(){

        //given
        String accountHolderName_1 = "Steve";
        String unitNumber_1 = "#01-22";
        String buildingName_1 = "New Castle";
        String streetOrLandmarkName_1 = "Flora Drive";
        String postalCode_1= "5001213";
        String countryCode_1 = "SG";
        String nationalIdentityCode_1 = "SG55415G";
        Double accountBalance_1 = 1000.10;

        Address address_1 = getAddressFromParameters(unitNumber_1,buildingName_1,streetOrLandmarkName_1,postalCode_1,countryCode_1);

        AccountModel accountForCreation_Current_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_1);

        assertNotNull(testAccount_Current_EUR_1);
        assertTrue(testAccount_Current_EUR_1.getIsActive());
        assertNotNull(testAccount_Current_EUR_1.getCreationDate());
        assertEquals(EUR,testAccount_Current_EUR_1.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_EUR_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Current_EUR_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Current_EUR_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Current_EUR_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Current_EUR_1.getAccountHolderName());
        assertEquals(Integer.valueOf(1),testAccount_Current_EUR_1.getAccountNumber());

        AccountModel accountForCreation_Savings_EUR_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_1);

        assertNotNull(testAccount_Savings_EUR_1);
        assertTrue(testAccount_Savings_EUR_1.getIsActive());
        assertNotNull(testAccount_Savings_EUR_1.getCreationDate());
        assertEquals(EUR,testAccount_Savings_EUR_1.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_EUR_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Savings_EUR_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Savings_EUR_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Savings_EUR_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Savings_EUR_1.getAccountHolderName());
        assertEquals(Integer.valueOf(2),testAccount_Savings_EUR_1.getAccountNumber());


        AccountModel accountForCreation_Current_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.CURRENT,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_1);

        assertNotNull(accountForCreation_Current_USD_1);
        assertTrue(accountForCreation_Current_USD_1.getIsActive());
        assertNotNull(testAccount_Current_USD_1.getCreationDate());
        assertEquals(USD,testAccount_Current_USD_1.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_USD_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Current_USD_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Current_USD_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Current_USD_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Current_USD_1.getAccountHolderName());
        assertEquals(Integer.valueOf(3),testAccount_Current_USD_1.getAccountNumber());

        AccountModel accountForCreation_Savings_USD_1 = getAccountModelBeanFromParameters(null,accountHolderName_1,address_1,
                nationalIdentityCode_1,AccountType.SAVINGS,accountBalance_1,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_1 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_1);

        assertNotNull(testAccount_Savings_USD_1);
        assertTrue(testAccount_Savings_USD_1.getIsActive());
        assertNotNull(testAccount_Savings_USD_1.getCreationDate());
        assertEquals(USD,testAccount_Savings_USD_1.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_USD_1.getAccountType());
        assertEquals(Double.valueOf(accountBalance_1), Double.valueOf(testAccount_Savings_USD_1.getAccountBalance()));
        assertEquals(nationalIdentityCode_1,testAccount_Savings_USD_1.getNationalIdentityCode());
        assertEquals(address_1,testAccount_Savings_USD_1.getAddress());
        assertEquals(accountHolderName_1,testAccount_Savings_USD_1.getAccountHolderName());
        assertEquals(Integer.valueOf(4),testAccount_Savings_USD_1.getAccountNumber());


        //AccountHolder-2

        //given
        String accountHolderName_2 = "Jobs";
        String unitNumber_2 = "#32-12";
        String buildingName_2 = "Fort Canning";
        String streetOrLandmarkName_2 = "Castle Road";
        String postalCode_2= "765331";
        String countryCode_2 = "SG";
        String nationalIdentityCode_2 = "SG99076G";
        Double accountBalance_2 = 200.00;

        Address address_2 = getAddressFromParameters(unitNumber_2,buildingName_2,streetOrLandmarkName_2,postalCode_2,countryCode_2);

        AccountModel accountForCreation_Current_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Current_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Current_EUR_2);

        assertNotNull(testAccount_Current_EUR_2);
        assertTrue(testAccount_Current_EUR_2.getIsActive());
        assertNotNull(testAccount_Current_EUR_2.getCreationDate());
        assertEquals(EUR,testAccount_Current_EUR_2.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_EUR_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Current_EUR_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Current_EUR_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Current_EUR_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Current_EUR_2.getAccountHolderName());
        assertEquals(Integer.valueOf(5),testAccount_Current_EUR_2.getAccountNumber());

        AccountModel accountForCreation_Savings_EUR_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                EUR,Boolean.TRUE,null,null);

        testAccount_Savings_EUR_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_EUR_2);

        assertNotNull(testAccount_Savings_EUR_2);
        assertTrue(testAccount_Savings_EUR_2.getIsActive());
        assertNotNull(testAccount_Savings_EUR_2.getCreationDate());
        assertEquals(EUR,testAccount_Savings_EUR_2.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_EUR_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Savings_EUR_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Savings_EUR_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Savings_EUR_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Savings_EUR_2.getAccountHolderName());
        assertEquals(Integer.valueOf(6),testAccount_Savings_EUR_2.getAccountNumber());


        AccountModel accountForCreation_Current_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.CURRENT,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Current_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Current_USD_2);

        assertNotNull(accountForCreation_Current_USD_2);
        assertTrue(accountForCreation_Current_USD_2.getIsActive());
        assertNotNull(testAccount_Current_USD_2.getCreationDate());
        assertEquals(USD,testAccount_Current_USD_2.getCurrency());
        assertEquals(AccountType.CURRENT,testAccount_Current_USD_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Current_USD_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Current_USD_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Current_USD_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Current_USD_2.getAccountHolderName());
        assertEquals(Integer.valueOf(7),testAccount_Current_USD_2.getAccountNumber());

        AccountModel accountForCreation_Savings_USD_2 = getAccountModelBeanFromParameters(null,accountHolderName_2,address_2,
                nationalIdentityCode_2,AccountType.SAVINGS,accountBalance_2,
                USD,Boolean.TRUE,null,null);

        testAccount_Savings_USD_2 = accountOperationsService.createNewAccount(accountForCreation_Savings_USD_2);

        assertNotNull(testAccount_Savings_USD_2);
        assertTrue(testAccount_Savings_USD_2.getIsActive());
        assertNotNull(testAccount_Savings_USD_2.getCreationDate());
        assertEquals(USD,testAccount_Savings_USD_2.getCurrency());
        assertEquals(AccountType.SAVINGS,testAccount_Savings_USD_2.getAccountType());
        assertEquals(Double.valueOf(accountBalance_2), Double.valueOf(testAccount_Savings_USD_2.getAccountBalance()));
        assertEquals(nationalIdentityCode_2,testAccount_Savings_USD_2.getNationalIdentityCode());
        assertEquals(address_2,testAccount_Savings_USD_2.getAddress());
        assertEquals(accountHolderName_2,testAccount_Savings_USD_2.getAccountHolderName());
        assertEquals(Integer.valueOf(8),testAccount_Savings_USD_2.getAccountNumber());

    }



    /**
     *
     * @param amount
     * @param bankAccountNumber
     * @param currency
     * @param recipientBankAccountNumber
     * @param senderBankAccountNumber
     * @param transactionMaker
     * @param transactionRecipient
     * @param transactionType
     * @return TransactionRequestModel
     */
    public TransactionRequestModel getATransactionRequestModelFromParameters(Double amount, Integer bankAccountNumber,
                                                                             Currency currency, Integer recipientBankAccountNumber,
                                                                             Integer senderBankAccountNumber,
                                                                             String transactionMaker,
                                                                             String transactionRecipient,
                                                                             TransactionType transactionType){

        return TransactionRequestModel.builder().amount(amount).bankAccountNumber(bankAccountNumber).currency(currency)
                .recipientBankAccountNumber(recipientBankAccountNumber).senderBankAccountNumber(senderBankAccountNumber)
                .transactionMaker(transactionMaker).transactionRecipient(transactionRecipient).transactionType(transactionType).build();
    }

    /**
     *
     * @param transactionId
     * @param transactionMaker
     * @param transactionRecipient
     * @param transactionStatus
     * @param transactionType
     * @return TransactionModel
     */
    public TransactionModel getATransactionModel(String transactionId, String transactionMaker, String transactionRecipient, TransactionStatus transactionStatus, TransactionType transactionType){

        return TransactionModel.builder().transactionId(transactionId).transactionMaker(transactionMaker).transactionRecipient(transactionRecipient)
                .transactionStatus(transactionStatus).transactionType(transactionType).build();


    }

    /**
     *
     * @param accountNumber
     * @param accountHolderName
     * @param address
     * @param nationalIdentityCode
     * @param accountType
     * @param accountBalance
     * @param currency
     * @param isActive
     * @param creationDate
     * @param updateDate
     * @return AccountModel
     */
    public AccountModel getAccountModelBeanFromParameters(Integer accountNumber, String accountHolderName,
                                                          Address address, String nationalIdentityCode,
                                                          AccountType accountType, Double accountBalance,
                                                          Currency currency, Boolean isActive,
                                                          LocalDateTime creationDate, LocalDateTime updateDate){

        return AccountModel.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(accountBalance)
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();

    }

    /**
     *
     * @param unitNumber
     * @param buildingName
     * @param streetOrLandmarkName
     * @param postalCode
     * @param countryCode
     * @return Address
     */
    public Address getAddressFromParameters( String unitNumber,
                                             String buildingName,
                                             String streetOrLandmarkName,
                                             String postalCode,
                                             String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                .postalCode(postalCode).countryCode(countryCode).build();
    }


}
