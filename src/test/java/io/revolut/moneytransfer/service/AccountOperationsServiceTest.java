package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.entity.Address;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.repository.AccountRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.multiverse.api.StmUtils.atomic;

public class AccountOperationsServiceTest {

    private AccountRepository accountRepository = null;

    private AccountOperationsService accountOperationsService;

    @Before
    public void init(){
        accountRepository = new AccountRepository();
        AccountQueryService accountQueryService = AccountQueryService.build(accountRepository);
        accountOperationsService = AccountOperationsService.build(accountRepository,accountQueryService);
    }

    @After
    public void teardown(){
        accountRepository.cleanup();
        accountRepository = null;
    }


    @Test
    public void test_Create_New_Current_Account(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        //when
        AccountModel account_Created = accountOperationsService.createNewAccount(accountForCreation);

        //then
        assertNotNull(account_Created);
        assertTrue(account_Created.getIsActive());
        assertNotNull(account_Created.getCreationDate());
        assertEquals(currency,account_Created.getCurrency());
        assertEquals(accountType,account_Created.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance),Double.valueOf(account_Created.getAccountBalance())));
        assertEquals(nationalIdentityCode,account_Created.getNationalIdentityCode());
        assertEquals(address,account_Created.getAddress());
        assertEquals(accountHolderName,account_Created.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Created.getAccountNumber());
    }

    @Test
    public void test_Delete_Current_Account(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        AccountModel account_Created = accountOperationsService.createNewAccount(accountForCreation);

        //when
        Boolean isDeletionSuccessful = accountOperationsService.deleteAccount(account_Created.getAccountNumber());

        //then
        assertTrue(isDeletionSuccessful);
        assertNull(accountRepository.getAnAccountByAccountNumber(account_Created.getAccountNumber()));
    }

    @Test
    public void test_Update_Existing_Current_Account(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        AccountModel account_Created = accountOperationsService.createNewAccount(accountForCreation);

        assertNotNull(account_Created);
        assertTrue(account_Created.getIsActive());
        assertNotNull(account_Created.getCreationDate());
        assertEquals(currency,account_Created.getCurrency());
        assertEquals(accountType,account_Created.getAccountType());
        assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Created.getAccountBalance()));
        assertEquals(nationalIdentityCode,account_Created.getNationalIdentityCode());
        assertEquals(address,account_Created.getAddress());
        assertEquals(accountHolderName,account_Created.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Created.getAccountNumber());

        accountHolderName = "Steven Spicer";
        unitNumber = "#22-12";
        countryCode = "SGP";

        address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForUpdate = getAccountModelBeanFromParameters(account_Created.getAccountNumber(),accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,accountForCreation.getCreationDate(),null);

        //when
        AccountModel account_Updated = accountOperationsService.updateAccountDetails(accountForUpdate);

        //then
        assertNotNull(account_Updated);
        assertTrue(account_Updated.getIsActive());
        assertEquals(address,account_Updated.getAddress());
        assertEquals(accountHolderName,account_Updated.getAccountHolderName());
        assertNotNull(account_Updated.getCreationDate());
        assertEquals(currency,account_Updated.getCurrency());
        assertEquals(accountType,account_Updated.getAccountType());
        assertEquals(Double.valueOf(accountBalance),Double.valueOf(account_Updated.getAccountBalance()));
        assertEquals(nationalIdentityCode,account_Updated.getNationalIdentityCode());
        assertEquals(address,account_Updated.getAddress());
        assertEquals(accountHolderName,account_Updated.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Updated.getAccountNumber());
    }


    @Test
    public void test_Update_Account_Balance(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountForCreation = getAccountModelBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        AccountModel account_Created = accountOperationsService.createNewAccount(accountForCreation);

        assertNotNull(account_Created);
        assertTrue(account_Created.getIsActive());
        assertNotNull(account_Created.getCreationDate());
        assertEquals(currency,account_Created.getCurrency());
        assertEquals(accountType,account_Created.getAccountType());
        assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Created.getAccountBalance()));
        assertEquals(nationalIdentityCode,account_Created.getNationalIdentityCode());
        assertEquals(address,account_Created.getAddress());
        assertEquals(accountHolderName,account_Created.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Created.getAccountNumber());

        Double newBalance = 12.31;
        account_Created.setAccountBalance(newBalance);

        //when
        AccountModel account_Updated = accountOperationsService.updateAccountBalance(account_Created.getAccountNumber(),account_Created.getAccountBalance(),newBalance);

        //then
        assertNotNull(account_Updated);
        assertTrue(account_Updated.getIsActive());
        assertEquals(address,account_Updated.getAddress());
        assertEquals(accountHolderName,account_Updated.getAccountHolderName());
        assertNotNull(account_Updated.getCreationDate());
        assertEquals(currency,account_Updated.getCurrency());
        assertEquals(accountType,account_Updated.getAccountType());
        assertEquals(Double.valueOf(newBalance),Double.valueOf(account_Updated.getAccountBalance()));
        assertEquals(nationalIdentityCode,account_Updated.getNationalIdentityCode());
        assertEquals(address,account_Updated.getAddress());
        assertEquals(accountHolderName,account_Updated.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Updated.getAccountNumber());
    }


    public AccountModel getAccountModelBeanFromParameters(Integer accountNumber, String accountHolderName,
                                                          Address address, String nationalIdentityCode,
                                                          AccountType accountType, Double accountBalance,
                                                          Currency currency, Boolean isActive,
                                                          LocalDateTime creationDate, LocalDateTime updateDate){

        return AccountModel.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(accountBalance)
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();

    }

    public Address getAddressFromParameters( String unitNumber,
                                             String buildingName,
                                             String streetOrLandmarkName,
                                             String postalCode,
                                             String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                .postalCode(postalCode).countryCode(countryCode).build();
    }




}
