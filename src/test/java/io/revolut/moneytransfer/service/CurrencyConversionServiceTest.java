package io.revolut.moneytransfer.service;

import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.model.CurrencyPairModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CurrencyConversionServiceTest {

    private ExchangeRateQueryService exchangeRateQueryService;

    private CurrencyConversionService currencyConversionService;

    public static final Map<CurrencyPairModel, BigDecimal>
            testRates = new HashMap<CurrencyPairModel, BigDecimal>() {{
        put(new CurrencyPairModel(Currency.GBP, Currency.EUR), BigDecimal.valueOf(1.11));
        put(new CurrencyPairModel(Currency.GBP, Currency.USD), BigDecimal.valueOf(1.25));
        put(new CurrencyPairModel(Currency.GBP, Currency.GBP), BigDecimal.valueOf(1D));

        put(new CurrencyPairModel(Currency.USD, Currency.EUR), BigDecimal.valueOf(0.89));
        put(new CurrencyPairModel(Currency.USD, Currency.USD), BigDecimal.valueOf(1D));
        put(new CurrencyPairModel(Currency.USD, Currency.GBP), BigDecimal.valueOf(0.8));

        put(new CurrencyPairModel(Currency.EUR, Currency.EUR), BigDecimal.valueOf(1D));
        put(new CurrencyPairModel(Currency.EUR, Currency.USD), BigDecimal.valueOf(1.13));
        put(new CurrencyPairModel(Currency.EUR, Currency.GBP), BigDecimal.valueOf(0.9));
    }};

    @Before
    public void beforeTest(){
        exchangeRateQueryService = new ExchangeRateQueryService();
        exchangeRateQueryService.buildStaticRates(testRates);
        currencyConversionService = CurrencyConversionService.build(exchangeRateQueryService);
    }

    @After
    public void tearDown(){
        exchangeRateQueryService = null;
        currencyConversionService = null;
    }

    @Test
    public void test_Convert_Amount_By_Querying_ExchangeRate(){

        //given
        Currency baseCurrency = Currency.EUR;
        Currency alternateCurrency = Currency.GBP;
        Double amount = 10000.00;
        Double convertedAmount_Expected = 9000.00;


        //when
        Double convertedAmount = currencyConversionService.convert(amount,baseCurrency,alternateCurrency);

        //then
        assertNotNull(convertedAmount);
        assertEquals(convertedAmount_Expected,convertedAmount);
    }

    @Test
    public void test_Convert_Amount_Using_ExchangeRate(){

        //given
        Double amount = 10000.00;
        Double convertedAmount_Expected = 9000.00;
        Double exchangeRate = 0.9;


        //when
        Double convertedAmount = currencyConversionService.convert(amount,exchangeRate);

        //then
        assertNotNull(convertedAmount);
        assertEquals(convertedAmount_Expected,convertedAmount);
    }

    @Test
    public void test_Convert_Amount_Using_ExchangeRate_For_Identical_Currencies(){

        //given
        Currency baseCurrency = Currency.EUR;
        Currency alternateCurrency = Currency.EUR;
        Double amount = 10000.00;
        Double convertedAmount_Expected = 10000.00;


        //when
        Double convertedAmount = currencyConversionService.convert(amount,baseCurrency,alternateCurrency);

        //then
        assertNotNull(convertedAmount);
        assertEquals(convertedAmount_Expected,convertedAmount);
    }
}
