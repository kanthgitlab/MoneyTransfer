package io.revolut.moneytransfer.factory;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class MoneyTransferAPIFactoryTest {

    @Test
    public void test_Build_Objects(){

        //when
        MoneyTransferAPIFactory.build();

        //then
        assertNotNull(MoneyTransferAPIFactory.getAccountOperationsService());
        assertNotNull(MoneyTransferAPIFactory.getAccountQueryService());
        assertNotNull(MoneyTransferAPIFactory.getAccountRepository());
        assertNotNull(MoneyTransferAPIFactory.getMoneyTransferService());
        assertNotNull(MoneyTransferAPIFactory.getTransactionQueryService());
        assertNotNull(MoneyTransferAPIFactory.getTransactionRepository());
    }
}
