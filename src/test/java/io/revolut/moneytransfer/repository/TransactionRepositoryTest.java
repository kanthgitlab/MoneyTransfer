package io.revolut.moneytransfer.repository;

import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.constants.TransactionStatus;
import io.revolut.moneytransfer.constants.TransactionType;
import io.revolut.moneytransfer.entity.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

import static io.revolut.moneytransfer.util.TimeManagementUtil.getCurrentTime;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


public class TransactionRepositoryTest {


    private TransactionRepository transactionRepository;

    @Before
    public void init(){
        transactionRepository = new TransactionRepository();
    }

    @After
    public void teardown(){
        transactionRepository = null;
    }

    @Test
    public void test_Generate_TransactionId(){

        //given
        TransactionType transactionType = TransactionType.DEPOSIT;

        //when
        String transactionId_Generated = transactionRepository.generateTransactionId(transactionType);

        //then
        assertNotNull(transactionId_Generated);
        assertTrue(transactionId_Generated.contains(transactionType.name()));
        assertTrue(transactionId_Generated.startsWith("TRN_"));
    }


    @Test
    public void test_Create_Transaction(){
        
        //given
        String transactionMaker = "Steven";
        String transactionRecipient = "Ramu";
        Integer senderBankAccountNumber = 1123111;
        Integer recipientBankAccountNumber = 44312111;
        Double amount = 2111.90;
        Currency currency = Currency.EUR;
        TransactionType transactionType =TransactionType.TRANSFER;
        LocalDateTime creationDate = null;
        LocalDateTime updateDate = null;
        TransactionStatus transactionStatus = TransactionStatus.SUCCESSFUL;
        List<String> exceptionMessages = null;
        String message = "Successful Transfer for amount 2111.90 from Steven to Ramu";
        
        Transaction transaction_For_Creation = getATransactionEntityFromParameters( transactionMaker,
                 transactionRecipient,  senderBankAccountNumber,
                 recipientBankAccountNumber,  amount,
                 currency,  transactionType,
                 creationDate,  updateDate,
                 transactionStatus, exceptionMessages,  message);

        //when
        Transaction transaction_Created = transactionRepository.createTransaction(transaction_For_Creation);

        //then
        assertNotNull(transaction_Created);
        assertNotNull(transaction_Created.getTransactionId());
        assertNotNull(transaction_Created.getCreationDate());
        assertNotNull(transaction_Created.getTransactionMaker());
        assertNotNull(transaction_Created.getTransactionRecipient());
        assertEquals(transactionType,transaction_Created.getTransactionType());
        assertEquals(transactionStatus,transaction_Created.getTransactionStatus());
        assertEquals(message,transaction_Created.getMessage());
        assertNull(transaction_Created.getExceptionMessages());
    }

    @Test
    public void test_Query_All_Transactions_By_AccountNumber(){

        //given
        String transactionMaker = "Steven";
        String transactionRecipient = "Ramu";
        Integer senderBankAccountNumber = 1123111;
        Integer recipientBankAccountNumber = 44312111;
        Double amount = 2111.90;
        Currency currency = Currency.EUR;
        TransactionType transactionType =TransactionType.TRANSFER;
        LocalDateTime creationDate = null;
        LocalDateTime updateDate = null;
        TransactionStatus transactionStatus = TransactionStatus.SUCCESSFUL;
        List<String> exceptionMessages = null;
        String message = "Successful Transfer for amount 2111.90 from Steven to Ramu";

        Transaction transaction_For_Creation = getATransactionEntityFromParameters( transactionMaker,
                transactionRecipient,  senderBankAccountNumber,
                recipientBankAccountNumber,  amount,
                currency,  transactionType,
                creationDate,  updateDate,
                transactionStatus, exceptionMessages,  message);

        Transaction transaction_Created = transactionRepository.createTransaction(transaction_For_Creation);

        //when
        List<Transaction> transactions_Queried_By_SenderAccount =
                transactionRepository.getAllTransactionsByAccountNumber(senderBankAccountNumber);
        List<Transaction> transactions_Queried_By_RecipientAccount =
                transactionRepository.getAllTransactionsByAccountNumber(recipientBankAccountNumber);

        //then
        assertNotNull(transactions_Queried_By_SenderAccount);
        assertNotNull(transactions_Queried_By_RecipientAccount);
        assertEquals(1,transactions_Queried_By_SenderAccount.size());
        assertEquals(1,transactions_Queried_By_RecipientAccount.size());
        assertEquals(transactions_Queried_By_SenderAccount,transactions_Queried_By_RecipientAccount);
    }

    @Test
    public void test_Query_All_Transactions(){

        //given
        String transactionMaker = "Steven";
        String transactionRecipient = "Ramu";
        Integer senderBankAccountNumber = 1123111;
        Integer recipientBankAccountNumber = 44312111;
        Double amount = 2111.90;
        Currency currency = Currency.EUR;
        TransactionType transactionType =TransactionType.TRANSFER;
        LocalDateTime creationDate = null;
        LocalDateTime updateDate = null;
        TransactionStatus transactionStatus = TransactionStatus.SUCCESSFUL;
        List<String> exceptionMessages = null;
        String message = "Successful Transfer for amount 2111.90 from Steven to Ramu";

        Transaction transaction_For_Creation = getATransactionEntityFromParameters( transactionMaker,
                transactionRecipient,  senderBankAccountNumber,
                recipientBankAccountNumber,  amount,
                currency,  transactionType,
                creationDate,  updateDate,
                transactionStatus, exceptionMessages,  message);

        Transaction transaction_Created = transactionRepository.createTransaction(transaction_For_Creation);

         transactionRecipient = "Ramu";
         recipientBankAccountNumber = 44312111;
         amount = 2111.90;
         currency = Currency.EUR;
         transactionType =TransactionType.DEPOSIT;
         creationDate = null;
         updateDate = null;
         transactionStatus = TransactionStatus.SUCCESSFUL;
         exceptionMessages = null;
         message = "Successful Deposit for amount 2111.90 from Steven to Ramu";

        Transaction depositTransaction_For_Creation = getATransactionEntityFromParameters( null,
                transactionRecipient,  null,
                recipientBankAccountNumber,  amount,
                currency,  transactionType,
                creationDate,  updateDate,
                transactionStatus, exceptionMessages,  message);

        Transaction depositTransaction_Created = transactionRepository.createTransaction(depositTransaction_For_Creation);

        //when
        List<Transaction> transactions_Queried = transactionRepository.getAllTransactions();

        //then
        assertNotNull(transactions_Queried);
        assertEquals(2,transactions_Queried.size());
    }

    @Test
    public void test_Query_Transaction_By_TransactionId(){

        //given
        String transactionMaker = "Steven";
        String transactionRecipient = "Ramu";
        Integer senderBankAccountNumber = 1123111;
        Integer recipientBankAccountNumber = 44312111;
        Double amount = 2111.90;
        Currency currency = Currency.EUR;
        TransactionType transactionType =TransactionType.TRANSFER;
        LocalDateTime creationDate = null;
        LocalDateTime updateDate = null;
        TransactionStatus transactionStatus = TransactionStatus.SUCCESSFUL;
        List<String> exceptionMessages = null;
        String message = "Successful Transfer for amount 2111.90 from Steven to Ramu";

        Transaction transaction_For_Creation = getATransactionEntityFromParameters( transactionMaker,
                transactionRecipient,  senderBankAccountNumber,
                recipientBankAccountNumber,  amount,
                currency,  transactionType,
                creationDate,  updateDate,
                transactionStatus, exceptionMessages,  message);

        Transaction transaction_Created = transactionRepository.createTransaction(transaction_For_Creation);

        transactionRecipient = "Ramu";
        recipientBankAccountNumber = 44312111;
        amount = 2111.90;
        currency = Currency.EUR;
        transactionType =TransactionType.DEPOSIT;
        creationDate = null;
        updateDate = null;
        transactionStatus = TransactionStatus.SUCCESSFUL;
        exceptionMessages = null;
        message = "Successful Deposit for amount 2111.90 from Steven to Ramu";

        Transaction depositTransaction_For_Creation = getATransactionEntityFromParameters( null,
                transactionRecipient,  null,
                recipientBankAccountNumber,  amount,
                currency,  transactionType,
                creationDate,  updateDate,
                transactionStatus, exceptionMessages,  message);

        Transaction depositTransaction_Created = transactionRepository.createTransaction(depositTransaction_For_Creation);

        //when
        Transaction transaction_Queried = transactionRepository.getATransactionByTransactionId(depositTransaction_Created.getTransactionId());

        //then
        assertNotNull(transaction_Queried);
        assertEquals(depositTransaction_Created.getTransactionId(),transaction_Queried.getTransactionId());
    }
    
    
    
    public Transaction getATransactionEntityFromParameters(String transactionMaker,
                                     String transactionRecipient, Integer senderBankAccountNumber,
                                     Integer recipientBankAccountNumber, Double amount,
                                     Currency currency, TransactionType transactionType,
                                     LocalDateTime creationDate, LocalDateTime updateDate,
                                     TransactionStatus transactionStatus, List<String> exceptionMessages, String message){

        Transaction transactionObject = new Transaction();
        transactionObject.setAmount(amount);
        transactionObject.setSenderBankAccountNumber(senderBankAccountNumber);
        transactionObject.setRecipientBankAccountNumber(recipientBankAccountNumber);
        transactionObject.setCreationDate(getCurrentTime());
        transactionObject.setCurrency(currency);
        transactionObject.setTransactionType(transactionType);
        transactionObject.setTransactionStatus(transactionStatus);
        transactionObject.setTransactionMaker(transactionMaker);
        transactionObject.setTransactionRecipient(transactionRecipient);
        transactionObject.setCreationDate(creationDate);
        transactionObject.setUpdateDate(updateDate);
        transactionObject.setExceptionMessages(exceptionMessages);
        transactionObject.setMessage(message);
        
        return transactionObject;
    }
    
}
