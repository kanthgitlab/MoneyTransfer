package io.revolut.moneytransfer.repository;

import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.entity.Account;
import io.revolut.moneytransfer.entity.Address;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.multiverse.api.StmUtils;
import org.multiverse.api.references.TxnDouble;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.multiverse.api.StmUtils.atomic;

public class AccountRepositoryTest {

    private AccountRepository accountRepository;

    @Before
    public void init(){
        accountRepository = new AccountRepository();
    }

    @After
    public void teardown(){
        accountRepository.cleanup();
        accountRepository = null;
    }

    @Test
    public void test_Create_New_Current_Account(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        //when
        Account account_Created = accountRepository.create(accountForCreation);

        //then
        assertNotNull(account_Created);
        assertTrue(account_Created.getIsActive());
        assertNotNull(account_Created.getCreationDate());
        assertEquals(currency,account_Created.getCurrency());
        assertEquals(accountType,account_Created.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance),Double.valueOf(account_Created.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Created.getNationalIdentityCode());
        assertEquals(address,account_Created.getAddress());
        assertEquals(accountHolderName,account_Created.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Created.getAccountNumber());
    }

    @Test
    public void test_Update_Existing_Current_Account(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created = accountRepository.create(accountForCreation);

        assertNotNull(account_Created);
        assertTrue(account_Created.getIsActive());
        assertNotNull(account_Created.getCreationDate());
        assertEquals(currency,account_Created.getCurrency());
        assertEquals(accountType,account_Created.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Created.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Created.getNationalIdentityCode());
        assertEquals(address,account_Created.getAddress());
        assertEquals(accountHolderName,account_Created.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Created.getAccountNumber());

        accountHolderName = "Steven Spicer";
        unitNumber = "#22-12";
        countryCode = "SGP";

        address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForUpdate = getAccountEntityBeanFromParameters(account_Created.getAccountNumber(),accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,accountForCreation.getCreationDate(),null);

        //when
        Account account_Updated = accountRepository.update(accountForUpdate);

        //then
        assertNotNull(account_Updated);
        assertTrue(account_Updated.getIsActive());
        assertEquals(address,account_Updated.getAddress());
        assertEquals(accountHolderName,account_Updated.getAccountHolderName());
        assertNotNull(account_Updated.getCreationDate());
        assertEquals(currency,account_Updated.getCurrency());
        assertEquals(accountType,account_Updated.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance),Double.valueOf(account_Updated.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Updated.getNationalIdentityCode());
        assertEquals(address,account_Updated.getAddress());
        assertEquals(accountHolderName,account_Updated.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Updated.getAccountNumber());
    }

    @Test
    public void test_Deactivate_Existing_Current_Account(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created = accountRepository.create(accountForCreation);

        assertNotNull(account_Created);
        assertTrue(account_Created.getIsActive());
        assertNotNull(account_Created.getCreationDate());
        assertEquals(currency,account_Created.getCurrency());
        assertEquals(accountType,account_Created.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Created.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Created.getNationalIdentityCode());
        assertEquals(address,account_Created.getAddress());
        assertEquals(accountHolderName,account_Created.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Created.getAccountNumber());

        //when
        Account account_Deactivated = accountRepository.deactivate(account_Created.getAccountNumber());

        //then
        assertNotNull(account_Deactivated);
        assertFalse(account_Deactivated.getIsActive());
    }

    @Test
    public void test_Activate_An_InActive_Current_Account(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created = accountRepository.create(accountForCreation);

        assertNotNull(account_Created);
        assertTrue(account_Created.getIsActive());
        assertNotNull(account_Created.getCreationDate());
        assertEquals(currency,account_Created.getCurrency());
        assertEquals(accountType,account_Created.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Created.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Created.getNationalIdentityCode());
        assertEquals(address,account_Created.getAddress());
        assertEquals(accountHolderName,account_Created.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Created.getAccountNumber());

        Account account_Deactivated = accountRepository.deactivate(account_Created.getAccountNumber());

        assertNotNull(account_Deactivated);
        assertFalse(account_Deactivated.getIsActive());

        //when
        Account account_Activated = accountRepository.activate(account_Created.getAccountNumber());

        //then
        assertNotNull(account_Activated);
        assertTrue(account_Activated.getIsActive());
    }

    @Test
    public void test_Update_Account_Balance(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created = accountRepository.create(accountForCreation);

        assertNotNull(account_Created);
        assertTrue(account_Created.getIsActive());
        assertNotNull(account_Created.getCreationDate());
        assertEquals(currency,account_Created.getCurrency());
        assertEquals(accountType,account_Created.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Created.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Created.getNationalIdentityCode());
        assertEquals(address,account_Created.getAddress());
        assertEquals(accountHolderName,account_Created.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Created.getAccountNumber());

        TxnDouble newBalance = StmUtils.newTxnDouble(12.31);
        account_Created.setAccountBalance(newBalance);

        //when
        Account account_Updated = accountRepository.update(account_Created);

        //then
        assertNotNull(account_Updated);
        assertTrue(account_Updated.getIsActive());
        assertEquals(address,account_Updated.getAddress());
        assertEquals(accountHolderName,account_Updated.getAccountHolderName());
        assertNotNull(account_Updated.getCreationDate());
        assertEquals(currency,account_Updated.getCurrency());
        assertEquals(accountType,account_Updated.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(newBalance.get()),Double.valueOf(account_Updated.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Updated.getNationalIdentityCode());
        assertEquals(address,account_Updated.getAddress());
        assertEquals(accountHolderName,account_Updated.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Updated.getAccountNumber());
    }

    @Test
    public void test_Delete_Account(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created = accountRepository.create(accountForCreation);

        assertNotNull(account_Created);
        assertTrue(account_Created.getIsActive());
        assertNotNull(account_Created.getCreationDate());
        assertEquals(currency,account_Created.getCurrency());
        assertEquals(accountType,account_Created.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Created.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Created.getNationalIdentityCode());
        assertEquals(address,account_Created.getAddress());
        assertEquals(accountHolderName,account_Created.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Created.getAccountNumber());

        //when
        Boolean accountDeletionStatus = accountRepository.delete(account_Created.getAccountNumber());

        //then
        assertNotNull(accountDeletionStatus);
        assertTrue(accountDeletionStatus);

        //Assert for Query by AccountNumber after deletion
        Account account_Queried_After_Deletion = accountRepository.getAnAccountByAccountNumber(account_Created.getAccountNumber());

        assertNull(account_Queried_After_Deletion);
    }

    @Test
    public void test_Get_An_Account_By_AccountNumber(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created = accountRepository.create(accountForCreation);

        //when
        Account account_Queried_By_AccountNumber = accountRepository.getAnAccountByAccountNumber(account_Created.getAccountNumber());

        //then
        assertNotNull(account_Queried_By_AccountNumber);
        assertNotNull(account_Queried_By_AccountNumber);
        assertTrue(account_Queried_By_AccountNumber.getIsActive());
        assertNotNull(account_Queried_By_AccountNumber.getCreationDate());
        assertEquals(currency,account_Queried_By_AccountNumber.getCurrency());
        assertEquals(accountType,account_Queried_By_AccountNumber.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Queried_By_AccountNumber.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Queried_By_AccountNumber.getNationalIdentityCode());
        assertEquals(address,account_Queried_By_AccountNumber.getAddress());
        assertEquals(accountHolderName,account_Queried_By_AccountNumber.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Queried_By_AccountNumber.getAccountNumber());

    }

    @Test
    public void test_Get_An_Active_Account_By_AccountNumber(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created = accountRepository.create(accountForCreation);

        //when
        Account account_Queried_By_AccountNumber = accountRepository.getAnActiveAccountByAccountNumber(account_Created.getAccountNumber());

        //then
        assertNotNull(account_Queried_By_AccountNumber);
        assertTrue(account_Queried_By_AccountNumber.getIsActive());
        assertNotNull(account_Queried_By_AccountNumber.getCreationDate());
        assertEquals(currency,account_Queried_By_AccountNumber.getCurrency());
        assertEquals(accountType,account_Queried_By_AccountNumber.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Queried_By_AccountNumber.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Queried_By_AccountNumber.getNationalIdentityCode());
        assertEquals(address,account_Queried_By_AccountNumber.getAddress());
        assertEquals(accountHolderName,account_Queried_By_AccountNumber.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Queried_By_AccountNumber.getAccountNumber());

        //Deactivate account and verify same method - Assert for null value after query
        accountRepository.deactivate(account_Queried_By_AccountNumber.getAccountNumber());

        //when
        Account account_Queried_After_Deactivation_By_AccountNumber = accountRepository.getAnActiveAccountByAccountNumber(account_Queried_By_AccountNumber.getAccountNumber());

        //then
        assertNull(account_Queried_After_Deactivation_By_AccountNumber);

    }

    @Test
    public void test_Get_An_Account_By_AccountNumber_And_AccountType(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created = accountRepository.create(accountForCreation);

        //when
        Account account_Queried_By_AccountNumber_And_AccountType = accountRepository.getAnAccountByAccountNumberAndAccountType(account_Created.getAccountNumber(),accountType);

        //then
        assertNotNull(account_Queried_By_AccountNumber_And_AccountType);
        assertTrue(account_Queried_By_AccountNumber_And_AccountType.getIsActive());
        assertNotNull(account_Queried_By_AccountNumber_And_AccountType.getCreationDate());
        assertEquals(currency,account_Queried_By_AccountNumber_And_AccountType.getCurrency());
        assertEquals(accountType,account_Queried_By_AccountNumber_And_AccountType.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Queried_By_AccountNumber_And_AccountType.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Queried_By_AccountNumber_And_AccountType.getNationalIdentityCode());
        assertEquals(address,account_Queried_By_AccountNumber_And_AccountType.getAddress());
        assertEquals(accountHolderName,account_Queried_By_AccountNumber_And_AccountType.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Queried_By_AccountNumber_And_AccountType.getAccountNumber());
    }


    @Test
    public void test_Get_An_Account_By_AccountHolderName_And_AccountType_And_Currency(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created = accountRepository.create(accountForCreation);

        //when
        Account account_Queried_By_AccountHolderName_And_AccountType = accountRepository.getAnAccountByAccountHolderNameAndAccountTypeAndCurrency(accountHolderName,accountType,currency);

        //then
        assertNotNull(account_Queried_By_AccountHolderName_And_AccountType);
        assertTrue(account_Queried_By_AccountHolderName_And_AccountType.getIsActive());
        assertNotNull(account_Queried_By_AccountHolderName_And_AccountType.getCreationDate());
        assertEquals(currency,account_Queried_By_AccountHolderName_And_AccountType.getCurrency());
        assertEquals(accountType,account_Queried_By_AccountHolderName_And_AccountType.getAccountType());
        atomic(() -> assertEquals(Double.valueOf(accountBalance), Double.valueOf(account_Queried_By_AccountHolderName_And_AccountType.getAccountBalance().get())));
        assertEquals(nationalIdentityCode,account_Queried_By_AccountHolderName_And_AccountType.getNationalIdentityCode());
        assertEquals(address,account_Queried_By_AccountHolderName_And_AccountType.getAddress());
        assertEquals(accountHolderName,account_Queried_By_AccountHolderName_And_AccountType.getAccountHolderName());
        assertEquals(Integer.valueOf(1),account_Queried_By_AccountHolderName_And_AccountType.getAccountNumber());
    }


    @Test
    public void test_Get_All_Accounts(){

        //given
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = Currency.EUR;
        Boolean isActive = true;

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        Account accountForCreation_Current = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created_1 = accountRepository.create(accountForCreation_Current);

        accountType = AccountType.SAVINGS;

        Account accountForCreation_Savings = getAccountEntityBeanFromParameters(null,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        Account account_Created_2 = accountRepository.create(accountForCreation_Savings);

        //when
        List<Account> accountList = accountRepository.getAllAccounts();

        //then
        assertNotNull(accountList);
        assertEquals(2,accountList.size());
        assertEquals(account_Created_1,accountList.get(0));
        assertEquals(account_Created_2,accountList.get(1));
    }


    public Account getAccountEntityBeanFromParameters(Integer accountNumber, String accountHolderName,
                                                      Address address, String nationalIdentityCode,
                                                      AccountType accountType, Double accountBalance,
                                                       Currency currency, Boolean isActive,
                                                      LocalDateTime creationDate, LocalDateTime updateDate){

        return Account.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(StmUtils.newTxnDouble(accountBalance))
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();

    }


    public Address getAddressFromParameters( String unitNumber,
                                             String buildingName,
                                             String streetOrLandmarkName,
                                             String postalCode,
                                             String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                .postalCode(postalCode).countryCode(countryCode).build();
    }

}
