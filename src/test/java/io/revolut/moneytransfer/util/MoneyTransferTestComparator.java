package io.revolut.moneytransfer.util;

import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.model.TransactionModel;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MoneyTransferTestComparator {

    public static void compareForTestMethods(AccountModel accountModel1,AccountModel accountModel2){

        assertEquals(accountModel1.getAccountNumber(),accountModel2.getAccountNumber());
        assertEquals(accountModel1.getAccountBalance(),accountModel2.getAccountBalance());
        assertEquals(accountModel1.getAddress(),accountModel2.getAddress());
        assertEquals(accountModel1.getIsActive(),accountModel2.getIsActive());
        assertEquals(accountModel1.getCurrency(),accountModel2.getCurrency());
        assertEquals(accountModel1.getAccountType(),accountModel2.getAccountType());
        assertEquals(accountModel1.getNationalIdentityCode(),accountModel2.getNationalIdentityCode());
        assertEquals(accountModel1.getAccountHolderName(),accountModel2.getAccountHolderName());
        assertEquals(accountModel1.getCreationDate().toLocalDate(),accountModel2.getCreationDate().toLocalDate());
    }

    public static void compareTransactionModelForTestMethods(TransactionModel transactionModel1, TransactionModel transactionModel2){

        assertEquals(transactionModel1.getTransactionId(),transactionModel2.getTransactionId());
        assertEquals(transactionModel1.getRecipientBankAccountNumber(),transactionModel2.getRecipientBankAccountNumber());
        assertEquals(transactionModel1.getTransactionMaker(),transactionModel2.getTransactionMaker());
        assertEquals(transactionModel1.getTransactionRecipient(),transactionModel2.getTransactionRecipient());
        assertEquals(transactionModel1.getCurrency(),transactionModel2.getCurrency());
        assertEquals(transactionModel1.getTransactionType(),transactionModel2.getTransactionType());
        assertEquals(transactionModel1.getTransactionStatus(),transactionModel2.getTransactionStatus());
        assertEquals(transactionModel1.getAmount(),transactionModel2.getAmount());
        assertEquals(transactionModel1.getCreationDate().toLocalDate(),transactionModel2.getCreationDate().toLocalDate());
        assertEquals(transactionModel1.getAccountBalance(),transactionModel2.getAccountBalance());
        assertEquals(transactionModel1.getMessage(),transactionModel2.getMessage());
    }

    public static void compareTransactionModelsForTestMethods(List<TransactionModel> transactionModels_1, List<TransactionModel> transactionModels_2){

        assertNotNull(transactionModels_1);
        assertNotNull(transactionModels_2);

        int size1 = transactionModels_1.size();
        int size2 = transactionModels_2.size();

        assertEquals(size1,size2);

        for(int index = 0 ; index < size1; index++){
            compareTransactionModelForTestMethods(transactionModels_1.get(index),transactionModels_2.get(index));
        }

    }


}
