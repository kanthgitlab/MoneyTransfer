package io.revolut.moneytransfer.util;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;

public class TimeManagementUtilTest {

    @Test
    public void test_Get_CurrentTime(){

        //when
        LocalDateTime currentTime = TimeManagementUtil.getCurrentTime();

        //then
        assertNotNull(currentTime);
    }
}
