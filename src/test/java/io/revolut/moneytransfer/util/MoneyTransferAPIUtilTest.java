package io.revolut.moneytransfer.util;

import io.revolut.moneytransfer.constants.AccountType;
import io.revolut.moneytransfer.constants.Currency;
import io.revolut.moneytransfer.constants.TransactionStatus;
import io.revolut.moneytransfer.constants.TransactionType;
import io.revolut.moneytransfer.entity.Account;
import io.revolut.moneytransfer.entity.Address;
import io.revolut.moneytransfer.entity.Transaction;
import io.revolut.moneytransfer.model.AccountModel;
import io.revolut.moneytransfer.model.TransactionModel;
import org.junit.Test;
import org.multiverse.api.StmUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static io.revolut.moneytransfer.constants.Currency.EUR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.multiverse.api.StmUtils.atomic;

public class MoneyTransferAPIUtilTest {

    /**
     * test validate transactionAmount
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_Validate_Negative_Amount() {

        //given
        Double amount = -123.21;

        //when
        MoneyTransferAPIUtil.validateAmount(amount);

        //then
        //Assert for IllegalArgumentException as Expected
        //Annotated part of the function-Signature will perform the test-assertion
    }

    /**
     * test validate transactionAmount
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_Validate_Null_Amount() {

        //given
        Double amount = null;

        //when
        MoneyTransferAPIUtil.validateAmount(amount);

        //then
        //Assert for IllegalArgumentException as Expected
        //Annotated part of the function-Signature will perform the test-assertion
    }

    @Test
    public  void test_Get_AccountModel_From_Account() {

         Integer accountNumber = 1;
         String accountHolderName = "Steve";
         String unitNumber = "#01-22";
         String buildingName = "New Castle";
         String streetOrLandmarkName = "Flora Drive";
         String postalCode= "5001213";
         String countryCode = "SG";
         String nationalIdentityCode = "SG55415G";
         AccountType accountType = AccountType.CURRENT;
         Double accountBalance = 1000.10;
         BigDecimal lockedBalance = BigDecimal.valueOf(0);
         Currency currency = EUR;
         Boolean isActive = true;
         LocalDateTime creationDate = LocalDateTime.of(2019,06,22,23,00,00);
         LocalDateTime updateDate = creationDate.plusDays(1);

         Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

         Account account = getAccountEntityBeanFromParameters(accountNumber,accountHolderName,address,
                                                              nationalIdentityCode,accountType,accountBalance,
                                                              currency,isActive,creationDate,updateDate);

         AccountModel accountModel_Expected = getAccountModelBeanFromParameters(accountNumber,accountHolderName,address,
                 nationalIdentityCode,accountType,accountBalance,
                 currency,isActive,creationDate,updateDate);

         //when
         AccountModel accountModel_Actual = MoneyTransferAPIUtil.getAccountModel(account);

         //then
         assertNotNull(accountModel_Actual);
         assertEquals(accountModel_Expected,accountModel_Actual);
    }

    @Test
    public  void test_Get_Account_From_AccountModel() {

        Integer accountNumber = 1;
        String accountHolderName = "Steve";
        String unitNumber = "#01-22";
        String buildingName = "New Castle";
        String streetOrLandmarkName = "Flora Drive";
        String postalCode= "5001213";
        String countryCode = "SG";
        String nationalIdentityCode = "SG55415G";
        AccountType accountType = AccountType.CURRENT;
        Double accountBalance = 1000.10;
        Currency currency = EUR;
        Boolean isActive = true;
        LocalDateTime creationDate = LocalDateTime.of(2019,06,22,23,00,00);
        LocalDateTime updateDate = creationDate.plusDays(1);

        Address address = getAddressFromParameters(unitNumber,buildingName,streetOrLandmarkName,postalCode,countryCode);

        AccountModel accountModel = getAccountModelBeanFromParameters(accountNumber,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,creationDate,updateDate);


        Account account_Expected = getAccountEntityBeanFromParameters(accountNumber,accountHolderName,address,
                nationalIdentityCode,accountType,accountBalance,
                currency,isActive,null,null);

        //when
        Account account_Actual = MoneyTransferAPIUtil.getAccount(accountModel);

        //then
        assertNotNull(account_Actual);
        assertEquals(account_Expected.getAccountHolderName(),account_Actual.getAccountHolderName());
        assertEquals(account_Expected.getAccountNumber(),account_Actual.getAccountNumber());
        assertEquals(account_Expected.getAddress(),account_Actual.getAddress());
        assertEquals(account_Expected.getAccountType(),account_Actual.getAccountType());
        assertEquals(account_Expected.getCurrency(),account_Actual.getCurrency());
        assertEquals(account_Expected.getIsActive(),account_Actual.getIsActive());
        assertEquals(account_Expected.getNationalIdentityCode(),account_Actual.getNationalIdentityCode());
        atomic(() -> assertEquals(Double.valueOf(account_Expected.getAccountBalance().get()),Double.valueOf(account_Actual.getAccountBalance().get())));
    }

    @Test
    public void test_Get_TransactionModel_From_Transaction(){

        String transactionId = "TRN_TRNSFR_4434132"; 
        String transactionMaker = "Jack";
        String transactionRecipient = "Eden";
        Integer senderBankAccountNumber = 12124423;
        Currency senderBankAccountCurrency = EUR;
        Integer recipientBankAccountNumber = 55232111;
        Currency recipientBankAccountCurrency = EUR;
        Double amount = 11200.1211;
        Currency currency = EUR;
        TransactionType transactionType = TransactionType.TRANSFER;
        Double transactionFees = 0D;
        Double exchangeRate = 1D;
        LocalDateTime creationDate = LocalDateTime.of(2019,06,22,23,00,12);
        LocalDateTime updateDate = null;
        TransactionStatus transactionStatus = TransactionStatus.FAILED;
        String message = null;
        String errorDetails = "Failed due to Insufficient Balance in Sender with AccountNumber: 12124423";

        Transaction transaction = getATransactionFromParameters(transactionId,  transactionMaker,
                transactionRecipient,  senderBankAccountNumber,senderBankAccountCurrency, recipientBankAccountNumber,recipientBankAccountCurrency,
                amount,currency,transactionType,transactionFees,exchangeRate, creationDate,updateDate,transactionStatus, Arrays.asList(errorDetails));

        TransactionModel transactionModel_Expected = getATransactionModelFromParameters( transactionId,  transactionMaker,
                                            transactionRecipient,  senderBankAccountNumber,senderBankAccountCurrency,
                                            recipientBankAccountNumber,recipientBankAccountCurrency,
                                            amount,currency,transactionType,transactionFees,exchangeRate,
                                            creationDate,updateDate,transactionStatus,message,errorDetails);

        //when
        TransactionModel transactionModel_actual = MoneyTransferAPIUtil.getTransactionModel(transaction);

        //then
        assertNotNull(transactionModel_actual);

        assertEquals(transactionModel_Expected,transactionModel_actual);
    }
    
    
    public Account getAccountEntityBeanFromParameters( Integer accountNumber, String accountHolderName,
                                                       Address address, String nationalIdentityCode,
                                                        AccountType accountType, Double accountBalance,
                                                        Currency currency, Boolean isActive,
                                                        LocalDateTime creationDate, LocalDateTime updateDate){

        return Account.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                                        .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                                        .accountBalance(StmUtils.newTxnDouble(accountBalance))
                                        .currency(currency).isActive(isActive)
                                        .creationDate(creationDate).updateDate(updateDate).build();

    }

    public AccountModel getAccountModelBeanFromParameters( Integer accountNumber, String accountHolderName,
                                                       Address address, String nationalIdentityCode,
                                                       AccountType accountType, Double accountBalance,
                                                       Currency currency, Boolean isActive,
                                                       LocalDateTime creationDate, LocalDateTime updateDate){

        return AccountModel.builder().accountNumber(accountNumber).accountHolderName(accountHolderName).address(address)
                .nationalIdentityCode(nationalIdentityCode).accountType(accountType)
                .accountBalance(accountBalance)
                .currency(currency).isActive(isActive)
                .creationDate(creationDate).updateDate(updateDate).build();

    }

    public Address getAddressFromParameters( String unitNumber,
             String buildingName,
             String streetOrLandmarkName,
             String postalCode,
             String countryCode){

        return Address.builder().unitNumber(unitNumber).buildingName(buildingName)
                                .streetOrLandmarkName(streetOrLandmarkName).countryCode(countryCode)
                                .postalCode(postalCode).countryCode(countryCode).build();
    }

    public TransactionModel getATransactionModelFromParameters(String transactionId, String transactionMaker,
                                                               String transactionRecipient, Integer senderBankAccountNumber,
                                                               Currency senderBankAccountCurrency,
                                                               Integer recipientBankAccountNumber,
                                                               Currency recipientBankAccountCurrency,
                                                               Double amount, Currency currency, TransactionType transactionType,
                                                               Double transactionFees,Double exchangeRate,
                                                               LocalDateTime creationDate, LocalDateTime updateDate,
                                                               TransactionStatus transactionStatus, String message,String errorDetails){

        return TransactionModel.builder().transactionId(transactionId).transactionMaker(transactionMaker)
                                         .transactionRecipient(transactionRecipient)
                                         .senderBankAccountNumber(senderBankAccountNumber)
                                         .senderBankAccountCurrency(senderBankAccountCurrency)
                                         .recipientBankAccountNumber(recipientBankAccountNumber)
                                         .recipientBankAccountCurrency(recipientBankAccountCurrency)
                                         .transactionStatus(transactionStatus).amount(amount)
                                         .transactionFees(transactionFees).exchangeRate(exchangeRate)
                                         .creationDate(creationDate).updateDate(updateDate)
                                         .currency(currency).transactionType(transactionType)
                                         .message(message).errorDetails(errorDetails).build();
    }


    public Transaction getATransactionFromParameters(String transactionId, String transactionMaker,
                                                     String transactionRecipient, Integer senderBankAccountNumber,
                                                     Currency senderBankAccountCurrency,
                                                     Integer recipientBankAccountNumber,
                                                     Currency recipientBankAccountCurrency,
                                                     Double amount, Currency currency, TransactionType transactionType,
                                                     Double transactionFees,Double exchangeRate,
                                                     LocalDateTime creationDate, LocalDateTime updateDate,
                                                     TransactionStatus transactionStatus, List<String> exceptionMessages){

        return Transaction.builder().transactionId(transactionId).transactionMaker(transactionMaker)
                .transactionRecipient(transactionRecipient)
                .senderBankAccountNumber(senderBankAccountNumber)
                .senderBankAccountCurrency(senderBankAccountCurrency)
                .recipientBankAccountNumber(recipientBankAccountNumber)
                .recipientBankAccountCurrency(recipientBankAccountCurrency)
                .transactionStatus(transactionStatus).amount(amount)
                .transactionFees(transactionFees).exchangeRate(exchangeRate)
                .creationDate(creationDate).updateDate(updateDate)
                .currency(currency).transactionType(transactionType).exceptionMessages(exceptionMessages).build();

    }
}