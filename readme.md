# MoneyTransfer App

## Use Case
 Design and implement a RESTful API (including data model and the backing implementation) for money transfers between accounts.

## MoneyTransfer API
- Create Multi-Currency Savings & Current Accounts
- Update Non-Financial details of Account
- Modify/Update accountBalance
- De-Activate Account
- Re-Activate Account
- Delete Account
- Get All Accounts
- Get AccountDetails using AccountNumber
- Transfer amount between accounts
- Withdraw Money from Account
- Deposit Money to Account
- Transfer Money across currencyAccounts of an AccountHolder
- Transfer Money to another AccountHolder
- Get All Transactions
- Get A Transaction by transactionId

# Tech Stack

## Development:
  - Java-8
  - Maven 3
  - Jersey Library
  - jersey-container-grizzly2-http for web-server or container
  - lombok for auto setter/getter/toString/hashcode/equals/builder
  - slf4j for logging
  - **multiverse** for Software Transactional Management (STM) -> Lock free approach

## Testing:
   - Junit
   - Mockito
   - Hamcrest Matchers

## Design approach:
  - REST API to invoke service methods 
  - Key-Value DataStore using HashMap (In-memory) for Multi-Currency-Accounts and Transactions 
  - Implemented on **SOLID** design principles
  - Concurrent request handling support for shared-state. (Thread Safe)
  
## Project Structure and Package and Class Details: 

[Project_Structure_Package_Details](./docs/Project_Structure_Package_Details.md)


## Dependency Injection Design:

- MoneyTransferAPI business logic holder needs AccountService,
  TransactionService for it’s business.

- It does not create them but asks for them (the dependencies).

- The factory (MoneyTransferAPIFactory) creates those dependencies and
  wire them to MoneyTransferAPI. 

- The main method in the application launcher first instantiates the
  factory, the factory in turn instantiates the object graph, each
  Object declares their dependencies and finally the main method itself
  sets the ball rolling 
  
- Launch the application by invoking main method of MoneyTransferAPI,
  ie. the objects starts talking to each other executing the usual
  business.

 Summary: Create the factory, create the application using the factory
 and then start the application!
 
## Concurrency and Transaction Handling 

### Software-Transactional-Memory

<p>Concurrency and transactional-Management is achieved using <b>multiverse</b> implementation for STM (Software Transactional Memory)</p>
<p>The word “multiverse” means more than one universe and is also the name of a software transactional memory library.</p>
<p>Multiverse allows us to turn a normal Java application into a transactional one, enabling most of the features normally provided by database transactions.</p>

#### Key Points on STM Approach:
<ul>
<li>Transactions are an essential part of any business critical application as these ensure the integrity of the data being managed by the same application.</li>
<li>Transactions ensure that the data remains in a consistent and integral state after this is manipulated, thus mitigating the risk of invalid state. </li>
<li>a Software Transactional Memory, to provide transactions at the software level without using any databases.</li>
</ul>


</ol>

 <p>STM Properties in Account Entity:</p>
  <ol>
      <li> accountBalance (TxnDouble)
      <li> lastModified ( TxnRef<LocalDateTime> ) </li>
      <li> stm (GammaStm)</li> 
   </ol>

  <p><b> Atomic Transactions: </b></p>
  <p>All transactions on Account are done with in the Atomic-Transaction boundary.
     Atomic transactions are handle and managed by multiverse.  </p>

- The timestamp when the balance was last modified and the current balance are bound together and these two need to be updated together. 
- This is very important to understand. If we fail to update one of the fields, then none of the fields should be updated. 
- This property is also known as atomic. 
- If you try to withdraw/transfer more money than you currently have, the withdrawal should fail and both the date and the balance should remain unchanged.
- In an atomic transaction, a series of operations either all occur, or nothing occurs 


#### State Mutation in Transactional Boundary:

<p>Bank-Account, which has two transactionally-mutable fields, the accountBalance and the date when this was last modified as shown in the following image.</p>

![Alt text](docs/Withdrawal_Transition.png?raw=true "withdrawal transition")

<p> Mutable fields are fields which can be modified with in a transactional-Boundary after the object is created. </p>
<p> On the other hand, non-financial immutable are fields which value will not change once the object is created.</p>
<p> and non-financial mutable are fields which value can change out of transactional-Boundary.</p>
  
# Data-Model:

## Entity Beans:

### Account

<ul>Account is the stored in In-Memory Key-Value DataStore (HashMap)
    <li>  Key: accountNumber </li>
    <li> Value: Account Object    </li>
</ul>    


<p>
Account Object has Transactional Reference Objects for
accountBalance and lastModified.
These 2 properties are managed by Software Transactional management in multiverse library.
</p>
      
  <b>Parameters:</b>  
  <ol>
      <li> accountBalance (TxnDouble)
      <li> lastModified ( TxnRef<LocalDateTime> ) </li>
      <li> stm (GammaStm)</li> 
      <li> accountNumber (Integer)</li>
      <li> accountHolderName (String)</li>
      <li> address (Address)</li>
      <li> nationalIdentityCode (String)</li>
      <li> accountType (AccountType)</li>
      <li> currency (Currency)</li>
     <li> isActive (Boolean)</li>
     <li> creationDate (LocalDateTime)</li>
     <li> updateDate (LocalDateTime)</li>
</ol>

### Transaction
  
<ul>    
<li> Transaction is stored in In-Memory Key-Value DataStore (HashMap) </li>
<li> Key: TransactionId</li>
<li> Value: Transaction Object</li>
</ul>

  <b>Parameters:</b>
     <ol>
     <li> transactionId (String)</li>
     <li> transactionMaker (String)</li>
     <li> transactionRecipient (String)</li>
     <li>  senderBankAccountNumber (Integer)</li>
     <li>  recipientBankAccountNumber (Integer)</li>
     <li>  amount (Double)</li>
     <li>  accountBalance (Double)</li>
     <li>  creationDate (LocalDateTime)</li>
     <li>  updateDate (LocalDateTime)</li>
     <li> transactionStatus (TransactionStatus)</li>
     <li> exceptionMessages (List<String>)</li>
     <li> message (String)</li>
    </ol>

## Response Models:

-    Model Objects equivalent to the json request & response for API

### AccountModel:

- Account-Model is the Object representation of API Response for account query and account operational activities

-  Parameters: 

### TransactionModel:
    
<p> TransactionModel is the Object representation of API Response for transactional activities </p>
    
  <b>Parameters:</b><ol>
    <li>  transactionId (String) </li>
     <li>  transactionMaker (String)</li>
     <li>  transactionRecipient (String)</li>
     <li>  senderBankAccountNumber (Integer)</li>
     <li>  recipientBankAccountNumber (Integer)</li>
     <li>  amount (Double)</li>
     <li>  accountBalance (Double)</li>
     <li>  creationDate (LocalDateTime)</li>
     <li>  updateDate (LocalDateTime)</li>
     <li> transactionStatus (TransactionStatus)</li>
     <li> exceptionMessages (List<String>)</li>
     <li> message (String)</li>
    </ol>

  
## Account Operations API:

- Create Multi-Currency Savings & Current Accounts
- Update Non-Financial details of Account
- Modify/Update accountBalance
- De-Activate Account
- Re-Activate Account
- Delete Account
- Get All Accounts
- Get All Active Accounts
- Get AccountDetails using AccountNumber
    
### Create Account
    
    POST http://localhost:8080/revolut/accounts
    {
        "accountHolderName":"lakshmikanth",
        "address":{
            "unitNumber":"#01-22",
            "buildingName":"5 Flora Drive",
            "streetOrLandmarkName":"Flora Road",
            "postalCode":"507011",
            "countryCode":"SG"
        },
        "nationalIdentityCode": "SG441291F",
        "accountType":"SAVINGS",
        "accountBalance":5000,
        "lockedBalance":0,
        "currency":"EUR"
    }
    
    Response:
    
    HTTP 200 OK
    POST http://localhost:8080/revolut/accounts
    {
        "accountNumber": 1,
        "accountHolderName": "lakshmikanth",
        "address": {
            "unitNumber": "#01-22",
            "buildingName": "5 Flora Drive",
            "streetOrLandmarkName": "Flora Road",
            "postalCode": "507011",
            "countryCode": "SG"
        },
        "nationalIdentityCode": "SG441291F",
        "accountType": "SAVINGS",
        "accountBalance": 5000,
        "lockedBalance": null,
        "currency": "EUR",
        "isActive": true,
        "creationDate": "2019-07-10 19:55:23",
        "updateDate": null
    }
 
### Modify/Update Account Details

    PUT http://localhost:8080/revolut/accounts/update/
    {
        "accountNumber": 1,
        "accountHolderName":"lakshmikanth",
        "address":{
            "unitNumber":"#02-21",
            "buildingName":"24 Baker Street",
            "streetOrLandmarkName":"victoria Avenue",
            "postalCode":"223111",
            "countryCode":"SG"
        },
        "nationalIdentityCode": "SG443121F",
        "accountType":"SAVINGS",
        "accountBalance":1000,
        "lockedBalance":0,
        "currency":"EUR"
    }

    Response:

    HTTP 200 OK
    PUT http://localhost:8080/revolut/accounts/update/
    {
        "accountNumber": 1,
        "accountHolderName": "lakshmikanth",
        "address": {
            "unitNumber": "#02-21",
            "buildingName": "24 Baker Street",
            "streetOrLandmarkName": "victoria Avenue",
            "postalCode": "223111",
            "countryCode": "SG"
        },
        "nationalIdentityCode": "SG443121F",
        "accountType": "SAVINGS",
        "accountBalance": 5000,
        "lockedBalance": null,
        "currency": "EUR",
        "isActive": true,
        "creationDate": "2019-07-10 19:55:23",
        "updateDate": "2019-07-10 19:59:26"
    }

### Modify/Update Account Balance

    PUT http://localhost:8080/revolut/accounts/balanceUpdate/
    {
        "accountNumber": 1,
        "accountHolderName":"lakshmikanth",
        "accountType":"SAVINGS",
        "accountBalance":12050,
        "currency":"EUR"
    }

    Response:

    HTTP 200 OK
    PUT http://localhost:8080/revolut/accounts/update/
    {
        "accountNumber": 1,
        "accountHolderName": "lakshmikanth",
        "address": {
            "unitNumber": "#02-21",
            "buildingName": "24 Baker Street",
            "streetOrLandmarkName": "victoria Avenue",
            "postalCode": "223111",
            "countryCode": "SG"
        },
        "nationalIdentityCode": "SG443121F",
        "accountType": "SAVINGS",
        "accountBalance": 12050,
        "lockedBalance": null,
        "currency": "EUR",
        "isActive": true,
        "creationDate": "2019-07-10 19:55:23",
        "updateDate": "2019-07-10 20:01:02"
    }

### De-Activate Account


    POST /revolut/accounts/deactivate?accountNumber=1

    Response:

    HTTP 200 OK
    POST http://localhost:8080/revolut/accounts/deactivate?accountNumber=1
    {
        "accountNumber": 1,
        "accountHolderName": "lakshmikanth",
        "address": {
            "unitNumber": "#02-21",
            "buildingName": "24 Baker Street",
            "streetOrLandmarkName": "victoria Avenue",
            "postalCode": "223111",
            "countryCode": "SG"
        },
        "nationalIdentityCode": "SG443121F",
        "accountType": "SAVINGS",
        "accountBalance": 12050,
        "lockedBalance": null,
        "currency": "EUR",
        "isActive": false,
        "creationDate": "2019-07-10 19:55:23",
        "updateDate": "2019-07-10 21:46:50"
    }

### Re-Activate Account

    POST http://localhost:8080/revolut/accounts/activate?accountNumber=1

    Response:

    HTTP 200 OK
    POST http://localhost:8080/revolut/accounts/activate?accountNumber=1
    {
        "accountNumber": 1,
        "accountHolderName": "lakshmikanth",
        "address": {
            "unitNumber": "#02-21",
            "buildingName": "24 Baker Street",
            "streetOrLandmarkName": "victoria Avenue",
            "postalCode": "223111",
            "countryCode": "SG"
        },
        "nationalIdentityCode": "SG443121F",
        "accountType": "SAVINGS",
        "accountBalance": 12050,
        "lockedBalance": null,
        "currency": "EUR",
        "isActive": true,
        "creationDate": "2019-07-10 19:55:23",
        "updateDate": "2019-07-10 21:47:58"
    }
    
### Delete Account

    DELETE http://localhost:8080/revolut/accounts/delete?accountNumber=3

    Response:

    HTTP 200 OK
    DELETE http://localhost:8080/revolut/accounts/delete?accountNumber=3
    true

   
### List all Accounts

    GET http://localhost:8080/revolut/accounts

    Response:

    HTTP 200 OK
    GET http://localhost:8080/revolut/accounts
    [
        {
            "accountNumber": 1,
            "accountHolderName": "lakshmikanth",
            "address": {
                "unitNumber": "#01-22",
                "buildingName": "5 Flora Drive",
                "streetOrLandmarkName": "Flora Road",
                "postalCode": "507011",
                "countryCode": "SG"
            },
            "nationalIdentityCode": "SG441291F",
            "accountType": "SAVINGS",
            "accountBalance": 5000,
            "lockedBalance": null,
            "currency": "EUR",
            "isActive": true,
            "creationDate": "2019-07-10 19:55:23",
            "updateDate": null
        },
        {
            "accountNumber": 2,
            "accountHolderName": "ramesh",
            "address": {
                "unitNumber": "#21-22",
                "buildingName": "5 toapayoh Avenue",
                "streetOrLandmarkName": "Toapayoh Road",
                "postalCode": "543112",
                "countryCode": "SG"
            },
            "nationalIdentityCode": "SG998721F",
            "accountType": "SAVINGS",
            "accountBalance": 1000,
            "lockedBalance": null,
            "currency": "USD",
            "isActive": true,
            "creationDate": "2019-07-10 19:57:43",
            "updateDate": null
        },
        {
            "accountNumber": 3,
            "accountHolderName": "jackRyan",
            "address": {
                "unitNumber": "#01-22",
                "buildingName": "5 Flora Drive",
                "streetOrLandmarkName": "Flora Road",
                "postalCode": "507011",
                "countryCode": "SG"
            },
            "nationalIdentityCode": "SG441291F",
            "accountType": "CURRENT",
            "accountBalance": 5000,
            "lockedBalance": null,
            "currency": "EUR",
            "isActive": true,
            "creationDate": "2019-07-10 19:57:52",
            "updateDate": null
        }
    ]

### Get Account Details By AccountNumber

    GET http://localhost:8080/revolut/accounts/1

    Response:

    HTTP 200 OK
    GET http://localhost:8080/revolut/accounts/1
     {
        "accountNumber": 1,
        "accountHolderName": "lakshmikanth",
        "address": {
            "unitNumber": "#01-22",
            "buildingName": "5 Flora Drive",
            "streetOrLandmarkName": "Flora Road",
            "postalCode": "507011",
            "countryCode": "SG"
        },
        "nationalIdentityCode": "SG441291F",
        "accountType": "SAVINGS",
        "accountBalance": 5000,
        "lockedBalance": null,
        "currency": "EUR",
        "isActive": true,
        "creationDate": "2019-07-10 19:55:23",
        "updateDate": null
    }
   
   
## Transaction API

- Withdraw Money from Account
- Deposit Money to Account
- Transfer Money across currencyAccounts of an AccountHolder
- Transfer Money to another AccountHolder
- Get All Transactions
- Get A Transaction by transactionId
  
  ```  
  transaction **(Atomic & Immutable)** is created  to initiate and complete the money transfer from sender to receiver

    Successful transfer:
        httpStatus OK 
     
     Failed transfer:
        httpStatus 500 - Internal Server Error
  ```
  
API endpoint below initiates a new transaction 

- Prerequisites:
    1. Validity of Sender and Recipient's BankAccounts
    2. Valid non-zero and positive transfer amount
    3. Sufficient Account Balance in Sender's bank account
    
    On Successful validation, funds will be transferred/withdrawn/deposited to Recipient 's Account

### Withdraw Money

    POST http://localhost:8080/revolut/transaction/withdraw
    
    {
        "transactionMaker":"Jack",
        "transactionRecipient":"lakshmi",
        "bankAccountNumber":1,
        "amount":1000.00,
        "currency":"EUR",
        "transactionType":"WITHDRAWAL"
    }
    
    Response:

    HTTP 200 OK
    POST http://localhost:8080/revolut/transaction/withdraw
    {
        "transactionId": "TRN_WITHDRAWAL_71c11e0a-6058-4769-85e6-b0204f279449",
        "transactionMaker": "Jack",
        "transactionRecipient": "lakshmi",
        "senderBankAccountNumber": null,
        "senderBankAccountCurrency": null,
        "recipientBankAccountNumber": 1,
        "recipientBankAccountCurrency": "EUR",
        "amount": 1000,
        "currency": "EUR",
        "transactionType": "WITHDRAWAL",
        "accountBalance": 3109.6039500000006,
        "transactionFees": 0,
        "exchangeRate": null,
        "creationDate": "2019-07-11 14:46:30",
        "updateDate": null,
        "transactionStatus": "SUCCESSFUL",
        "message": "SUCCESSFUL Withdrawal Transaction for amount: 1000.0 EUR from BankAccountNumber: 1",
        "errorDetails": ""
    }

### Deposit Money

    POST http://localhost:8080/revolut/transaction/deposit
    {
        "transactionMaker":"Jack",
        "transactionRecipient":"Jack",
        "bankAccountNumber":1,
        "amount":100,
        "currency":"EUR",
        "transactionType":"DEPOSIT"
    }
    
    Response:

    HTTP 200 OK
    POST http://localhost:8080/revolut/transaction/deposit
    {
        "transactionId": "TRN_DEPOSIT_6d5dff15-fde2-42ff-a313-bdc53b6f0342",
        "transactionMaker": "Jack",
        "transactionRecipient": "Jack",
        "senderBankAccountNumber": null,
        "senderBankAccountCurrency": null,
        "recipientBankAccountNumber": 1,
        "recipientBankAccountCurrency": "EUR",
        "amount": 100,
        "currency": "EUR",
        "transactionType": "DEPOSIT",
        "accountBalance": 4109.603950000001,
        "transactionFees": 0,
        "exchangeRate": null,
        "creationDate": "2019-07-11 14:45:54",
        "updateDate": null,
        "transactionStatus": "SUCCESSFUL",
        "message": "SUCCESSFUL Deposit Transaction for amount: 100.0 EUR to BankAccountNumber: 1",
        "errorDetails": ""
    }

### Transfer Money (Successful)

    POST http://localhost:8080/revolut/transaction/transfer
    {
        "transactionMaker":"lakshmikanth",
        "transactionRecipient":"jack",
        "senderBankAccountNumber":1,
        "recipientBankAccountNumber": 4,
        "amount":100,
        "currency":"EUR",
        "transactionType":"TRANSFER"
    }
    
    Response:

    HTTP 200 OK
    POST http://localhost:8080/revolut/transaction/transfer
    {
        "transactionId": "TRN_TRANSFER_1a4b1e1f-aa95-4959-903f-9006ae203350",
        "transactionMaker": "lakshmikanth",
        "transactionRecipient": "jack",
        "senderBankAccountNumber": 1,
        "senderBankAccountCurrency": "EUR",
        "recipientBankAccountNumber": 3,
        "recipientBankAccountCurrency": "EUR",
        "amount": 100,
        "currency": "EUR",
        "transactionType": "TRANSFER",
        "accountBalance": 4900,
        "transactionFees": 0,
        "exchangeRate": null,
        "creationDate": "2019-07-11 14:43:59",
        "updateDate": null,
        "transactionStatus": "SUCCESSFUL",
        "message": "SUCCESSFUL MoneyTransfer Transaction for amount: 100.0 EUR from senderBankAccountNumber: 1 to recipientBankAccountNumber: 3",
        "errorDetails": ""
    }

### Transfer Money (Failure - Insufficient AccountBalance)

    POST http://localhost:8080/revolut/transaction/transfer
    {
        "transactionMaker":"lakshmikanth",
        "transactionRecipient":"jack",
        "senderBankAccountNumber":1,
        "recipientBankAccountNumber": 4,
        "amount":100000,
        "currency":"EUR",
        "transactionType":"TRANSFER"
    }
    
    Response:

    HTTP 200 OK
    POST http://localhost:8080/revolut/transaction/transfer
    {
        "transactionId": "TRN_TRANSFER_8fe0b293-8136-447d-a500-ed5432551c66",
        "transactionMaker": "lakshmikanth",
        "transactionRecipient": "jack",
        "senderBankAccountNumber": 1,
        "senderBankAccountCurrency": "EUR",
        "recipientBankAccountNumber": 3,
        "recipientBankAccountCurrency": "EUR",
        "amount": 100000,
        "currency": "EUR",
        "transactionType": "TRANSFER",
        "accountBalance": 4009.60395,
        "transactionFees": 0,
        "exchangeRate": null,
        "creationDate": "2019-07-11 14:45:27",
        "updateDate": null,
        "transactionStatus": "FAILED",
        "message": "FAILED MoneyTransfer Transaction for amount: 100000.0 EUR from senderBankAccountNumber: 1 to recipientBankAccountNumber: 3",
        "errorDetails": "MoneyTransfer failed -  Reason: Insufficient money in Sender's Account with AccountNumber: 1 (EUR)"
    }

### Transfer Money (Failure - Disparate Currencies of accounts)

    POST http://localhost:8080/revolut/transaction/transfer
    {
        "transactionMaker":"lakshmikanth",
        "transactionRecipient":"jack",
        "senderBankAccountNumber":1,
        "recipientBankAccountNumber": 4,
        "amount":100000,
        "currency":"USD",
        "transactionType":"TRANSFER"
    }
    
    Response:

    HTTP 200 OK
    POST http://localhost:8080/revolut/transaction/transfer
    {
        "transactionId": "TRN_TRANSFER_4c2823b2-e888-4e6c-9e69-a05e50436f13",
        "transactionMaker": "lakshmikanth",
        "transactionRecipient": "jack",
        "senderBankAccountNumber": 1,
        "recipientBankAccountNumber": 4,
        "amount": 100000,
        "currency": "USD",
        "transactionType": "TRANSFER",
        "accountBalance": 1951,
        "creationDate": "2019-07-10 22:29:59",
        "updateDate": null,
        "transactionStatus": "FAILED",
        "message": "FAILED MoneyTransfer Transaction for amount: 100000.0 USD from senderBankAccountNumber: 1 to recipientBankAccountNumber: 4"
    }
    
### Transfer Money (Failure - Non-Existent Account)

    POST http://localhost:8080/revolut/transaction/transfer
    {
        "transactionMaker":"lakshmikanth",
        "transactionRecipient":"jack",
        "senderBankAccountNumber":1,
        "recipientBankAccountNumber": 40,
        "amount":100000,
        "currency":"USD",
        "transactionType":"TRANSFER"
    }
    
    Response:

    HTTP 200 OK
    POST http://localhost:8080/revolut/transaction/transfer
    {
        "message": "AccountNumber: 40 doesn't exist"
    } 
            
### CrossCurrency - Money Transfer (transfer between accounts with distinct currencies)

    POST http://localhost:8080/revolut/transaction/crossTransfer
    {
        "transactionMaker":"lakshmikanth",
        "transactionRecipient":"jack",
        "senderBankAccountNumber":1,
        "recipientBankAccountNumber": 2,
        "amount":1000,
        "currency":"USD",
        "transactionType":"TRANSFER"
    }
    
    Response:

    HTTP 200 OK
    POST http://localhost:8080/revolut/transaction/crossTransfer
    {
        "transactionId": "TRN_TRANSFER_82aeae31-baa5-4d59-b851-c04cfbbbf473",
        "transactionMaker": "lakshmikanth",
        "transactionRecipient": "jack",
        "senderBankAccountNumber": 1,
        "senderBankAccountCurrency": "EUR",
        "recipientBankAccountNumber": 2,
        "recipientBankAccountCurrency": "USD",
        "amount": 1000,
        "currency": "USD",
        "transactionType": "TRANSFER",
        "accountBalance": 4009.60395,
        "transactionFees": 0.39605,
        "exchangeRate": 0.89,
        "creationDate": "2019-07-11 14:44:32",
        "updateDate": null,
        "transactionStatus": "SUCCESSFUL",
        "message": "SUCCESSFUL MoneyTransfer Transaction for amount: 1000.0 USD from senderBankAccountNumber: 1 to recipientBankAccountNumber: 2",
        "errorDetails": ""
    }
           
### Get all Transactions (Successful and failed)

    GET http://localhost:8080/revolut/transaction

    Sample response:

    HTTP 200 OK    
    [
        {
            "transactionId": "TRN_WITHDRAWAL_71c11e0a-6058-4769-85e6-b0204f279449",
            "transactionMaker": "Jack",
            "transactionRecipient": "lakshmi",
            "senderBankAccountNumber": null,
            "senderBankAccountCurrency": null,
            "recipientBankAccountNumber": 1,
            "recipientBankAccountCurrency": "EUR",
            "amount": 1000,
            "currency": "EUR",
            "transactionType": "WITHDRAWAL",
            "accountBalance": 3109.6039500000006,
            "transactionFees": 0,
            "exchangeRate": null,
            "creationDate": "2019-07-11 14:46:30",
            "updateDate": null,
            "transactionStatus": "SUCCESSFUL",
            "message": "SUCCESSFUL Withdrawal Transaction for amount: 1000.0 EUR from BankAccountNumber: 1",
            "errorDetails": ""
        },
        {
            "transactionId": "TRN_WITHDRAWAL_ad8682ae-456b-4393-8724-f8f04c9e1574",
            "transactionMaker": "Jack",
            "transactionRecipient": "lakshmi",
            "senderBankAccountNumber": null,
            "senderBankAccountCurrency": null,
            "recipientBankAccountNumber": 1,
            "recipientBankAccountCurrency": "EUR",
            "amount": 10000,
            "currency": "EUR",
            "transactionType": "WITHDRAWAL",
            "accountBalance": 4109.603950000001,
            "transactionFees": 0,
            "exchangeRate": null,
            "creationDate": "2019-07-11 14:46:20",
            "updateDate": null,
            "transactionStatus": "FAILED",
            "message": "FAILED Withdrawal Transaction for amount: 10000.0 EUR from BankAccountNumber: 1",
            "errorDetails": "Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money"
        },
        {
            "transactionId": "TRN_DEPOSIT_6d5dff15-fde2-42ff-a313-bdc53b6f0342",
            "transactionMaker": "Jack",
            "transactionRecipient": "Jack",
            "senderBankAccountNumber": null,
            "senderBankAccountCurrency": null,
            "recipientBankAccountNumber": 1,
            "recipientBankAccountCurrency": "EUR",
            "amount": 100,
            "currency": "EUR",
            "transactionType": "DEPOSIT",
            "accountBalance": 4109.603950000001,
            "transactionFees": 0,
            "exchangeRate": null,
            "creationDate": "2019-07-11 14:45:54",
            "updateDate": null,
            "transactionStatus": "SUCCESSFUL",
            "message": "SUCCESSFUL Deposit Transaction for amount: 100.0 EUR to BankAccountNumber: 1",
            "errorDetails": ""
        },
        {
            "transactionId": "TRN_TRANSFER_8fe0b293-8136-447d-a500-ed5432551c66",
            "transactionMaker": "lakshmikanth",
            "transactionRecipient": "jack",
            "senderBankAccountNumber": 1,
            "senderBankAccountCurrency": "EUR",
            "recipientBankAccountNumber": 3,
            "recipientBankAccountCurrency": "EUR",
            "amount": 100000,
            "currency": "EUR",
            "transactionType": "TRANSFER",
            "accountBalance": 4009.60395,
            "transactionFees": 0,
            "exchangeRate": null,
            "creationDate": "2019-07-11 14:45:27",
            "updateDate": null,
            "transactionStatus": "FAILED",
            "message": "FAILED MoneyTransfer Transaction for amount: 100000.0 EUR from senderBankAccountNumber: 1 to recipientBankAccountNumber: 3",
            "errorDetails": "MoneyTransfer failed -  Reason: Insufficient money in Sender's Account with AccountNumber: 1 (EUR)"
        },
        {
            "transactionId": "TRN_TRANSFER_82aeae31-baa5-4d59-b851-c04cfbbbf473",
            "transactionMaker": "lakshmikanth",
            "transactionRecipient": "jack",
            "senderBankAccountNumber": 1,
            "senderBankAccountCurrency": "EUR",
            "recipientBankAccountNumber": 2,
            "recipientBankAccountCurrency": "USD",
            "amount": 1000,
            "currency": "USD",
            "transactionType": "TRANSFER",
            "accountBalance": 4009.60395,
            "transactionFees": 0.39605,
            "exchangeRate": 0.89,
            "creationDate": "2019-07-11 14:44:32",
            "updateDate": null,
            "transactionStatus": "SUCCESSFUL",
            "message": "SUCCESSFUL MoneyTransfer Transaction for amount: 1000.0 USD from senderBankAccountNumber: 1 to recipientBankAccountNumber: 2",
            "errorDetails": ""
        },
        {
            "transactionId": "TRN_TRANSFER_1a4b1e1f-aa95-4959-903f-9006ae203350",
            "transactionMaker": "lakshmikanth",
            "transactionRecipient": "jack",
            "senderBankAccountNumber": 1,
            "senderBankAccountCurrency": "EUR",
            "recipientBankAccountNumber": 3,
            "recipientBankAccountCurrency": "EUR",
            "amount": 100,
            "currency": "EUR",
            "transactionType": "TRANSFER",
            "accountBalance": 4900,
            "transactionFees": 0,
            "exchangeRate": null,
            "creationDate": "2019-07-11 14:43:59",
            "updateDate": null,
            "transactionStatus": "SUCCESSFUL",
            "message": "SUCCESSFUL MoneyTransfer Transaction for amount: 100.0 EUR from senderBankAccountNumber: 1 to recipientBankAccountNumber: 3",
            "errorDetails": ""
        }
    ]

#### Get a transaction by transactionId

    GET http://localhost:8080/revolut/transaction/details/?transactionId=TRN_TRANSFER_bd88b49f-38d9-45ca-aafb-d342735f398a

    Sample response:

    HTTP 200 OK    
    {
        "transactionId": "TRN_TRANSFER_bd88b49f-38d9-45ca-aafb-d342735f398a",
        "transactionMaker": "lakshmikanth",
        "transactionRecipient": "jack",
        "senderBankAccountNumber": 1,
        "recipientBankAccountNumber": 4,
        "amount": 100000,
        "currency": "EUR",
        "transactionType": "TRANSFER",
        "accountBalance": 1951,
        "creationDate": "2019-07-10 22:05:11",
        "updateDate": null,
        "transactionStatus": "FAILED",
        "message": "FAILED MoneyTransfer Transaction for amount: 100000.0 EUR from senderBankAccountNumber: 1 to recipientBankAccountNumber: 4"
    }

# Test Coverage:

  - Unit Tests:
    - Repository
    - Service
    
  - Integration Tests:
    - Controller

   
# Build / Deployment:
     
## Standalone App Run Instructions:

[Standalone_App_Run](./docs/Standalone_App_Run.md)

## ServerLogs:

[Server_Logs](./docs/Server_Logs.md)


# Improvements Required:

 - Distributed processing via AKKA Actor based programming and design

    - Use AKKA support for Java to distribute the requests

 - Implement API Gateway (With load Balancer), Eureka Service Discovery and Hystrix Circuit Breaker (Netfix OSS)

 - Continuous deployment model to cloud

