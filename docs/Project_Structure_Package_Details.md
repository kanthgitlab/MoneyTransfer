# MoneyTransferAPI - Project Structure & Class Design

## Project and Package Structure:

**core-package-name:**&nbsp;io.revolut.moneytransfer

**Main-Class:**

| ClassName  |Path | Description|
|---|---|---|
| MoneyTransferAPI  | [MoneyTransferAPI.java](./../src/main/java/io/revolut/moneytransfer/MoneyTransferAPI.java) | Main Class - App's Entrypoint|

**Server-Config:**

| ClassName  |Path | Description|
|---|---|---|
| MoneyTransferServerConfiguration  | [MoneyTransferServerConfiguration](./../src/main/java/io/revolut/moneytransfer/configuration/MoneyTransferServerConfig.java) | App's Server Configuration |


**Service-Factory:**&nbsp;&nbsp;./src/main/java/io/revolut/moneytransfer/factory/

| ClassName  |Path | Description|
|---|---|---|
| MoneyTransferAPIFactory  | [MoneyTransferAPIFactory.java](./../src/main/java/io/revolut/moneytransfer/factory//MoneyTransferAPIFactory.java) | Factory Class to create/initialize Service ans repository Beans in App|

**Entity Package:**&nbsp;&nbsp;./src/main/java/io/revolut/moneytransfer/entity/

| ClassName  |Path | Description|
|---|---|---|
| Account  | [Account.java](./../src/main/java/io/revolut/moneytransfer/entity/Account.java) | Customer Account Details|
| Address  | [Address.java](./../src/main/java/io/revolut/moneytransfer/entity/Address.java) | Customer Address |
| Transaction  | [Transaction.java](./../src/main/java/io/revolut/moneytransfer/entity/Transaction.java) | Customer Transaction Details|

**Repository Package:**&nbsp;&nbsp;src/main/java/io/revolut/moneytransfer/repository/

| ClassName  |Path | Description|
|---|---|---|
| AccountRepository  | [AccountRepository.java](./../src/main/java/io/revolut/moneytransfer/repository/AccountRepository.java) | Repository to handle Customer Account Operations and retrieval from Account-DataStore|
| TransactionRepository  | [TransactionRepository.java](./../src/main/java/io/revolut/moneytransfer/repository/TransactionRepository.java) | Repository to record/retrieve Customer Transactions to/from Transaction-DataStore |


**Service Package:**&nbsp;&nbsp;./src/main/java/io/revolut/moneytransfer/service/

| ClassName  |Path | Description|
|---|---|---|
| AccountOperationsService  | [AccountOperationsService.java](./../src/main/java/io/revolut/moneytransfer/service/AccountOperationsService.java) | Service to handle Customer Account Operations and retrieval via AccountRepository|
| AccountQueryService  | [AccountQueryService.java](./../src/main/java/io/revolut/moneytransfer/service/AccountQueryService.java) | Service to query Customer's Account details and listing via AccountRepository |
| MoneyTransferService  | [MoneyTransferService.java](./../src/main/java/io/revolut/moneytransfer/service/MoneyTransferService.java) | Service to handle/execute Money-Deposit/Withdrawal/transfer to self/other accounts via TransactionRepository and AccountRepository |
| TransactionQueryService  | [TransactionQueryService.java](./../src/main/java/io/revolut/moneytransfer/service/TransactionQueryService.java) | Service to query/list Customer's transaction from Transaction-DataStore via TransactionRepository |
| ExchangeRateQueryService  | [ExchangeRateQueryService.java](./../src/main/java/io/revolut/moneytransfer/service/ExchangeRateQueryService.java) | Service to query exchnageRate for a currencyPair |
| CurrencyConversionService  | [CurrencyConversionService.java](./../src/main/java/io/revolut/moneytransfer/service/CurrencyConversionService.java) | Service to convert amount from one Currency to another Currency. depends on ExchangeRateQueryService to access rates from rateDataStore |

**Controller Package:**&nbsp;&nbsp;src/main/java/io/revolut/moneytransfer/controller/

| ClassName  |Path | Description|
|---|---|---|
| AccountCommandController  | [AccountCommandController.java](./../src/main/java/io/revolut/moneytransfer/controller/AccountCommandController.java) | Controller to server the API calls to handle Customer Account Operations and retrieval via AccountOperationsService|
| AccountQueryController  | [AccountQueryController.java](./../src/main/java/io/revolut/moneytransfer/controller/AccountQueryController.java) | Controller to handle API calls to query Customer's Account details and listing via AccountQueryService |
| MoneyTransferController  | [MoneyTransferController.java](./../src/main/java/io/revolut/moneytransfer/controller/MoneyTransferController.java) | Controller to handle/execute Money-Deposit/Withdrawal/transfer to self/other accounts via MoneyTransferService |
| TransactionQueryController  | [TransactionQueryController.java](./../src/main/java/io/revolut/moneytransfer/controller/TransactionQueryController.java) | Controller to query/list Customer's transaction from Transaction-DataStore via TransactionQueryService |

**Global Exception Handler:**&nbsp;&nbsp;src/main/java/io/revolut/moneytransfer/controller/

| ClassName  |Path | Description|
|---|---|---|
| MoneyTransferExceptionMapper  | [MoneyTransferExceptionMapper.java](./../src/main/java/io/revolut/moneytransfer/controller/MoneyTransferExceptionMapper.java) | Custom ExceptionMapper to handle Custom-RuntimeExceptions thrown by the API/Service |
| GenericExceptionMapper  | [GenericExceptionMapper.java](./../src/main/java/io/revolut/moneytransfer/controller/GenericExceptionMapper.java) | Generic ExceptionMapper to handle generic-RuntimeExceptions thrown by the API/Service |

**Custom Exception Package:**&nbsp;&nbsp;src/main/java/io/revolut/moneytransfer/exception/

| ClassName  |Path | Description|
|---|---|---|
| AccountNotFoundException  | [AccountNotFoundException.java](./../src/main/java/io/revolut/moneytransfer/exception/AccountNotFoundException.java) | Custom Exception thrown while query of invalid/non-Existing Account Query thrown by the API/Service |
| DuplicateAccountCreationExceptionMoneyTransfer  | [DuplicateAccountCreationExceptionMoneyTransfer.java](./../src/main/java/io/revolut/moneytransfer/exception/DuplicateAccountCreationExceptionMoneyTransfer.java) | Custom Exception thrown during an attempt to create a Duplicate-Account thrown by the API/Service |
| InsufficientMoneyException  | [InsufficientMoneyException.java](./../src/main/java/io/revolut/moneytransfer/exception/InsufficientMoneyException.java) | Custom Exception thrown while making an attempt to withdraw/Transfer from an Account with In-Sufficient Balance - thrown by the API/Service |
| InvalidAmountException  | [InvalidAmountException.java](./../src/main/java/io/revolut/moneytransfer/exception/InvalidAmountException.java) | Custom Exception thrown while amount passed is not a valid Decimal-Number - thrown by the API/Service |
| InvalidTransactionTypeException  | [InvalidTransactionTypeException.java](./../src/main/java/io/revolut/moneytransfer/exception/InvalidTransactionTypeException.java) | Custom Exception thrown during an attempt to depost/withdraw/Transfer with an invalid transactionType - thrown by the API/Service |

**API Model Package:**&nbsp;&nbsp;src/main/java/io/revolut/moneytransfer/model/

| ClassName  |Path | Description|
|---|---|---|
| AccountModel  | [AccountModel.java](./../src/main/java/io/revolut/moneytransfer/model/AccountModel.java) | Customer Account Details|
| CurrencyPairModel  | [CurrencyPairModel.java](./../src/main/java/io/revolut/moneytransfer/model/CurrencyPairModel.java) | CurrencyPair Model used to hold baseCurrency and alternateCurrency while querying exchangeRates and performing CurrencyConversion |
| TransactionModel  | [TransactionModel.java](./../src/main/java/io/revolut/moneytransfer/model/TransactionModel.java) | TransactionModel used to hold API-Response-Details for a Transaction|
| TransactionRequestModel  | [TransactionRequestModel.java](./../src/main/java/io/revolut/moneytransfer/model/TransactionRequestModel.java) | TransactionRequestModel - a POJO used to represent the JSON for transaction requests via API Calls|
 

**Constants:**&nbsp;&nbsp;src/main/java/io/revolut/moneytransfer/constants/

| ClassName  |Path | Description|
|---|---|---|
| TransactionStatus  | [TransactionStatus.java](./../src/main/java/io/revolut/moneytransfer/constants/TransactionStatus.java) | Enum with valid TransactionStatuses |
| TransactionType  | [TransactionType.java](./../src/main/java/io/revolut/moneytransfer/constants/TransactionType.java) | Enum with valid TransactionTypes supported  |
| AccountType  | [AccountType.java](./../src/main/java/io/revolut/moneytransfer/constants/AccountType.java) | Enum with valid AccountTypes for Customer-Accounts|
| Currency  | [Currency.java](./../src/main/java/io/revolut/moneytransfer/constants/Currency.java) | Enum with list of currencies supported |
 

**Utility Package:**&nbsp;&nbsp; src/main/java/io/revolut/moneytransfer/util/

| ClassName  |Path | Description|
|---|---|---|
| MoneyTransferAPIUtil  | [TransactionStatus.java](./../src/main/java/io/revolut/moneytransfer/util/MoneyTransferAPIUtil.java) | essential static methods for Service/repository operations |
| TimeManagementUtil  | [TimeManagementUtil.java](./../src/main/java/io/revolut/moneytransfer/util/TimeManagementUtil.java) | methods to handle timeObjects |
