```
lakshmikanth-MacBook-Pro:MoneyTransfer lakshmikanth$ pwd
/Users/lakshmikanth/Desktop/Revolut/MoneyTransfer
lakshmikanth-MacBook-Pro:MoneyTransfer lakshmikanth$ mvn clean install
[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for com.moneytransfer:moneytransfer:jar:1.0.0-SNAPSHOT
[WARNING] 'dependencies.dependency.version' for org.mockito:mockito-inline:jar is either LATEST or RELEASE (both of them are being deprecated) @ line 136, column 13
[WARNING] 'build.plugins.plugin.version' for org.apache.maven.plugins:maven-surefire-plugin is missing. @ line 212, column 12
[WARNING] 'build.plugins.plugin.version' for org.apache.maven.plugins:maven-jar-plugin is missing. @ line 189, column 12
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO] 
[INFO] ------------------< com.moneytransfer:moneytransfer >-------------------
[INFO] Building MoneyTransfer 1.0.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ moneytransfer ---
[INFO] Deleting /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/target
[INFO] 
[INFO] --- jacoco-maven-plugin:0.8.2:prepare-agent (default) @ moneytransfer ---
[INFO] argLine set to -javaagent:/Users/lakshmikanth/.m2/repository/org/jacoco/org.jacoco.agent/0.8.2/org.jacoco.agent-0.8.2-runtime.jar=destfile=/Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/target/jacoco.exec
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ moneytransfer ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.6.0:compile (default-compile) @ moneytransfer ---
[INFO] Changes detected - recompiling the module!
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 38 source files to /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ moneytransfer ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] Copying 0 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.6.0:testCompile (default-testCompile) @ moneytransfer ---
[INFO] Changes detected - recompiling the module!
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 17 source files to /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/target/test-classes
[INFO] /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/src/test/java/io/revolut/moneytransfer/controller/MoneyTransferControllerTest.java: Some input files use unchecked or unsafe operations.
[INFO] /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/src/test/java/io/revolut/moneytransfer/controller/MoneyTransferControllerTest.java: Recompile with -Xlint:unchecked for details.
[INFO] 
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ moneytransfer ---
[WARNING] useSystemClassloader setting has no effect when not forking
[INFO] Surefire report directory: /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running io.revolut.moneytransfer.repository.AccountRepositoryTest
Jul 11, 2019 3:03:43 PM org.multiverse.api.GlobalStmInstance <clinit>
INFO: Initializing GlobalStmInstance using factoryMethod 'org.multiverse.stms.gamma.GammaStm.createFast'.
Jul 11, 2019 3:03:44 PM org.multiverse.api.GlobalStmInstance <clinit>
INFO: Successfully initialized GlobalStmInstance using factoryMethod 'org.multiverse.stms.gamma.GammaStm.createFast'.
Tests run: 11, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.236 sec
Running io.revolut.moneytransfer.repository.TransactionRepositoryTest
Tests run: 5, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.007 sec
Running io.revolut.moneytransfer.util.MoneyTransferAPIUtilTest
Tests run: 5, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.012 sec
Running io.revolut.moneytransfer.util.TimeManagementUtilTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Running io.revolut.moneytransfer.controller.MoneyTransferControllerTest
Jul 11, 2019 3:03:45 PM org.hibernate.validator.internal.util.Version <clinit>
INFO: HV000001: Hibernate Validator 5.1.3.Final
Jul 11, 2019 3:03:45 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [localhost:9092]
Jul 11, 2019 3:03:45 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer] Started.
[grizzly-http-server-1] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Cannot withdraw money in Currency: USD from SenderAccount: 1 (EUR)
[grizzly-http-server-1] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_0947d59a-24ee-46cb-b0cf-0878464d9428, transactionMaker=Rod Hammer, transactionRecipient=Rod Hammer, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=USD, transactionType=WITHDRAWAL, accountBalance=1000.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:46.260, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Cannot withdraw money in Currency: USD from SenderAccount: 1 (EUR)], message=FAILED Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[grizzly-http-server-5] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Cannot deposit money in Currency: USD to RecipientAccount: 1 (EUR)
[grizzly-http-server-3] ERROR io.revolut.moneytransfer.service.MoneyTransferService - MoneyTransfer failed -  Reason: Insufficient money in Sender's Account with AccountNumber: 1 (EUR)
[grizzly-http-server-5] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_a8f44d39-6843-4fce-a4fe-fb306e037dfb, transactionMaker=Rod Hammer, transactionRecipient=Rod Hammer, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=900.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:46.591, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
Jul 11, 2019 3:03:46 PM org.glassfish.grizzly.http.server.NetworkListener shutdownNow
INFO: Stopped listener bound to [localhost:9092]
Tests run: 9, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 2.538 sec
Running io.revolut.moneytransfer.controller.AccountCommandControllerTest
Jul 11, 2019 3:03:46 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [localhost:9090]
Jul 11, 2019 3:03:46 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer-1] Started.
Jul 11, 2019 3:03:46 PM org.glassfish.grizzly.http.server.NetworkListener shutdownNow
INFO: Stopped listener bound to [localhost:9090]
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [localhost:9090]
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer-2] Started.
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.NetworkListener shutdownNow
INFO: Stopped listener bound to [localhost:9090]
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [localhost:9090]
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer-3] Started.
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.NetworkListener shutdownNow
INFO: Stopped listener bound to [localhost:9090]
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [localhost:9090]
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer-4] Started.
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.NetworkListener shutdownNow
INFO: Stopped listener bound to [localhost:9090]
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [localhost:9090]
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer-5] Started.
Jul 11, 2019 3:03:47 PM org.glassfish.grizzly.http.server.NetworkListener shutdownNow
INFO: Stopped listener bound to [localhost:9090]
Jul 11, 2019 3:03:48 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [localhost:9090]
Jul 11, 2019 3:03:48 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer-6] Started.
[main] INFO io.revolut.moneytransfer.controller.AccountCommandControllerTest - Account created: AccountModel(accountNumber=1, accountHolderName=Jhonson, address=Address(unitNumber=#01-22, buildingName=New Castle, streetOrLandmarkName=Flora Drive, postalCode=5001213, countryCode=SG), nationalIdentityCode=SG55415G, accountType=CURRENT, accountBalance=1000.1, lockedBalance=null, currency=EUR, isActive=true, creationDate=2019-07-11T15:03:48, updateDate=null)
[main] INFO io.revolut.moneytransfer.controller.AccountCommandControllerTest - is Started: true
Jul 11, 2019 3:03:48 PM org.glassfish.grizzly.http.server.NetworkListener shutdownNow
INFO: Stopped listener bound to [localhost:9090]
Tests run: 6, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.433 sec
Running io.revolut.moneytransfer.controller.TransactionQueryControllerTest
Jul 11, 2019 3:03:48 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [localhost:8099]
Jul 11, 2019 3:03:48 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer-7] Started.
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_0cf45a5b-600f-40db-bc0a-2eec7b65f87b, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1100.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.271, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_3f1f25bb-e3fb-4d0e-b41e-3fa4cbfa15a9, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=10000.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1000.0999999999999, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.287, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money], message=FAILED Withdrawal Transaction for amount: 10000.0 EUR from BankAccountNumber: 1)
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_8364ac94-7a50-4d63-8b79-efaf279979a6, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1100.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.367, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_6eaefb3b-16e6-4b61-8671-9d0b663153e1, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=10000.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1000.0999999999999, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.378, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money], message=FAILED Withdrawal Transaction for amount: 10000.0 EUR from BankAccountNumber: 1)
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_5dad792a-fa9c-4650-9c51-95d4ce4480f8, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1100.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.404, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_7eaa23ce-8832-4f6e-94da-25f849366320, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=10000.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1000.0999999999999, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.412, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money], message=FAILED Withdrawal Transaction for amount: 10000.0 EUR from BankAccountNumber: 1)
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_6827d6d4-bee6-446b-a636-028c7b5b60db, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1100.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.444, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_da5dd188-7ffa-400a-83ab-8125286b9e8b, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=10000.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1000.0999999999999, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.454, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money], message=FAILED Withdrawal Transaction for amount: 10000.0 EUR from BankAccountNumber: 1)
Jul 11, 2019 3:03:48 PM org.glassfish.grizzly.http.server.NetworkListener shutdownNow
INFO: Stopped listener bound to [localhost:8099]
Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.305 sec
Running io.revolut.moneytransfer.controller.AccountQueryControllerTest
Jul 11, 2019 3:03:48 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [localhost:8080]
Jul 11, 2019 3:03:48 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer-8] Started.
Jul 11, 2019 3:03:48 PM org.glassfish.grizzly.http.server.NetworkListener shutdownNow
INFO: Stopped listener bound to [localhost:8080]
Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.164 sec
Running io.revolut.moneytransfer.service.AccountQueryServiceTest
Tests run: 5, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.011 sec
Running io.revolut.moneytransfer.service.AccountOperationsServiceTest
Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.004 sec
Running io.revolut.moneytransfer.service.CurrencyConversionServiceTest
Tests run: 3, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Running io.revolut.moneytransfer.service.MoneyTransferServiceTest
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Cannot withdraw money in Currency: USD from SenderAccount: 1 (EUR)
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_f54e1651-466d-4902-8034-1aa744d4345a, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=USD, transactionType=WITHDRAWAL, accountBalance=1000.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.670, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Cannot withdraw money in Currency: USD from SenderAccount: 1 (EUR)], message=FAILED Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Cannot deposit money in Currency: USD to RecipientAccount: 1 (EUR)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - MoneyTransfer failed -  Reason: Insufficient money in Sender's Account with AccountNumber: 1 (EUR)
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_6f21d8f5-6017-4fad-afac-6473af616a64, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=900.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.688, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
Tests run: 8, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.023 sec
Running io.revolut.moneytransfer.service.ExchangeRateQueryServiceTest
Tests run: 3, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Running io.revolut.moneytransfer.service.TransactionQueryServiceTest
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_dad9ed5d-58ed-4cf1-8702-1d62bb05463a, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1100.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.721, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_8671620a-0065-43cc-992d-da70185088d5, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=10000.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1000.0999999999999, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.729, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money], message=FAILED Withdrawal Transaction for amount: 10000.0 EUR from BankAccountNumber: 1)
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_ca5c8b6c-5df2-48f0-8054-0ddf8f59f5dd, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1100.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.745, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_9e983ca7-3dca-45ce-b36d-8f999e6d5b7b, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=10000.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1000.0999999999999, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.757, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money], message=FAILED Withdrawal Transaction for amount: 10000.0 EUR from BankAccountNumber: 1)
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_69d99bd1-ff83-4bfb-97c7-cf2d31c6b79a, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1100.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.773, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_020c885b-901d-4a6b-bdfd-a45bfb08c4d3, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=10000.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1000.0999999999999, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.781, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money], message=FAILED Withdrawal Transaction for amount: 10000.0 EUR from BankAccountNumber: 1)
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_3173a438-8eb0-42ac-91df-7657dbcb37fd, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=100.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1100.1, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.804, updateDate=null, transactionStatus=SUCCESSFUL, exceptionMessages=[], message=SUCCESSFUL Withdrawal Transaction for amount: 100.0 EUR from BankAccountNumber: 1)
[main] ERROR io.revolut.moneytransfer.service.MoneyTransferService - Validation Error in Withdrawal Transaction: Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money
[main] INFO io.revolut.moneytransfer.service.MoneyTransferService - transactionMessage Stored: Transaction(transactionId=TRN_WITHDRAWAL_18fe97f4-c9bb-4c40-9a29-b852183a91ee, transactionMaker=Steve, transactionRecipient=Steve, senderBankAccountNumber=null, senderBankAccountCurrency=null, recipientBankAccountNumber=1, recipientBankAccountCurrency=null, amount=10000.0, currency=EUR, transactionType=WITHDRAWAL, accountBalance=1000.0999999999999, transactionFees=0.0, exchangeRate=null, creationDate=2019-07-11T15:03:48.814, updateDate=null, transactionStatus=FAILED, exceptionMessages=[Failed to withdraw Amount: 10000.0 from Account: 1 due to inSufficient Money], message=FAILED Withdrawal Transaction for amount: 10000.0 EUR from BankAccountNumber: 1)
Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.124 sec
Running io.revolut.moneytransfer.factory.MoneyTransferAPIFactoryTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.003 sec

Results :

Tests run: 73, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- jacoco-maven-plugin:0.8.2:report (report) @ moneytransfer ---
[INFO] Skipping JaCoCo execution due to missing execution data file.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ moneytransfer ---
[INFO] Building jar: /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/target/moneytransfer-1.0.0-SNAPSHOT.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.2-beta-5:single (make-assembly) @ moneytransfer ---
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/com.fasterxml.jackson.core/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/com.fasterxml.jackson.core/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/org.glassfish.jersey.internal.spi.AutoDiscoverable already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] org/glassfish/jersey/message/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/com.fasterxml.jackson.jaxrs/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] com/fasterxml/jackson/jaxrs/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/com.fasterxml.jackson.databind.Module already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.grizzly/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/grizzly/ already added, skipping
[INFO] org/glassfish/grizzly/http/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.grizzly/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/grizzly/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.jersey.core/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/org.glassfish.jersey.internal.spi.AutoDiscoverable already added, skipping
[INFO] com/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.jersey.core/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.jersey.media/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/org.glassfish.jersey.internal.spi.ForcedAutoDiscoverable already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.jersey.ext/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/org.glassfish.jersey.internal.spi.ForcedAutoDiscoverable already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] org/glassfish/jersey/server/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] com/ already added, skipping
[INFO] com/sun/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.hk2/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.hk2.external/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/jvnet/ already added, skipping
[INFO] org/jvnet/hk2/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/hk2/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.hk2/ already added, skipping
[INFO] javax/ already added, skipping
[INFO] javax/inject/ already added, skipping
[INFO] javax/inject/Inject.class already added, skipping
[INFO] javax/inject/Named.class already added, skipping
[INFO] javax/inject/Provider.class already added, skipping
[INFO] javax/inject/Qualifier.class already added, skipping
[INFO] javax/inject/Scope.class already added, skipping
[INFO] javax/inject/Singleton.class already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/jvnet/ already added, skipping
[INFO] org/jvnet/hk2/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/hk2/ already added, skipping
[INFO] org/glassfish/hk2/utilities/ already added, skipping
[INFO] META-INF/maven/org.glassfish.hk2/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] com/ already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/apache/ already added, skipping
[INFO] org/apache/commons/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/NOTICE.txt already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] com/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] javax/annotation/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] com/ already added, skipping
[INFO] com/google/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] com/ already added, skipping
[INFO] com/google/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/slf4j/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.slf4j/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] module-info.class already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] module-info.class already added, skipping
[INFO] org/ already added, skipping
[INFO] org/objectweb/ already added, skipping
[INFO] org/objectweb/asm/ already added, skipping
[INFO] org/objectweb/asm/signature/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/apache/ already added, skipping
[INFO] org/apache/commons/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/NOTICE.txt already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/apache/ already added, skipping
[INFO] org/apache/commons/ already added, skipping
[INFO] META-INF/NOTICE.txt already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/apache/ already added, skipping
[INFO] org/apache/commons/ already added, skipping
[INFO] META-INF/NOTICE.txt already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] Building jar: /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/target/moneytransfer-1.0.0-SNAPSHOT-jar-with-dependencies.jar
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/com.fasterxml.jackson.core/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/com.fasterxml.jackson.core/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/org.glassfish.jersey.internal.spi.AutoDiscoverable already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] org/glassfish/jersey/message/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/com.fasterxml.jackson.jaxrs/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] com/fasterxml/jackson/jaxrs/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/com.fasterxml.jackson.databind.Module already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] com/fasterxml/jackson/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.grizzly/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/grizzly/ already added, skipping
[INFO] org/glassfish/grizzly/http/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.grizzly/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/grizzly/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.jersey.core/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/org.glassfish.jersey.internal.spi.AutoDiscoverable already added, skipping
[INFO] com/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.jersey.core/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.jersey.media/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/org.glassfish.jersey.internal.spi.ForcedAutoDiscoverable already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.jersey.ext/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] META-INF/services/org.glassfish.jersey.internal.spi.ForcedAutoDiscoverable already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] org/glassfish/jersey/server/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] com/ already added, skipping
[INFO] com/fasterxml/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] com/ already added, skipping
[INFO] com/sun/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/jersey/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.hk2/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.hk2.external/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/jvnet/ already added, skipping
[INFO] org/jvnet/hk2/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/hk2/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.glassfish.hk2/ already added, skipping
[INFO] javax/ already added, skipping
[INFO] javax/inject/ already added, skipping
[INFO] javax/inject/Inject.class already added, skipping
[INFO] javax/inject/Named.class already added, skipping
[INFO] javax/inject/Provider.class already added, skipping
[INFO] javax/inject/Qualifier.class already added, skipping
[INFO] javax/inject/Scope.class already added, skipping
[INFO] javax/inject/Singleton.class already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/jvnet/ already added, skipping
[INFO] org/jvnet/hk2/ already added, skipping
[INFO] org/glassfish/ already added, skipping
[INFO] org/glassfish/hk2/ already added, skipping
[INFO] org/glassfish/hk2/utilities/ already added, skipping
[INFO] META-INF/maven/org.glassfish.hk2/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/LICENSE already added, skipping
[INFO] META-INF/NOTICE already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/services/ already added, skipping
[INFO] com/ already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/apache/ already added, skipping
[INFO] org/apache/commons/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/NOTICE.txt already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] com/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] javax/ already added, skipping
[INFO] javax/annotation/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] com/ already added, skipping
[INFO] com/google/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] com/ already added, skipping
[INFO] com/google/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/slf4j/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/org.slf4j/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] module-info.class already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] module-info.class already added, skipping
[INFO] org/ already added, skipping
[INFO] org/objectweb/ already added, skipping
[INFO] org/objectweb/asm/ already added, skipping
[INFO] org/objectweb/asm/signature/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] org/ already added, skipping
[INFO] org/apache/ already added, skipping
[INFO] org/apache/commons/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/NOTICE.txt already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/apache/ already added, skipping
[INFO] org/apache/commons/ already added, skipping
[INFO] META-INF/NOTICE.txt already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] org/apache/ already added, skipping
[INFO] org/apache/commons/ already added, skipping
[INFO] META-INF/NOTICE.txt already added, skipping
[INFO] META-INF/LICENSE.txt already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] org/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] 
[INFO] --- maven-install-plugin:2.4:install (default-install) @ moneytransfer ---
[INFO] Installing /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/target/moneytransfer-1.0.0-SNAPSHOT.jar to /Users/lakshmikanth/.m2/repository/com/moneytransfer/moneytransfer/1.0.0-SNAPSHOT/moneytransfer-1.0.0-SNAPSHOT.jar
[INFO] Installing /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/pom.xml to /Users/lakshmikanth/.m2/repository/com/moneytransfer/moneytransfer/1.0.0-SNAPSHOT/moneytransfer-1.0.0-SNAPSHOT.pom
[INFO] Installing /Users/lakshmikanth/Desktop/Revolut/MoneyTransfer/target/moneytransfer-1.0.0-SNAPSHOT-jar-with-dependencies.jar to /Users/lakshmikanth/.m2/repository/com/moneytransfer/moneytransfer/1.0.0-SNAPSHOT/moneytransfer-1.0.0-SNAPSHOT-jar-with-dependencies.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  20.860 s
[INFO] Finished at: 2019-07-11T15:03:57+08:00
[INFO] ------------------------------------------------------------------------
lakshmikanth-MacBook-Pro:MoneyTransfer lakshmikanth$ 
```