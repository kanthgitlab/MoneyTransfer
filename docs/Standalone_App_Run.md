# Build and Run Instructions: 

## Clean Build:

<b>Pre-Requisites for build</b>
<p>
Ensure that java-8 and maven-3 are installed in your machine
Verify installation by running java and mvn commands from your command console
</p>

<b>Commands:</b>

1. Clone the git repository:

    ```sh
    git clone https://gitlab.com/kanthgitlab/MoneyTransfer.git
    ```

2. If Clone doesn't work, download the project from:

    ```shell
    https://gitlab.com/kanthgitlab/MoneyTransfer
    ```

3. Navigate to the Cloned project directory:

    ```sh
    cd Users/lakshmikanth/Desktop/Revolut/MoneyTransfer
    ```

4. Run maven command to clean, compile, test and install app

    ```sh
    mvn clean install
    ```

    <b>Install Logs:</b> [Maven_Clean_Install_Logs](./docs/Maven_Clean_Install_Logs.md)

5. Run java command to start the Service

    ```sh
    java -jar target/moneytransfer-1.0.0-SNAPSHOT-jar-with-dependencies.jar 
    ```

    <b>Command Console Logs:</b>

    ```
    lakshmikanth-MacBook-Pro:MoneyTransfer lakshmikanth$ java -jar target/moneytransfer-1.0.0-SNAPSHOT-jar-with-dependencies.jar 
    Jul 11, 2019 3:11:55 PM org.glassfish.grizzly.http.server.NetworkListener start
    INFO: Started listener bound to [localhost:8080]
    Jul 11, 2019 3:11:55 PM org.glassfish.grizzly.http.server.HttpServer start
    INFO: [HttpServer] Started.
    [main] INFO io.revolut.moneytransfer.MoneyTransferAPI - started MoneyTransferAPI Server - Press ENTER-Key in this command console to stop the server```
    ```

